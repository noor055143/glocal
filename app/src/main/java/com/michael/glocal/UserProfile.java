package com.michael.glocal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.michael.glocal.Utils.NetworkUtil;
import com.michael.glocal.models.users;

import java.util.UUID;

import static com.google.firebase.database.FirebaseDatabase.getInstance;

public class UserProfile extends AppCompatActivity implements View.OnClickListener{

    private static final int SELECT_PICTURE = 100;
    private ImageView btn;
    private Button ok;
    private String name;
    private String path;
    SharedPreferences sp;
    Uri selectedImageUri;
    SharedPreferences shared;
    DatabaseReference databaseUsers, firedatabase;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageReference = storage.getReference();
    private boolean editMode;
    String picture = UUID.randomUUID().toString();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        setTitle("Upload Picture");
        shared = PreferenceManager.getDefaultSharedPreferences(this);
        btn = (ImageView) findViewById(R.id.imgbtn);
        btn.setOnClickListener(this);
        ok = (Button) findViewById(R.id.OK);
        ok.setOnClickListener(this);
        editMode=false;
        if(getIntent().hasExtra("pictureEditMode"))
            editMode=true;
    }
    @Override
    protected void onResume() {
        super.onResume();

        if (NetworkUtil.startActivityIfNetworkIsNotConnected(this)) {
            finish();
            return;
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        NetworkUtil.canNetworkWatcherThreadKeepRunning = false;
    }
    /* public void onClick(View v) {

         if(v.getId()==R.id.upload)
         {


             Intent in=new Intent(this,EmailValidationActivity.class);

             startActivity(in);
         }*/
    // Choose an image from Gallery
    public void chooseImage() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                // Get the url from data
                selectedImageUri = data.getData();


                if (selectedImageUri != null) {
                    // Get the path from the Uri
                    path = getPathFromURI(selectedImageUri);
                   /* Log.i(TAG, "Image Path : " + path);
                    String filename = path.substring(path.lastIndexOf("/") + 1);
                    Log.i(TAG, "Image name: " + filename);*/
                    // Set the image in ImageView
                    ((ImageButton) findViewById(R.id.imgbtn)).setImageURI(selectedImageUri);
                }
            }
        }
    }

    /* Get the real path from the URI */
    public String getPathFromURI(Uri contentUri) {
/*        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;*/
        String realPath=null;
        // SDK < API11
        if (Build.VERSION.SDK_INT < 11)
            realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, contentUri);

            // SDK >= 11 && SDK < 19
        else if (Build.VERSION.SDK_INT < 19)
            realPath = RealPathUtil.getRealPathFromURI_API11to18(this, contentUri);

            // SDK > 19 (Android 4.4)
        else
            realPath = RealPathUtil.getRealPathFromURI_API19(this, contentUri);
        return realPath;
    }

    public void onClick(View v) {

        if (v.getId() == R.id.imgbtn) {
            chooseImage();
        }
        if (v.getId() == R.id.OK) {
            if (path == null) {
                Toast.makeText(getApplicationContext(), "Select the profile picture", Toast.LENGTH_LONG).show();
            } else {

                uploadImage();
                //String filename = path.substring(path.lastIndexOf("/") + 1);

               /*
                SharedPreferences.Editor ed=sp.edit();
                ed.putString("PictureName",filename);
                ed.commit();
*/
                firedatabase = getInstance().getReference();
                databaseUsers = FirebaseDatabase.getInstance().getReference("users");

              //  if(editMode==true)
               // {
                    SharedPreferences shp = getSharedPreferences("user", Context.MODE_PRIVATE);

                    String userid= shp.getString("UserID","defaultValue");
                    SharedPreferences.Editor ed = shp.edit();
                    databaseUsers.child(userid).child("picture").setValue(picture);
                    ed.putString("Picture",picture);
                    ed.commit();
               // }
         /*       else {
                    final String id = databaseUsers.push().getKey();

                    //  int UserID=Integer.getInteger(id);
                    String name = shared.getString("Name", "defaultValue");
                    String password = shared.getString("Password", "defaultValue");
                    String email = shared.getString("Email", "defaultValue");
                    String title = shared.getString("Title", "defaultValue");
                    int abstractid = shared.getInt("AbstractID", 0);

                    String abstracttitle = shared.getString("AbstractTitle", "defaultValue");
                    String country = shared.getString("Country", "defaultValue");
                    String affiliation = shared.getString("Affiliation", "defaultValue");
                    String contact = shared.getString("Contact", "defaultValue");
                    final users user = new users(name, email, country, password, title, affiliation, abstracttitle, abstractid, contact, picture, id);
                    //databaseUsers.child("2").setValue(3);
                    //Log.i("Chck", "users: "+users.geName());
                    Log.e("chck", "ref : " + databaseUsers);
                    //firedatabase.child("users").child(id).setValue("3");

                    user.loggedin = "false";
                    databaseUsers.child(id).setValue(user);
                    Log.e("insert", "user");
                    Intent i = new Intent(UserProfile.this, LogIn.class);
                    Log.e("intent", "i");

                    startActivity(i);
                }*/

            }
        }

    }

    private void uploadImage() {

        if (selectedImageUri != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();

            StorageReference ref = storageReference.child("images/" + picture);
            ref.putFile(selectedImageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Toast.makeText(UserProfile.this, "Uploaded", Toast.LENGTH_SHORT).show();
                            if(editMode==true)
                                finish();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(UserProfile.this, "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot
                                    .getTotalByteCount());
                            progressDialog.setMessage("Uploaded " + (int) progress + "%");
                        }
                    });
        }
    }

}
