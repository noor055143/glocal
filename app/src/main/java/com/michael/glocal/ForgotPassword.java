package com.michael.glocal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.michael.glocal.Utils.NetworkUtil;

import java.util.Random;
import java.util.concurrent.Executors;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener{

    EditText userEmail;
    Button btnsendEmail;
    boolean s;
    boolean status;
    private ProgressDialog progress;
    DatabaseReference databaseUsers, firedatabase;
    SharedPreferences preferences;
    FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        userEmail = (EditText) findViewById(R.id.userEmailForResetPassword);
        btnsendEmail = (Button) findViewById(R.id.SendResetPassword);

        btnsendEmail.setOnClickListener(this);
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (NetworkUtil.startActivityIfNetworkIsNotConnected(this)) {
            finish();
            return;
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        NetworkUtil.canNetworkWatcherThreadKeepRunning = false;
    }
    @Override
    public void onClick(View v) {
/*
        if(v.getId() == R.id.SendResetPassword) {
            firebaseAuth.sendPasswordResetEmail(userEmail.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {


                    if(task.isSuccessful()){
                        Toast.makeText(ForgotPassword.this, "Password Sent to Your Email", Toast.LENGTH_SHORT).show();
                    }else
                    {
                        Toast.makeText(ForgotPassword.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }*/
        if(v.getId() == R.id.SendResetPassword) {
            progress = new ProgressDialog(ForgotPassword.this);
            progress.setMessage("Verifying email...");
            progress.show();

            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    s = sendEmail();
                    if (s == true) {
                        progress.dismiss();
                        Toast.makeText(ForgotPassword.this, "Check Your Email For new Password",Toast.LENGTH_SHORT).show();
                        Intent in = new Intent(ForgotPassword.this, LogIn.class);
                        startActivity(in);
                    } else {
                        new AlertDialog.Builder(ForgotPassword.this)

                                .setTitle("Connection")
                                .setMessage("Your email is not in Database.Go back and check your email or internet connection")
                                .setCancelable(true)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent i = new Intent(ForgotPassword.this, ForgotPassword.class);
                                        startActivity(i);
                                    }
                                }).show();

                    }
                }
            });
        }
    }


    protected boolean sendEmail() {
        try {

            String pass = "cala2020";

            SharedPreferences.Editor ed = preferences.edit();
            ed.putString("DefaultPassword", pass);
            ed.putString("Email",userEmail.getText().toString());
            ed.commit();


            String email = preferences.getString("Email", "defaultValue");

            GMailSender sender = new GMailSender("recoverums123@gmail.com", "eadpassword");
            status = sender.sendMail("Your Default Password",pass + " is your Default Password For Glocal, You Can Reset it via Application.","recoverums123@gmail.com",email);
        } catch (Exception e) {
            Log.e("NotSend", e.getMessage(), e);

        }
        return status;
    }
}


