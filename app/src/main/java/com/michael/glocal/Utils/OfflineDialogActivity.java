package com.michael.glocal.Utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import android.app.FragmentManager;

public class OfflineDialogActivity extends android.app.DialogFragment {
    public static final String connectMessage = "You are offline! please connect to the internet to continue.";
    public static final String disconnecMessage = "Internet is connected!";
    private AlertDialog.Builder builder;
    private AlertDialog dialog;
    private FragmentManager mFragment;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(connectMessage);
        builder.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().moveTaskToBack(true);
                getActivity().finish();
            }
        });
        setCancelable(false);
        dialog = builder.create();
        return dialog;
    }

    public void showDialog(FragmentManager fragmentManager) {
        if(isAdded()) {
            return;
        }
        if(fragmentManager != null && !isVisible()) {

            show(fragmentManager, "OfflineDialogActivity");
        }
    }
    public void hideDialog() {
        if(builder != null && dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }

}
