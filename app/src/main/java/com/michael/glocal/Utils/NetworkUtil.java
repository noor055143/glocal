package com.michael.glocal.Utils;

import android.app.Activity;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.content.Context;
import android.widget.Toast;

public class NetworkUtil {

    private static final String TAG = "NetworkUtil";
    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;

    public static boolean canNetworkWatcherThreadKeepRunning = true;
    private static OfflineDialogActivity offlineDialogActivity = new OfflineDialogActivity();

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static String getConnectivityStatusString(Context context) {
        int conn = NetworkUtil.getConnectivityStatus(context);
        String status = null;
        if (conn == NetworkUtil.TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == NetworkUtil.TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
        }
        return status;
    }

    public static boolean startActivityIfNetworkIsNotConnected(Activity activity) {


        canNetworkWatcherThreadKeepRunning = true;
        networkWatcherThreadStart(activity);

        return false;
    }

    private static void networkWatcherThreadStart(Activity activity) {
        Log.d("NetworkUtil", "Mobile has api > 24");

        new Thread(new Runnable() {
            @Override
            public void run() {

                if(getConnectivityStatus(activity) == TYPE_NOT_CONNECTED) {
                    activity.runOnUiThread(new Runnable() {
                        public void run() {
                            Log.d(TAG, "Network not connected");

                            offlineDialogActivity.showDialog(activity.getFragmentManager());

                        }
                    });
                }


                int networkCurrentStatus = getConnectivityStatus(activity);
                while (canNetworkWatcherThreadKeepRunning) {
                    if (networkCurrentStatus != getConnectivityStatus(activity)) {
                        // network status changed
                        if (getConnectivityStatus(activity) == TYPE_NOT_CONNECTED) {
                            activity.runOnUiThread(new Runnable() {
                                public void run() {
                                    Log.d(TAG, "Network not connected");

                                    offlineDialogActivity.showDialog(activity.getFragmentManager());

                                }
                            });
                        } else {
                            activity.runOnUiThread(new Runnable() {
                                public void run() {
                                    Log.d(TAG, "Network is connected");
                                    Toast.makeText(activity, OfflineDialogActivity.disconnecMessage, Toast.LENGTH_SHORT).show();
                                    offlineDialogActivity.hideDialog();
                                }
                            });
                        }

                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        // update current status
                        networkCurrentStatus = getConnectivityStatus(activity);
                    }

                } // end while loop


            }
        }).start();
    }

}
