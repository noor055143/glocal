package com.michael.glocal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.michael.glocal.Utils.NetworkUtil;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignUp extends AppCompatActivity implements View.OnClickListener{

    SharedPreferences sp;
    EditText userTitle, userName, userEmail, userPassword, userAbstractTitle, userAffiliation, userAbstractID, userCountry, userContact;
    ProgressBar progressBar;
    Button signUpBtn;
    DatabaseReference databaseUsers;
    boolean flag=false;

    FirebaseAuth firebaseAuth;
    FirebaseFirestore firestore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        sp= PreferenceManager.getDefaultSharedPreferences(this);
        firebaseAuth = FirebaseAuth.getInstance();
        initViews();
        userEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus)
                {
                    String e=userEmail.getText().toString();
                    String validemail = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +"\\@" +"[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +"(" +"\\." +"[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +")+";
                    if(!TextUtils.isEmpty(e)) {
                        Matcher matcher = Pattern.compile(validemail).matcher(e);
                        if (!matcher.matches()) {
                            // Toast.makeText(getApplicationContext(), "Enter the correct Email", Toast.LENGTH_SHORT).show();
                            userEmail.setError("Enter the correct Email");
                        }
                    }

                }
            }
        });
    }
    void initViews(){
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        userTitle = (EditText) findViewById(R.id.UserTitle);
        userName = (EditText) findViewById(R.id.UserName);
        userAffiliation = (EditText) findViewById(R.id.UserAffiliation);
        userCountry = (EditText) findViewById(R.id.UserCountry);
        userAbstractTitle = (EditText) findViewById(R.id.UserAbstractTitle);
        userAbstractID = (EditText) findViewById(R.id.UserAbstractID);
        userEmail = (EditText) findViewById(R.id.UserEmail);
        userPassword = (EditText) findViewById(R.id.UserPassword);
        userContact = (EditText) findViewById(R.id.UserContact);

        signUpBtn = (Button) findViewById(R.id.SignUpIntoDatabase);
        signUpBtn.setOnClickListener(this);
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (NetworkUtil.startActivityIfNetworkIsNotConnected(this)) {
            finish();
            return;
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        NetworkUtil.canNetworkWatcherThreadKeepRunning = false;
    }
    @Override
    public void onClick(View v) {
        progressBar.setVisibility(View.VISIBLE);
        if(v.getId() == R.id.SignUpIntoDatabase) {
            String title = userTitle.getText().toString();
            String name = userName.getText().toString();
            String email = userEmail.getText().toString();
            String pass = userPassword.getText().toString();
            String affiliation = userAffiliation.getText().toString();
            String absTitle = userAbstractTitle.getText().toString();
            int abdID = Integer.parseInt(userAbstractID.getText().toString());
            String country = userCountry.getText().toString();
            String contact = userContact.getText().toString();

            if (TextUtils.isEmpty(name) || TextUtils.isEmpty(contact) || TextUtils.isEmpty(pass) || TextUtils.isEmpty(email) || TextUtils.isEmpty(title) || TextUtils.isEmpty(affiliation) || TextUtils.isEmpty(absTitle) || TextUtils.isEmpty(country)) {

                Toast.makeText(getApplicationContext(), "Fill all Fields", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Wait", Toast.LENGTH_LONG).show();
                databaseUsers = FirebaseDatabase.getInstance().getReference("users");
                databaseUsers.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                            Object o = postSnapshot.getValue();
                            HashMap u = (HashMap) o;

                            Object o2 = u.get("email");
                            String db_email = String.valueOf(o2);

                            o2 = u.get("contact");
                            String db_contact = String.valueOf(o2);


                            if (db_email.equals(email)  || db_contact.equals(contact)) {
                                flag=true;
                                Log.e("db-email", db_email);
                                Log.e("db-cnic", db_contact);
                                break;
                            }
                        }
                        if(flag==true)
                        {
                            //Intent i = new Intent(SignUp.this, ExistingUserActivity.class);
                            Log.e("existing", "user");
                            Toast.makeText(getApplicationContext(), "existing: user", Toast.LENGTH_LONG).show();
                            //startActivity(i);

                        }
                        else
                        {
                            SharedPreferences.Editor ed=sp.edit();
                            ed.putString("Title",title);
                            ed.putString("Name",name);
                            ed.putString("Password",pass);
                            ed.putString("Email",email);
                            ed.putString("Contact",contact);
                            ed.putInt("AbstractID",abdID);
                            ed.putString("Affiliation",affiliation);
                            ed.putString("Country",country);
                            ed.putString("AbstractTitle",absTitle);
                            ed.commit();
                            // Users u=new Users(3,n,p,e,"",nic,a,nat);
                            databaseUsers.removeEventListener(this);

                            Intent in = new Intent(SignUp.this, EmailValidation.class);
                            startActivity(in);
                          //  sendEmailVerificationMsg();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

            }
        }
    }


}
