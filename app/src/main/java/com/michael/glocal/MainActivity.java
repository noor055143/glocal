package com.michael.glocal;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.michael.glocal.Utils.NetworkUtil;

import java.io.File;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {

    private SharedPreferences mprefrences;
    private SharedPreferences.Editor meditor;
    private LocationManager locationManager;
    ImageView user_image;
    String CurrentUser;
    String pic;
    ImageButton btn_uploadImage, btn_edit_profile;
    ImageButton btnSearch;
    RelativeLayout cala;

    private RelativeLayout comela;
    // private LinearLayout Melela;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, menu);


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_logout) {
            SharedPreferences sp = getSharedPreferences("user", Context.MODE_PRIVATE);
            if (sp.getString("UserID", null) != null) {
                String uID = sp.getString("UserID", null);
                SharedPreferences.Editor editor = sp.edit();
                editor.remove("UserID");
                editor.commit();

                DatabaseReference databaseUsers = FirebaseDatabase.getInstance().getReference("users");
                databaseUsers.child(uID).child("loggedin").setValue("false");


                Intent intent = new Intent(this, LogIn.class);
                startActivity(intent);
                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       /* locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);*/
        cala = findViewById(R.id.layout1);
        comela = findViewById(R.id.layout2);
      //  Melela = findViewById(R.id.layout3);

        mprefrences = PreferenceManager.getDefaultSharedPreferences(this);
        meditor = mprefrences.edit();
      /*  if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
*/

        //Code

            user_image = findViewById(R.id.user_image_home_activity);
            btn_uploadImage = findViewById(R.id.btn_upload_image_main_activity);
            btn_edit_profile = findViewById(R.id.btn_edit_profile_home_activity);
            btnSearch = findViewById(R.id.search);
            SharedPreferences sp = getSharedPreferences("user", Context.MODE_PRIVATE);
            pic=sp.getString("Picture", "");
            //pic="359749a1-709f-4e5c-af26-231dc363cea0";

            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    loadImage();
                }
            });



            if (getIntent() != null && getIntent().getExtras() != null && getIntent().getExtras().getString("CurrentUser") != null) {
                CurrentUser = getIntent().getExtras().getString("CurrentUser");
            } else {

                // SharedPreferences sp = getSharedPreferences("user", Context.MODE_PRIVATE);
                CurrentUser = sp.getString("UserID", "");
            }
        btn_uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MainActivity.this, UserProfile.class);
                i.putExtra("CurrentUser", CurrentUser);
                i.putExtra("pictureEditMode", true);

                startActivity(i);
            }
        });
        btn_edit_profile.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, EditUserProfile.class);
                i.putExtra("CurrentUser", CurrentUser);
                startActivity(i);
            }
        }));
            //Code
        cala.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                meditor.putString("Event","CALA");
                meditor.commit();
                String name = mprefrences.getString("Event", "default");
                //Log.d("prefrence name", "Event = " + name);
              //  Toast.makeText(MainActivity.this, "Event: " + name, Toast.LENGTH_SHORT).show();
                Intent CALAINTENT = new Intent(MainActivity.this,CALA.class);
                getIntent().putExtra("key", "CALA");
                startActivity(CALAINTENT);
            }
        });
        comela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                meditor.putString("Event","Comela");
                meditor.commit();
                String name = mprefrences.getString("Event: ", "default");
                //Log.d("prefrence name", "Event = " + name);
               // Toast.makeText(MainActivity.this, "Event" + name, Toast.LENGTH_SHORT).show();
                Intent COMELA = new Intent(MainActivity.this,COMELA.class);
                getIntent().putExtra("key", "COMELA");
                startActivity(COMELA);
            }
        });

      /*  Melela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });*/
    /*    btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MainActivity.this, LocationTracer.class);
                startActivity(i);
            }
        });*/

    }
    SharedPreferences.Editor putDouble(final SharedPreferences.Editor edit, final String key, final double value) {
        return edit.putLong(key, Double.doubleToRawLongBits(value));
    }
  /*  @Override
    public void onLocationChanged(Location location) {
        double lat = location.getLatitude();
        double lon = location.getLongitude();
        DatabaseReference databaseUsers = FirebaseDatabase.getInstance().getReference("users");
        String userID = mprefrences.getString("UserID", null);
        databaseUsers.child(userID).child("lat").setValue(lat);
        databaseUsers.child(userID).child("lon").setValue(lon);
        SharedPreferences.Editor ed = mprefrences.edit();
        putDouble(ed, "lat", lat);
        putDouble(ed, "lon", lon);

        ed.commit();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }*/
    @Override
    protected void onResume() {
        super.onResume();
        if (NetworkUtil.startActivityIfNetworkIsNotConnected(this)) {
            finish();
            return;
        }
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        SharedPreferences sp = getSharedPreferences("user", Context.MODE_PRIVATE);
        pic=sp.getString("Picture", "");

        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                loadImage();
            }
        });

    }

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        finish();
       // super.onBackPressed();
       /* SharedPreferences sp = getSharedPreferences("user", Context.MODE_PRIVATE);
        if (sp.getString("UserID", null) != null) {
            String uID = sp.getString("UserID", null);
            SharedPreferences.Editor editor = sp.edit();
            editor.remove("UserID");
            editor.commit();

            DatabaseReference databaseUsers = FirebaseDatabase.getInstance().getReference("users");
            databaseUsers.child(uID).child("loggedin").setValue("false");


            finishAffinity();
        }*/
      /*  Intent intent = new Intent(MainActivity.this, LogIn.class);
        startActivity(intent);*/

    }


    public void loadImage()
    {
        try {
            final File tmpFile = File.createTempFile("img", "png");
            StorageReference reference = FirebaseStorage.getInstance().getReference("images");

            //  "id" is name of the image file....
            String id=pic;

            reference.child(id).getFile(tmpFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                    Bitmap image = BitmapFactory.decodeFile(tmpFile.getAbsolutePath());

                    user_image.setImageBitmap(image);

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
