package com.michael.glocal;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.michael.glocal.Utils.NetworkUtil;
import com.michael.glocal.models.users;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EditUserProfile extends AppCompatActivity implements View.OnClickListener{
    EditText userTitle, userName, userEmail, userPassword, userAbstractTitle, userAffiliation, userAbstractID, userCountry, userContact;
    private Button btn;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference ref = database.getReference("users");
    SharedPreferences sp;
    boolean flag = false;
    String email_cnic;
    private ProgressDialog loadingDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user_profile);

        setTitle("Edit Profile");

        loadingDialog = new ProgressDialog(this);
        loadingDialog.setCancelable(false);
        loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);


        sp = getSharedPreferences("user", Context.MODE_PRIVATE);


       // userTitle = (EditText) findViewById(R.id.UserTitle);
        userName = (EditText) findViewById(R.id.UserName);
       // userAffiliation = (EditText) findViewById(R.id.UserAffiliation);
       // userCountry = (EditText) findViewById(R.id.UserCountry);
       // userAbstractTitle = (EditText) findViewById(R.id.UserAbstractTitle);
       // userAbstractID = (EditText) findViewById(R.id.UserAbstractID);
        userEmail = (EditText) findViewById(R.id.UserEmail);
        userPassword = (EditText) findViewById(R.id.UserPassword);
       // userContact = (EditText) findViewById(R.id.UserContact);

        btn = (Button) findViewById(R.id.btnEdit);
        btn.setOnClickListener(this);

        //Email validation
        userEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String e = userEmail.getText().toString();
                    String validemail = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@" + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+";
                    if (!TextUtils.isEmpty(e)) {
                        Matcher matcher = Pattern.compile(validemail).matcher(e);
                        if (!matcher.matches()) {
                            // Toast.makeText(getApplicationContext(), "Enter the correct Email", Toast.LENGTH_SHORT).show();
                            userEmail.setError("Enter the correct Email");
                        }
                    }

                }
            }
        });
        String userID = sp.getString("UserID", null);
        Toast.makeText(this, "User ID is:" + userID, Toast.LENGTH_SHORT).show();
        if (userID == null) {
            // - user is null ...launch login activity
            Toast.makeText(this, "User ID is null", Toast.LENGTH_SHORT).show();
            finish();
        }
        showData();
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (NetworkUtil.startActivityIfNetworkIsNotConnected(this)) {
            finish();
            return;
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        NetworkUtil.canNetworkWatcherThreadKeepRunning = false;
    }


    public void showData() {

    /*    DatabaseReference databaseUsers = FirebaseDatabase.getInstance().getReference("users");
        String userID = sp.getString("UserID", null);
        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
                users u = dataSnapshot.getValue(users.class);
                userName.setText(u.name);
                userAbstractID.setText(u.absid);
                userAffiliation.setText(u.affiliation);
                userCountry.setText(u.country);
                 userTitle.setText(u.title);
                userAbstractTitle.setText(u.abstitle);
                userContact.setText(u.contact);
                // ...
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("msg: ", "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
        databaseUsers.addValueEventListener(postListener);

*/
//get values
     /*   userAbstractID.setText(databaseUsers.child(userID).child("absid").toString());
        userAffiliation.setText(databaseUsers.child(userID).child("affiliation").toString());
        userCountry.setText(databaseUsers.child(userID).child("country").toString());
        userName.setText(databaseUsers.child(userID).child("name").toString());
        userTitle.setText(databaseUsers.child(userID).child("title").toString());
        userAbstractTitle.setText(databaseUsers.child(userID).child("abstitle").toString());
        userContact.setText(databaseUsers.child(userID).child("contact").toString());
*/
        String password = sp.getString("Password", "");
        String email = sp.getString("Email", "");
        //int absid = sp.getInt("AbstractID", 0);
        String name = sp.getString("Name", "");
      //  String title = sp.getString("Title", "");
        //String contact = sp.getString("Contact", "");
        //String country = sp.getString("Country", "");
        //String affiliation = sp.getString("Affiliation", "");
        //String absTitle = sp.getString("AbstractTitle", "");

        userName.setText(name);
       // userTitle.setText(title);
        userEmail.setText(email);
        userPassword.setText(password);
       // userAbstractTitle.setText(absTitle);
       // userAbstractID.setText(""+ absid);
        //userCountry.setText(country);
        //userAffiliation.setText(affiliation);
        //userContact.setText(contact);

    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnEdit) {

            String n = userName.getText().toString();
            String p = userPassword.getText().toString();
            String e = userEmail.getText().toString();
           // String aff = userAffiliation.getText().toString();
           // String a = userAbstractID.getText().toString();
           // String abst = userAbstractTitle.getText().toString();
           // String count = userCountry.getText().toString();
          // String title = userTitle.getText().toString();
           // String con = userContact.getText().toString();
            if ( TextUtils.isEmpty(p) || TextUtils.isEmpty(e)) {
                Toast.makeText(getApplicationContext(), "Enter Your Email and Password", Toast.LENGTH_SHORT).show();
            } else {
               // Toast.makeText(getApplicationContext(), "DATA UPDATED", Toast.LENGTH_LONG).show();
                DatabaseReference databaseUsers = FirebaseDatabase.getInstance().getReference("users");
                String userID = sp.getString("UserID", null);


               // databaseUsers.child(userID).child("absid").setValue(a);
               // databaseUsers.child(userID).child("affiliation").setValue(aff);
               // databaseUsers.child(userID).child("country").setValue(count);
                databaseUsers.child(userID).child("name").setValue(n);
               // databaseUsers.child(userID).child("title").setValue(title);
              //  databaseUsers.child(userID).child("abstitle").setValue(n);
               // databaseUsers.child(userID).child("contact").setValue(count);
                databaseUsers.child(userID).child("password").setValue(p);
                SharedPreferences.Editor ed = sp.edit();
                ed.putString("Password", p);
             //   ed.putInt("AbstractID", Integer.parseInt(a));
                ed.putString("Name", n);
             //   ed.putString("Contact", con);
              //  ed.putString("Country", count);
              //  ed.putString("Affiliation", aff);
              //  ed.putString("Title", title);
               // ed.putString("AbstractTitle", abst);
                ed.commit();
                Toast.makeText(getApplicationContext(), "DATA UPDATED", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }
}

