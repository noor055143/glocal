package com.michael.glocal.models;
public class presenter {
    String PresenterName, Title, Strands, ID, Room, Time;
    public presenter() {
    }
    public  presenter(String name, String title, String strand, String id, String room, String time){
        this.PresenterName = name;
        this.Title = title;
        this.Strands = strand;
        this.ID = id;
        this.Room = room;
        this.Title = time;
    }
    public String getPresenterName() {
        return PresenterName;
    }
    public void setPresenterName(String presenterName) {
        PresenterName = presenterName;
    }
    public String getTitle() {
        return Title;
    }
    public void setTitle(String title) {
        Title = title;
    }
    public String getStrands() {
        return Strands;
    }
    public void setStrands(String strands) {
        Strands = strands;
    }
    public String getID() {
        return ID;
    }
    public void setID(String ID) {
        this.ID = ID;
    }
    public String getRoom() {
        return Room;
    }
    public void setRoom(String room) {
        Room = room;
    }
    public String getTime() {
        return Time;
    }
    public void setTime(String time) {
        Time = time;
    }
}
