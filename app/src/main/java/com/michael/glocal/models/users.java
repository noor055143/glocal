package com.michael.glocal.models;

public class users {
    public String userID;
    public String name;
    public String email;
    public String country;
    public String password;
   public  String title;
   public  String affiliation;
   public  String abstitle;
   public int absid;
   public String contact;
    public String loggedin;
    public String picture;


    public users() {
    }

    public users(String name, String email, String country, String password, String title, String affiliation, String abstitle, int absid, String contact, String picture, String userID) {
        this.name = name;
        this.email = email;
        this.country = country;
        this.password = password;
        this.title = title;
        this.affiliation = affiliation;
        this.abstitle = abstitle;
        this.absid = absid;
        this.contact = contact;
        this.picture = picture;
        this.userID = userID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getLoggedin() {
        return loggedin;
    }

    public void setLoggedin(String loggedin) {
        this.loggedin = loggedin;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getAbstitle() {
        return abstitle;
    }

    public void setAbstitle(String abstitle) {
        this.abstitle = abstitle;
    }

    public int getAbsid() {
        return absid;
    }

    public void setAbsid(int absid) {
        this.absid = absid;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
