package com.michael.glocal.models;

import java.util.ArrayList;

public class singleoption_model {

    int id1;
    String name1;

    public singleoption_model(int id1, String name1) {
        this.id1 = id1;
        this.name1 = name1;
    }

    public int getId1() {
        return id1;
    }

    public String getName1() {
        return name1;
    }
}
