package com.michael.glocal.models;

public class ColloquiaModel {
    String organizer, title,chair, strand,id;
    public ColloquiaModel() {
    }
    public ColloquiaModel(String organizer, String title, String chair, String strand, String id) {
        this.organizer = organizer;
        this.title = title;
        this.chair = chair;
        this.strand = strand;
        this.id = id;
    }
    public String getOrganizer()
    {
        return organizer;
    }
    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getChair() {
        return chair;
    }
    public void setChair(String chair) {
        this.chair = chair;
    }
    public String getStrand() {
        return strand;
    }
    public void setStrand(String strand) {
        this.strand = strand;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
}
