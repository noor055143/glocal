package com.michael.glocal.models;

import java.util.ArrayList;

public class pagemodel {

    ArrayList<singleoption_model> list;

    public pagemodel(ArrayList<singleoption_model> list) {
        this.list = list;
    }

    public ArrayList<singleoption_model> getList() {
        return list;
    }
}
