package com.michael.glocal.models;
public class ColloquiaPresenterModel {
    String presenter, title,organizer, chair, strand,id;
    public ColloquiaPresenterModel(String presenter, String title, String organizer, String chair, String strand, String id) {
        this.presenter = presenter;
        this.title = title;
        this.organizer = organizer;
        this.chair = chair;
        this.strand = strand;
        this.id = id;
    }
    public ColloquiaPresenterModel() {
    }
    public String getPresenter() {
        return presenter;
    }
    public void setPresenter(String presenter) {
        this.presenter = presenter;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getOrganizer() {
        return organizer;
    }
    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }
    public String getChair() {
        return chair;
    }
    public void setChair(String chair) {
        this.chair = chair;
    }
    public String getStrand() {
        return strand;
    }
    public void setStrand(String strand) {
        this.strand = strand;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
}
