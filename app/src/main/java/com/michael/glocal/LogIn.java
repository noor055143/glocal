package com.michael.glocal;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.annotations.Nullable;
import com.michael.glocal.Utils.NetworkUtil;
import com.michael.glocal.models.users;

import java.util.HashMap;
import java.util.concurrent.Executors;

public class LogIn extends Activity implements View.OnClickListener {

    android.widget.Button btnLogin, btnSignUp, btnForgotPassword;
    RelativeLayout relay1, relay2;
    Handler handler = new Handler();
    Runnable run = new Runnable() {
        @Override
        public void run() {
            relay1.setVisibility(View.VISIBLE);
            relay2.setVisibility(View.VISIBLE);
        }
    };
    final private int PERMISSION_REQUEST_CODE_NETWORK_ACCESS = 123;
    final private int PERMISSION_REQUEST_CODE_INTERNET = 124;
    final private int PERMISSION_REQUEST_CODE_READ_EXTERNAL_STORAGE = 125;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private FirebaseAuth mAuth;
    String login, pass;
    EditText et_email, et_pass;
    final FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference ref = database.getReference("users");
    SharedPreferences sp;
    boolean emailFound = false;
    private ProgressDialog loadingDialog;
    ImageButton MoveAsGuest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        askForPermission(Manifest.permission.INTERNET, PERMISSION_REQUEST_CODE_INTERNET, "You need to allow access to Internet");
        askForPermission(Manifest.permission.ACCESS_NETWORK_STATE, PERMISSION_REQUEST_CODE_NETWORK_ACCESS, "You need to allow access to Network State");
        askForPermission(Manifest.permission.READ_EXTERNAL_STORAGE, PERMISSION_REQUEST_CODE_READ_EXTERNAL_STORAGE, "You need to allow access to External Storage");
        askForPermission(Manifest.permission.ACCESS_FINE_LOCATION, MY_PERMISSIONS_REQUEST_LOCATION, "You need to allow access your Location");
        relay1 = (RelativeLayout) findViewById(R.id.rellay1);
        relay2 = (RelativeLayout) findViewById(R.id.rellay2);

        loadingDialog = new ProgressDialog(this);
        loadingDialog.setCancelable(false);
        loadingDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);


        btnLogin = (android.widget.Button) findViewById(R.id.btnLogin);
      //  btnSignUp = (android.widget.Button) findViewById(R.id.btnSignUp);
        btnForgotPassword = (android.widget.Button) findViewById(R.id.btnForgotPassword);
        MoveAsGuest = (ImageButton) findViewById(R.id.MoveAsGuest);
        MoveAsGuest.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
       // btnSignUp.setOnClickListener(this);
        btnForgotPassword.setOnClickListener(this);

        et_email = (EditText) findViewById(R.id.login_email);
        et_pass = (EditText) findViewById(R.id.login_password);

        sp = getSharedPreferences("user", Context.MODE_PRIVATE);
        SharedPreferences sp = getSharedPreferences("user", Context.MODE_PRIVATE);
        String isUserLoggedIn = sp.getString("LoggedIn","false");
        String CurrentUserID = sp.getString("UserID", "");
        if(isUserLoggedIn.equals("true")) {
            Intent i = new Intent(LogIn.this, MainActivity.class);
            finish();
        }
        if (CurrentUserID.length() > 0 ) {

                Intent i = new Intent(LogIn.this, MainActivity.class);
                i.putExtra("CurrentUser", CurrentUserID);
                startActivity(i);
                finish();

        }

// Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        handler.postDelayed(run, 2000);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        //updateUI(currentUser);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (NetworkUtil.startActivityIfNetworkIsNotConnected(this)) {
            finish();
            return;
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        NetworkUtil.canNetworkWatcherThreadKeepRunning = false;
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                login = et_email.getText().toString();
                pass = et_pass.getText().toString();

                if (TextUtils.isEmpty(login)) {
                    Toast.makeText(LogIn.this, "Enter Email", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(pass)) {
                    Toast.makeText(LogIn.this, "Enter Password", Toast.LENGTH_SHORT).show();
                    return;
                }
                loadingDialog.setMessage("Logging In, Please wait... ");
                loadingDialog.show();
                Executors.newSingleThreadExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        signIn();
                    }
                });
                //signIn(login, pass);

                break;
            //case R.id.btnSignUp:
            //    Intent SignupScreen = new Intent(LogIn.this, SignUp.class);
            //    startActivity(SignupScreen);
            //    break;
            case R.id.btnForgotPassword:
              //  Toast.makeText(LogIn.this, "Got Your New Password", Toast.LENGTH_SHORT).show();
                Intent ForgotPassword = new Intent(LogIn.this, ForgotPassword.class);
                startActivity(ForgotPassword);
                break;

            case R.id.MoveAsGuest:
                Intent intent = new Intent(LogIn.this, MainActivity.class);
                startActivity(intent);
                break;
        }
    }

    public void signIn() {
        final String login = et_email.getText().toString().toLowerCase();
        final String pass = et_pass.getText().toString().toLowerCase();
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Object o = postSnapshot.getValue();
                    HashMap u = (HashMap) o;

                    Object o2 = u.get("password");
                    String password = String.valueOf(o2).toLowerCase();
                    o2 = u.get("email");
                    String email = String.valueOf(o2).toLowerCase();

                    Object loggedin = u.get("loggedin");
                    String loggedinS = String.valueOf(loggedin);

                    if (email.equals(login) && password.equals(pass)&& loggedinS.equals("false")) {
                        SharedPreferences.Editor ed = sp.edit();
                        ed.putString("Email", email);

                        o2 = u.get("userID");
                        String uID = String.valueOf(o2);
                        ed.putString("UserID", uID);

                        DatabaseReference  databaseUsers = FirebaseDatabase.getInstance().getReference("users");

                        o2 = u.get("absid");
                        int abstractid = (Integer.parseInt(String.valueOf(o2)));
                        ed.putInt("AbstractID", abstractid);

                        o2 = u.get("name");
                        String name = String.valueOf(o2);
                        ed.putString("Name", name);

                        o2 = u.get("title");
                        String title = String.valueOf(o2);
                        ed.putString("Titele", title);

                        o2 = u.get("contact");
                        String contact = String.valueOf(o2);
                        ed.putString("Contact", contact);

                        o2 = u.get("country");
                        String country = String.valueOf(o2);
                        ed.putString("Country", country);

                        o2 = u.get("affiliation");
                        String affiliation = String.valueOf(o2);
                        ed.putString("Affiliation", affiliation);

                        o2 = u.get("abstitle");
                        String abstitle = String.valueOf(o2);
                        ed.putString("Abstract Title", abstitle);

                        o2 = u.get("picture");
                        String p = String.valueOf(o2);
                        ed.putString("Picture", p);

                        ed.putString("LoggedIn", "true");
                        if (ed.commit()) {
                            databaseUsers.child(uID).child("loggedin").setValue("true");

                            databaseUsers.child(uID).child("password").setValue("cala2020");
                            loadingDialog.hide();
                            emailFound=true;
                           // Toast.makeText(LogIn.this,"Logged in as "+ name, Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LogIn.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                          //  Toast.makeText(getApplicationContext(), "Authenticated", Toast.LENGTH_SHORT).show();
                        }
                    } else if(email.equals(login)) {
                        emailFound=true;
                        loadingDialog.hide();
                        if(!(password.equals(pass)))
                        {
                            Toast.makeText(getApplicationContext(), "Invalid Password", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Intent intent = new Intent(LogIn.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        //    Toast.makeText(getApplicationContext(), "Authenticated", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    }
                    //loadingDialog.hide();
                }
                if(emailFound==false)
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadingDialog.hide();
                            Toast.makeText(LogIn.this, "Invalid Email", Toast.LENGTH_SHORT).show();
                        }
                    });
                ref.removeEventListener(this);
                emailFound=false;
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void askForPermission(String permissionName, int permissionRequestCode, String askMessage) {
        int hasPermission = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            hasPermission = checkSelfPermission(permissionName);
        }
        if (hasPermission != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!shouldShowRequestPermissionRationale(permissionName)) {
                    showMessageOKCancel(askMessage,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{permissionName}, permissionRequestCode);
                                    }
                                }
                            });
                    return;
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{permissionName},
                        permissionRequestCode);
            }
            return;
        }
    }
    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(LogIn.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE_NETWORK_ACCESS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(LogIn.this, " Permission Granted", Toast.LENGTH_SHORT)
                            .show();
                    // Permission Granted put your code here
                } else {
                    // Permission Denied
                    Toast.makeText(LogIn.this, "Permission Denied", Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}