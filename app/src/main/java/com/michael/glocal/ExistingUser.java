package com.michael.glocal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.michael.glocal.Utils.NetworkUtil;

public class ExistingUser extends AppCompatActivity implements View.OnClickListener{
    Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_existing_user);
        btn = (Button) findViewById(R.id.goToLogin);
        btn.setOnClickListener(this);
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (NetworkUtil.startActivityIfNetworkIsNotConnected(this)) {
            finish();
            return;
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        NetworkUtil.canNetworkWatcherThreadKeepRunning = false;
    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.goToLogin) {
            Intent i = new Intent(ExistingUser.this, LogIn.class);
            startActivity(i);
        }

    }
}
