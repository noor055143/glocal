package com.michael.glocal;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DatabaseReference;
import com.michael.glocal.Utils.NetworkUtil;

import java.util.Random;
import java.util.concurrent.Executors;

public class EmailValidation extends AppCompatActivity implements View.OnClickListener{

    private Button btn1;
    boolean s;
    boolean status;
    private ProgressDialog progress;
    DatabaseReference databaseUsers, firedatabase;
    SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_validation);
        setTitle("EmailVeification");
        btn1 = (Button) findViewById(R.id.verifybtn);
        btn1.setOnClickListener(this);

        preferences = PreferenceManager.getDefaultSharedPreferences(this);

    }
    @Override
    protected void onResume() {
        super.onResume();
        if (NetworkUtil.startActivityIfNetworkIsNotConnected(this)) {
            finish();
            return;
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        NetworkUtil.canNetworkWatcherThreadKeepRunning = false;
    }
    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.verifybtn) {
            progress = new ProgressDialog(EmailValidation.this);
            progress.setMessage("Verifying email...");
            progress.show();

            Executors.newSingleThreadExecutor().execute(new Runnable() {
                @Override
                public void run() {
                    s = sendEmail();
                    if (s == true) {
                        progress.dismiss();
                        Intent in = new Intent(EmailValidation.this, CodeActivity.class);
                        startActivity(in);
                    } else {
                        new AlertDialog.Builder(EmailValidation.this)

                                .setTitle("Connection")
                                .setMessage("Your email is not verified.Go back and check your email or internet connection")
                                .setCancelable(true)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent i = new Intent(EmailValidation.this, EmailValidation.class);
                                        startActivity(i);
                                    }
                                }).show();

                    }
                }
            });
        }
    }

    protected boolean sendEmail() {
        try {
            Random r = new Random();
            int code = r.nextInt(1000) + 1;

            SharedPreferences.Editor ed = preferences.edit();
            ed.putInt("code", code);
            ed.commit();


            String email = preferences.getString("Email", "defaultValue");

            GMailSender sender = new GMailSender("recoverums123@gmail.com", "eadpassword");
            status = sender.sendMail("Email Verfication Code",Integer.toString(code) + " is your Glocal verification Code.","recoverums123@gmail.com",email);
        } catch (Exception e) {
            Log.e("NotSend", e.getMessage(), e);

        }
        return status;
    }
}
