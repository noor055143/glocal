package com.michael.glocal;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.michael.glocal.adapter.PageAdapter_main;
import com.michael.glocal.models.pagemodel;
import com.michael.glocal.models.singleoption_model;
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

import java.util.ArrayList;

public class COMELA extends AppCompatActivity {

    ViewPager menulist;
    SpringDotsIndicator dot;
    ArrayList<singleoption_model> list  = new ArrayList<>();
    ImageView back;
    //private ArrayList<singleoption_model> list2;
    //private ArrayList<singleoption_model> list3;
    private int dotscount;

    ArrayList<pagemodel> mainlist = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_comela);
        menulist = (ViewPager) findViewById(R.id.gridviewpager);
      //  dot = findViewById(R.id.spring_dots_indicator);
        back = (ImageView) findViewById(R.id.btnBack);
        singleoption_model m1 = new singleoption_model(R.drawable.ic_info,"OVERVIEW");
        singleoption_model m2 = new singleoption_model(R.drawable.ic_theme,"THEME");
        singleoption_model m3 = new singleoption_model(R.drawable.announcements,"ANNOUNCEMENTS");
        singleoption_model m4 = new singleoption_model(R.drawable.ic_agenda,"AGENDA");
        singleoption_model m5= new singleoption_model(R.drawable.ic_speaker,"SPEAKERS");
        singleoption_model m6 = new singleoption_model(R.drawable.ic_business_and_finance,"EVENT GAME");
        singleoption_model m7 = new singleoption_model(R.drawable.ic_location,"MAPS");
        singleoption_model m8 = new singleoption_model(R.drawable.ic_documents,"GENERAL PAPERS");
        singleoption_model m9 = new singleoption_model(R.drawable.ic_qr_code,"MY QR CODE");
        singleoption_model m11 = new singleoption_model(R.drawable.ic_central_committee,"CENTRAL COMMITTEE");
        singleoption_model m22 = new singleoption_model(R.drawable.ic_theme,"PRESENTATIONS");
        singleoption_model m33 = new singleoption_model(R.drawable.meeting,"COLLOQUIA");
        singleoption_model m44 = new singleoption_model(R.drawable.ic_poster,"POSTERS");

        list.add(m1);
        list.add(m2);
        list.add(m3);
        list.add(m4);
        list.add(m5);
        list.add(m6);
        list.add(m7);
        list.add(m8);
        list.add(m9);
        list.add(m11);
        list.add(m33);
        list.add(m44);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(COMELA.this, MainActivity.class);
                startActivity(i);
            }});
        pagemodel pagemodel = new pagemodel(list);
        mainlist.add(pagemodel);

        PagerAdapter pagerAdapter = new PageAdapter_main(mainlist,this);
        menulist.setAdapter(pagerAdapter);
      //  dotscount =  pagerAdapter.getCount();

      //  dot.setViewPager(menulist);
    }

}
