package com.michael.glocal.options;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.michael.glocal.R;

public class Schedule_activity extends AppCompatActivity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_schedule);

        Spinner scheduleSpinner = (Spinner) findViewById(R.id.scheduleSpinner);
        ArrayAdapter<String> scheduleAdapter = new ArrayAdapter<String>(Schedule_activity.this,
                android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.All_Events));
        scheduleAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        scheduleSpinner.setAdapter(scheduleAdapter);
    }
}
