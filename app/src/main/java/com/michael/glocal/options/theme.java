package com.michael.glocal.options;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.michael.glocal.R;

public class theme extends Activity {
    private SharedPreferences mprefrences;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.theme);
        setTitle("Theme");


        mprefrences = PreferenceManager.getDefaultSharedPreferences(this);
        String event_name = mprefrences.getString("Event", "default");
        //Toast.makeText(theme.this, "Event: " + event_name, Toast.LENGTH_SHORT).show();
        if (event_name.equals("CALA")) {
            TextView data = findViewById(R.id.data_textview);
            TextView subtitle = findViewById(R.id.subtitle);
            subtitle.setText("Asian Text, Global Context");
            data.setText("    The CALA 2020 theme, “Asian Text, Global Context”, describes a culmination of 300 years of East-West global communication. Throughouty, the symbolisms of Asian ‘text’ have been significantly emphasized, contested, (re)interpreted, and distorted, while employed for both political and Anthropological purpose. Asian text has become a highly representational, and legitimizing device, and its potency cannot be underestimated. Never has it shown more significance than in the current era, where its intensified usage, and its qualities in Asian identities, seek description.\n" +
                    "\n    The Asian text pervades the whole semiotic spectrum of that which is performatively Asian, and distinct from the Non-Asian, yet a text which can interlink the East and the West, through a multitude of textual modes. The continuous recentralization and recontextualization of Asian texts, both locally and globally, are hence vital to representations of Asia, Linguistically, Anthropologically, Socioculturally, Politically, and beyond.\n" +
                    "\n    The CALA 2020 thus calls for renewed interpretations of Asian texts, and asks that we seek new perspectives of these complex texts, in their global contexts. These interpretations increase in significance as; return migration to Asia is now a salient factor in transnational flows; online texts and their textual modes now compete ever more enthusiastically to effect disjunctures in previously Western dominated technologies; perspectives of life and social interaction now increasingly draw from Asia, producing spaces for revised textual semiotics; the intersubjectivities of political, sociocultural, and religious practices motivate dialogue, thus shifting ethnic demarcations, and sociopolitical interventions. Ultimately, Eastern demographics, and their social dynamics, continue to uniquely inform and complexify Asian texts, in both local and in global contexts.");
        }
        if(event_name.equals("Comela"))
        {
            TextView data = findViewById(R.id.data_textview);
            TextView subtitle = findViewById(R.id.subtitle);
            subtitle.setText("Bounded languages  … Unbounded");
            data.setText("    Politics of identity are central to language change. Here, linguistic boundaries rise and fall, motivating the ephemeral characteristics of language communities. The Mediterranean and European regions are replete with histories, with power struggles, uniquely demarcating nation, ethnicity, and community. For this, cultural and political identities, language ideologies, as well as the languages themselves, have sought boundedness, dynamics of which have indeed sought change over eons, through demographic movements, through geopolitics, through technological innovation. In a current era of technological advancement, transnational fluidity, intellectual power, capitalism, and new sexualities, then, we question, once again, the boundedness of language and identity, and ways in which to unbound languages and ideologies. More than before, we now increasingly pursue anthropological toil, so to innovate ways to locate these ideologies and their fluid boundaries, actively. We now need to increasingly unbind these languages, and their ideologies, so to arrive at progressive realizations, and to rectify, or at least see and move past, the segregations of old.\n" +
                    "\nThe COMELA 2020 theme,\n\n" +
                    "Bounded languages … Unbounded\n\n"+
                    "    Encapsulates the ongoing struggle throughout Mediterranean and European regions. As the continuous tension between demarcation, and the concurrent legitimization, of languages, language ideologies, and language identities, enters an era where new modes of interactivity require language communities to take on roles super-ordinate to the past, flexible citizenship now operates within, and not only across, language communities, to unbind languages, and to create new boundaries, unlike those ever seen throughout history.\n"
            + "    The COMELA 2020 invites work which addresses the shifting boundedness of Language Communities of the Mediterranean and Europe. Papers and posters should acknowledge and describe processes of language shape, change, and ideology, pertinent to social, cultural, political histories, and futures of Mediterranean and European regions, and by those working in Mediterranean and European regions.\n\n");
        }
    }
}
