package com.michael.glocal.options;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.icu.text.CaseMap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.michael.glocal.R;
import com.michael.glocal.models.presenter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class GeneralPapers extends AppCompatActivity implements View.OnClickListener {

    private SharedPreferences mprefrences;
    String event_name;
    TableLayout tableLayout;
    ArrayList<presenter> presenterList;

    Button  btnA,btnB,btnC,btnD,btnF,btnG,btnH,btnI,btnJ,btnK,btnL,btnM,btnN,btnP,btnQ,btnR,btnS,btnT,btnU,btnV,btnW,btnX,btnY,btnZ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general_papers);
        getSupportActionBar().hide();

        initViews();

        if(event_name.equals("CALA")) {
            CalaButtonA();
        }
        if(event_name.equals("Comela")) {
        }
    }
    void initViews(){
        tableLayout = (TableLayout) findViewById(R.id.table);
        btnA = (Button) findViewById(R.id.btnA);
        btnB = (Button) findViewById(R.id.btnB);
        btnC = (Button) findViewById(R.id.btnC);
        btnD = (Button) findViewById(R.id.btnD);
        btnF = (Button) findViewById(R.id.btnF);
        btnG = (Button) findViewById(R.id.btnG);
        btnH = (Button) findViewById(R.id.btnH);
        btnI = (Button) findViewById(R.id.btnI);
        btnJ = (Button) findViewById(R.id.btnJ);
        btnK = (Button) findViewById(R.id.btnK);
        btnL = (Button) findViewById(R.id.btnL);
        btnM = (Button) findViewById(R.id.btnM);
        btnN = (Button) findViewById(R.id.btnN);
        btnP = (Button) findViewById(R.id.btnP);
        btnQ = (Button) findViewById(R.id.btnQ);
        btnR = (Button) findViewById(R.id.btnR);
        btnS = (Button) findViewById(R.id.btnS);
        btnT = (Button) findViewById(R.id.btnT);
        btnU = (Button) findViewById(R.id.btnU);
        btnV = (Button) findViewById(R.id.btnV);
        btnW = (Button) findViewById(R.id.btnW);
        btnX = (Button) findViewById(R.id.btnX);
        btnY = (Button) findViewById(R.id.btnY);
        btnZ = (Button) findViewById(R.id.btnZ);

        //Set Onclick Listeners;
        btnA.setOnClickListener(this);
        btnB.setOnClickListener(this);
        btnC.setOnClickListener(this);
        btnD.setOnClickListener(this);
        btnF.setOnClickListener(this);
        btnG.setOnClickListener(this);
        btnH.setOnClickListener(this);
        btnI.setOnClickListener(this);
        btnJ.setOnClickListener(this);
        btnK.setOnClickListener(this);
        btnL.setOnClickListener(this);
        btnM.setOnClickListener(this);
        btnN.setOnClickListener(this);
        btnP.setOnClickListener(this);
        btnQ.setOnClickListener(this);
        btnR.setOnClickListener(this);
        btnS.setOnClickListener(this);
        btnT.setOnClickListener(this);
        btnU.setOnClickListener(this);
        btnV.setOnClickListener(this);
        btnW.setOnClickListener(this);
        btnX.setOnClickListener(this);
        btnY.setOnClickListener(this);
        btnZ.setOnClickListener(this);

        addHeaders();

        mprefrences = PreferenceManager.getDefaultSharedPreferences(this);
        event_name = mprefrences.getString("Event", "default");
    }
    public void addHeaders(){
        TableLayout t1 = (TableLayout) findViewById(R.id.table);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());

        tr.addView(getTextView(0, "Presenter", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "Title", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "Strand", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "ID    ", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "Room   ", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "Time   ", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));

        t1.addView(tr, getTblLayoutParams());
    }
    private TextView getTextView(int id, String Title, int color, int TypeFace, int bg){
        TextView tv = new TextView(this);
        tv.setId(id);
        tv.setText(Title.toUpperCase());
        tv.setTextColor(color);
        tv.setPadding(40,40,20,40);
        tv.setTypeface(Typeface.DEFAULT, TypeFace);
        tv.setBackgroundColor(bg);
        tv.setBackgroundResource(bg);
        tv.setLayoutParams(getLayoutParams());
        return tv;
    }
    private TextView getRowsTextView(int id, String Title, int color, int bg){
        TextView tv = new TextView(this);
        tv.setId(id);
        tv.setTextSize(12);
        tv.setText(Title);
        tv.setTextColor(color);
        tv.setPadding(40,40,20,40);
        tv.setTypeface(Typeface.DEFAULT);
        tv.setBackgroundColor(bg);
        tv.setBackgroundResource(bg);
        tv.setLayoutParams(getLayoutParams());
        return tv;
    }
    private TableRow.LayoutParams getLayoutParams(){
        TableRow.LayoutParams params = new TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(2,1,2,1);
        params.weight = 1;
        return  params;
    }
    private TableLayout.LayoutParams getTblLayoutParams() {
        return new TableLayout.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT
        );

    }
    void addRows(){
       // Collections.reverse(presenterList);
        for(int i=0; i < presenterList.size() ; i ++){
            TableRow tr= new TableRow(GeneralPapers.this);
            tr.setLayoutParams(getLayoutParams());

            tr.addView(getRowsTextView(0, presenterList.get(i).getPresenterName(), Color.BLACK, R.drawable.cell_shape));
            tr.addView(getRowsTextView(0, presenterList.get(i).getTitle(), Color.BLACK , R.drawable.cell_shape));
            tr.addView(getRowsTextView(0, presenterList.get(i).getStrands(), Color.BLACK, R.drawable.cell_shape));
            tr.addView(getRowsTextView(0, presenterList.get(i).getID(), Color.BLACK,R.drawable.cell_shape));
            tr.addView(getRowsTextView(0, presenterList.get(i).getRoom(), Color.BLACK,  R.drawable.cell_shape));
            tr.addView(getRowsTextView(0, presenterList.get(i).getTime(), Color.BLACK, R.drawable.cell_shape));

            tableLayout.addView(tr, getTblLayoutParams());
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btnA:
                if(event_name.equals("CALA")) {
                    CalaButtonA();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnB:
                if(event_name.equals("CALA")) {
                    CalaButtonB();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnC:
                if(event_name.equals("CALA")) {
                    CalaButtonC();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnD:
                if(event_name.equals("CALA")) {
                    CalaButtonD();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnF:
                if(event_name.equals("CALA")) {
                    CalaButtonF();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnG:
                if(event_name.equals("CALA")) {
                    CalaButtonG();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnH:
                if(event_name.equals("CALA")) {
                    CalaButtonH();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnI:
                if(event_name.equals("CALA")) {
                    CalaButtonI();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnJ:
                if(event_name.equals("CALA")) {
                    CalaButtonJ();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnK:
                if(event_name.equals("CALA")) {
                    CalaButtonK();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnL:
                if(event_name.equals("CALA")) {
                    CalaButtonL();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnM:
                if(event_name.equals("CALA")) {
                    CalaButtonM();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnN:
                if(event_name.equals("CALA")) {
                    CalaButtonN();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnP:
                if(event_name.equals("CALA")) {
                    CalaButtonP();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnQ:
                if(event_name.equals("CALA")) {
                    CalaButtonQ();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnR:
                if(event_name.equals("CALA")) {
                    CalaButtonR();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnS:
                if(event_name.equals("CALA")) {
                    CalaButtonS();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnT:
                if(event_name.equals("CALA")) {
                    CalaButtonT();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnU:
                if(event_name.equals("CALA")) {
                    CalaButtonU();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnV:
                if(event_name.equals("CALA")) {
                    CalaButtonV();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnW:
                if(event_name.equals("CALA")) {
                    CalaButtonW();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnX:
                if(event_name.equals("CALA")) {
                    CalaButtonX();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnY:
                if(event_name.equals("CALA")) {
                    CalaButtonY();
                }
                if(event_name.equals("Comela")) {
                }
                break;
            case R.id.btnZ:
                if(event_name.equals("CALA")) {
                    CalaButtonZ();
                }
                if(event_name.equals("Comela")) {
                }
                break;
        }

    }
    void removeRows(){
        if(tableLayout.getChildCount() != 0) {
            while (tableLayout.getChildCount() > 1) {
                TableRow row = (TableRow) tableLayout.getChildAt(1);
                tableLayout.removeView(row);
            }
        }

    }
    void CalaButtonA(){
        removeRows();
        //make List of presenters which needs to be show in table
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();
        presenter p5 = new presenter();
        presenter p6 = new presenter();
        presenter p7 = new presenter();
        presenter p8 = new presenter();
        presenter p9 = new presenter();
        presenter p10 = new presenter();
        presenter p11 = new presenter();
        presenter p12 = new presenter();
        presenter p13 = new presenter();
        presenter p14 = new presenter();
        presenter p15 = new presenter();
        presenter p16 = new presenter();

        //set all attributes
        p1.setPresenterName("Presenter: Abdullah, Md Abu Shahid\n" +
                "Author: Abdullah, Md Abu Shahid");
        p1.setTitle("Of the Women, by the Women, for the \n Women: Role of Female Writers in Sanskrit \n Literature as a Source of Female Agency");
        p1.setStrands("Language,\n Gender, Sexuality");
        p1.setID("32");
        p1.setRoom(".");
        p1.setTime(".");

        //set all attributes
        p2.setPresenterName("Presenter: Abdullah, Md Abu Shahid\n" +
                "Author: Abdullah, Md Abu Shahid");
        p2.setTitle("(In)ability to Assert Independence and Freedom \nof Choice: A Comparative Study of the Treatment\n" +
                " of Women in British and Bengali Ballads");
        p2.setStrands("Language,\n Gender, Sexuality");
        p2.setID("33");
        p2.setRoom(".");
        p2.setTime(".");

        //p3
        p3.setPresenterName(" Presenter: Adihartono, Wisnu\n Author: Adihartono, Wisnu");
        p3.setTitle("Gay Language or Lavender Linguistics in \nIndonesia");
        p3.setStrands("Language, \nGender, Sexuality");
        p3.setID("11");
        p3.setRoom(".");
        p3.setTime(".");

        //p4
        p4.setPresenterName("Presenter: Adhikari, Haris Chand\n" +
                "Author: Adhikari, Haris Chand");
        p4.setTitle("Reflectivity, Reflexivity, and Interdisciplinarity in \nNepali Academic Context");
        p4.setStrands("Multifunctionality");
        p4.setID("59");
        p4.setRoom(".");
        p4.setTime(".");

        //p5
        p5.setPresenterName("Presenters: Adlina, Abdul Samad; \nAminabibi, Saidalvi\n" +
                "Authors: Adlina, Abdul Samad;\n Aminabibi, Saidalvi");
        p5.setTitle("Online Peer Corrective Feedback: Innovation \nin a Public Speaking Course");
        p5.setStrands("Anthropological\n Linguistics");
        p5.setID("239");
        p5.setRoom("R.05");
        p5.setTime("Thurs 08:30 – 10:00");

        //p6
        p6.setPresenterName("Presenter: Afifah, Hashim\n" +
                "Author: Afifah, Hashim");
        p6.setTitle("Are Control Messages in English and Malay \nContextually-Independent?");
        p6.setStrands("Semiotics and\n Semiology");
        p6.setID("112");
        p6.setRoom(".");
        p6.setTime(".");

        //p7
        p7.setPresenterName("Presenter: Allen, Todd\n" +
                "Author: Allen, Todd");
        p7.setTitle("The Linguistic Activities and Cultural \nSignificance of the Izakaya");
        p7.setStrands("Language,\n Community,\n Ethnicity");
        p7.setID("31");
        p7.setRoom(".");
        p7.setTime(".");

        //p8
        p8.setPresenterName("Presenters: Amery, Robert; Aziz,\n Zulfadli A.\n" +
                "Authors: Amery, Robert; Aziz,\n Zulfadli A.");
        p8.setTitle("The Languages of Pulau Simeulue and Pulau \nBanyak: Mutated Forms Point to Southerly\n Origins");
        p8.setStrands("Language,\n Community,\n Ethnicity");
        p8.setID("71");
        p8.setRoom(".");
        p8.setTime(".");

        //p9
        p9.setPresenterName("Presenters: Aminabibi, Saidalvi;\n Adlina, Abdul Samad\n" +
                "Authors: Adlina, Abdul Samad;\n Aminabibi, Saidalvi");
        p9.setTitle("Online Peer Corrective Feedback: Innovation\n in a Public Speaking Course");
        p9.setStrands("Anthropological \nLinguistics");
        p9.setID("239");
        p9.setRoom("R.05");
        p9.setTime("Thurs 08:30 – 10:00");

        //p10
        p10.setPresenterName("Presenter: Ansary, Hifzur Rahman\n" +
                "Author: Ansary, Hifzur Rahman");
        p10.setTitle("The verb \'say\' in Dravidian Languages: A study\n in Grammaticalisation");
        p10.setStrands("Language Contact \nand Change");
        p10.setID("210");
        p10.setRoom(".");
        p10.setTime(".");
        //p11
        p11.setPresenterName("Presenter: Aoyama, Reijiro\n" +
                "Author: Aoyama, Reijiro");
        p11.setTitle("Beyond a method of written communication:\n The value and power of Chinese characters in\n Japanese socio-historical domains");
        p11.setStrands("Language \nIdeologies");
        p11.setID("50");
        p11.setRoom("R.01");
        p11.setTime("Wed 17:00 – 18:30");
        //p12
        p12.setPresenterName("Presenter: Archana T.R\n" +
                "Author: Archana T.R");
        p12.setTitle("Edakkal Cave Paintings, Tiger Cult and Tribal \nLife; An Anthropo- Linguistical Analysis on \nMullukuruma Tribe");
        p12.setStrands("Anthropological \nLinguistics");
        p12.setID("191");
        p12.setRoom(".");
        p12.setTime(".");

        //p13
        p13.setPresenterName("Presenter: Ayala, Susana\n" +
                "Author: Ayala, Susana");
        p13.setTitle("Becoming Puppeteer: Reflections on Language\n and Culture in the Global Context by Puppetry \nStudents in Yogyakarta, Indonesia");
        p13.setStrands("Language, \nCommunity,\n Ethnicity");
        p13.setID("204");
        p13.setRoom("R.04");
        p13.setTime("Fri 15:00 – 16:30");

        //p14
        p14.setPresenterName("Presenters: Aziz, Zulfadli A.; Amery, Robert\n" +
                "Authors: Amery, Robert; Aziz, Zulfadli A.");
        p14.setTitle("The Languages of Pulau Simeulue and Pulau \nBanyak: Mutated Forms Point to Southerly\n Origins");
        p14.setStrands("Language, \nCommunity, \nEthnicity");
        p14.setID("71");
        p14.setRoom(".");
        p14.setTime(".");

        //p15
        p15.setPresenterName("Presenter: Azwahanum, Nor Shaid, Nor\n" +
                "Authors: Azwahanum, Nor Shaid, Nor;\n Marlyna, Maros; Shahidi, \nAbd Hamid; Mohd, Sharifudin Yusop");
        p15.setTitle("Sociocultural Influences of Indigenous\n Teenager in Second Language Acquisition\n (SLA) Experiences in Perak");
        p15.setStrands("Social Psychology \nof Language");
        p15.setID("156");
        p15.setRoom(".");
        p15.setTime(".");

        //p16
        p16.setPresenterName("Presenter: Azyyati, Nurina\n" +
                "Author: Azyyati, Nurina");
        p16.setTitle("The Emergence of The New Identity of \'Emak-emak\n (Mothers)\' in Indonesian 2019\n Presidential Election");
        p16.setStrands("Semiotics and \nSemiology");
        p16.setID("104");
        p16.setRoom(".");
        p16.setTime(".");

        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);
        presenterList.add(p5);
        presenterList.add(p6);
        presenterList.add(p7);
        presenterList.add(p8);
        presenterList.add(p9);
        presenterList.add(p10);
        presenterList.add(p11);
        presenterList.add(p12);
        presenterList.add(p13);
        presenterList.add(p14);
        presenterList.add(p15);
        presenterList.add(p16);

        addRows();
    }
    void CalaButtonB(){
        removeRows();

        //make List of presenters which needs to be show in table
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();

        //set all attributes
        p1.setPresenterName("Presenters: Bandyopadhyay, Sumahan; \nChatterjee, Doyel\n" +
                "Authors: Bandyopadhyay, Sumahan;\n Chatterjee, Doyel");
        p1.setTitle("Texts and Performative Tradition:\n Tribal Festivals in the Perspectives \n" +
                "of Tribalism, De-colonisation and \nGlobalization in Eastern India");
        p1.setStrands("Text, Context,\n Entextualization");
        p1.setID("99");
        p1.setRoom(".");
        p1.setTime(".");

        p2.setPresenterName("Presenter: Bhat, Rajnath\n" +
                "Author: Bhat, Rajnath");
        p2.setTitle("Language Culture and Heritage");
        p2.setStrands("Anthropological\n Linguistics");
        p2.setID("72");
        p2.setRoom(".");
        p2.setTime(".");

        p3.setPresenterName("Presenter: Bolivar, Loyalda T.\n" +
                "Author: Bolivar, Loyalda T.");
        p3.setTitle("Rain or Shine Shield: Language and \n" +
                "Ropes of Sadok Making");
        p3.setStrands("Anthropological \nLinguistics");
        p3.setID("125");
        p3.setRoom("R.01");
        p3.setTime("Wed \n17:00 – 18:30");

        p4.setPresenterName("Presenter: Burkhardt, Jürgen M.\n" +
                "Author: Burkhardt, Jürgen M.");
        p4.setTitle("Nasal fortitioning of undergoer\n markers in Long Jegan and\n Long Teru Berawan");
        p4.setStrands("Language, \nCommunity,\n Ethnicity");
        p4.setID("54");
        p4.setRoom(".");
        p4.setTime(".");

        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);

        addRows();
    }
    void CalaButtonC(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();
        presenter p5 = new presenter();
        presenter p6 = new presenter();
        presenter p7 = new presenter();
        presenter p8 = new presenter();
        presenter p9 = new presenter();
        presenter p10 = new presenter();
        presenter p11 = new presenter();
        presenter p12 = new presenter();
        presenter p13 = new presenter();
        presenter p14 = new presenter();
        presenter p15 = new presenter();
        presenter p16 = new presenter();
        presenter p17 = new presenter();
        presenter p18 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenter: C., Shabarinath\n" +
                "Author: C., Shabarinath");
        p1.setTitle("Dynamic Museum of Myths and\nNarratives: To" +
                " Learn and To Teach the \nCommunity Through" +
                " Languages. Re-\nConstruction of Kirtads\n" +
                "Ethnological Museum and The \nImportance of " +
                "Language Narratives.\n An Anthropo-Linguistics " +
                "Study");
        p1.setStrands("Anthropological \nLinguistics");
        p1.setID("198");
        p1.setRoom(".");
        p1.setTime(".");
        p2.setPresenterName("Presenters: Cabrillos, Edbert Jay M.; \nCabrillos, Rowena\n" +
                "Authors: Cabrillos, Edbert Jay M.; \nCabrillos, Rowena");
        p2.setTitle("Makan: Diversifying Ethno-\nlinguistic Heritage Through \nthe Art of Bamboo Weaving");
        p2.setStrands("Ethnographical Language Work");
        p2.setID("46");
        p2.setRoom("R.02");
        p2.setTime("Thurs\n 15:00 – 16:30");

        p3.setPresenterName("Presenters: Cabrillos, Edbert Jay M.\n" +
                "Authors: Cabrillos, Edbert \nJay M.; Cabrillos, Rowena");
        p3.setTitle("Pagdihon: The Art and Language of \nPottery Making in Bari, Sibalom, Antique");
        p3.setStrands("Ethnographical Language Work");
        p3.setID("63");
        p3.setRoom("R.05");
        p3.setTime("Fri\n15:00 – 16:30");

        p4.setPresenterName("Presenters: Cabrillos, Rowena;\n Cabrillos, Edbert Jay M.\n" +
                "Authors: Cabrillos, Rowena;\n Cabrillos, Edbert Jay M.");
        p4.setTitle("Makan: Diversifying Ethno-linguistic \nHeritage Through the Art of Bamboo Weaving");
        p4.setStrands("Ethnographical Language Work");
        p4.setID("46");
        p4.setRoom("R.02");
        p4.setTime("Thurs\n15:00 – 16:30");

        p5.setPresenterName("Presenter: Cao, Kim Lan\n" +
                "Author: Cao, Kim Lan");
        p5.setTitle("Pagdihon: The Art and Language of \nPottery Making in Bari, \nSibalom, Antique");
        p5.setStrands("Ethnographical\n Language Work");
        p5.setID("63");
        p5.setRoom("R.05");
        p5.setTime("Fri\n 15:00 – 16:30");

        p6.setPresenterName("Presenters: Cao, Thi Thu Hoai; \nHoang, Thi My Hanh\n" +
                "Authors: Hoang, Thi My Hanh;\n Cao, Thi Thu Hoai");
        p6.setTitle("Nguyen Xuan Khanh’s Novel as a \nNarrative of the Mother Goddess Religion \nand this " +
                "Indigenous Religion’s\n effects in a New Context \nin Vietnam Today");
        p6.setStrands("Narrative and \nMetanarrative");
                p6.setID("87");
        p6.setRoom("R.02");
        p6.setTime("Wed\n15:00 – 16:30");

        p7.setPresenterName("Presenter:Carstens, Sharon \n Author: Carstens, Sharon");
        p7.setTitle("Languaging Resistance and \nAccommodation in Chinese \nMalaysian Political Speech");
        p7.setStrands("Language, \nCommunity,\n Ethnicity");
        p7.setID("122");
        p7.setRoom("R.04");
        p7.setTime("Fri \n15:00 – 16:30");

        p8.setPresenterName("Presenter:Casibual, Joseph P.\nAuthor: Casibual, Joseph P.");
        p8.setTitle("Redefining Success: Exploring Filipino\n Ideological Expressions of Success \n" +
                "in Local Congratulatory Bidangan and \nTarpaulins");
        p8.setStrands("Language,\n Community,\n Ethnicity");
        p8.setID("106");
        p8.setRoom(".");
        p8.setTime(".");

        p9.setPresenterName("Presenter:Casibual, Joseph P.\nAuthor: Casibual, Joseph P.");
        p9.setTitle("The Aswang Complex: Deconstructing \nPredominant Thematic Focus in Hiligaynon \nFolk Narratives");
        p9.setStrands("Anthropological \nLinguistics");
        p9.setID("107");
        p9.setRoom(".");
        p9.setTime(".");

        p10.setPresenterName("Presenter:Cauilan, Fermila\nAuthor: Cauilan, Fermila");
        p10.setTitle("Rama’: A Locally-Based Practice \nin a Philippine Fishing Community");
        p10.setStrands("Language \nCommunity\n Ethnicity");
        p10.setID("199");
        p10.setRoom(".");
        p10.setTime(".");

        p11.setPresenterName("Presenters: Chatterjee,\n Doyel; Bandyopadhyay, Sumahan\nAuthors: Bandyopadhyay, Sumahan; \nChatterjee, Doyel");
        p11.setTitle("Texts and Performative Tradition:\n Tribal Festivals in the Perspectives of\n" +
                " Tribalism, De-colonisation and \nGlobalization in Eastern India");
        p11.setStrands("Text, \nContext,\n Entextualization");
        p11.setID("99");
        p11.setRoom(".");
        p11.setTime(".");

        p12.setPresenterName("Presenter: Chatterjee, Doyel\nAuthors: Bandyopadhyay, Sumahan;\n Chatterjee, Doyel");
        p12.setTitle("A Paradigmatic Shift from\n Dalitism to Neo Dalitism in\n the Globalised Context:" +
                " A study\n of Bama’s Karukku and Sangati");
        p12.setStrands("Text, \nContext,\n Entextualization");
        p12.setID("118");
        p12.setRoom(".");
        p12.setTime(".");

        p13.setPresenterName("Presenter: Chawadha, Vanita\nAuthor: Chawadha, Vanita");
        p13.setTitle("Contextual Treatment in Language\n Through Socialization Analyzing \nManner vs. Rudeness");
        p13.setStrands("Anthropological\n Linguistics");
        p13.setID("226");
        p13.setRoom(".");
        p13.setTime(".");

        p14.setPresenterName("Presenter: Chen, Ping-Hsueh\nAuthor: Chen, Ping-Hsueh");
        p14.setTitle("The French causative lexicon\n and its equivalents in Chinese: \nCorpus, Methodology, Results");
        p14.setStrands("Anthropological\n Linguistics");
        p14.setID("234");
        p14.setRoom(".");
        p14.setTime(".");

        p15.setPresenterName("Presenter: Chonpairot, Jarernchai\n  Author: Chonpairot, Jarernchai");
        p15.setTitle("Pha Nya: A Cultural Treasure of \nthe Folks");
        p15.setStrands("Language \nSocialization");
        p15.setID("66");
        p15.setRoom(".");
        p15.setTime(".");

        p16.setPresenterName("Presenter: Chowdhury, Arif\nAuthor: Chowdhury, Arif");
        p16.setTitle("Reading Shopfront Signs:\n A Multimodal (Social) Semiotic\n Approach to Text Analysis");
        p16.setStrands("Semiotics \nand Semiology");
        p16.setID("235");
        p16.setRoom("R.02");
        p16.setTime("Fri \n08:30 – 10:00");

        p17.setPresenterName("Presenter: Chowdhury, Nilanjana\n Author: Chowdhury, Nilanjana");
        p17.setTitle("A Detailed Study on the process \nof Word Formation in Rabha");
        p17.setStrands("Language Documentation ");
                p17.setID("217");
        p17.setRoom("R.01");
        p17.setTime("Fri\n 08:30 – 10:00");

        p18.setPresenterName("Presenter: Clarke, Sean\n Authors: Clarke, Sean; \nAthavale, Jayant Balaji");
        p18.setTitle("Spiritual vibrations of the\n most popular languages and \ntheir scripts");
        p18.setStrands("Critical \nLinguistic\n Anthropology");
        p18.setID("209");
        p18.setRoom("R.05");
                p18.setTime("Fri\n 13:00 – 14:30");

        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);
        presenterList.add(p5);
        presenterList.add(p6);
        presenterList.add(p7);
        presenterList.add(p8);
        presenterList.add(p9);
        presenterList.add(p10);
        presenterList.add(p11);
        presenterList.add(p12);
        presenterList.add(p13);
        presenterList.add(p14);
        presenterList.add(p15);
        presenterList.add(p16);
        presenterList.add(p17);
        presenterList.add(p18);
        addRows();
    }
    void CalaButtonD(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();
        presenter p5 = new presenter();
        presenter p6 = new presenter();
        presenter p7 = new presenter();
        presenter p8 = new presenter();
        presenter p9 = new presenter();
        presenter p10 = new presenter();
        presenter p11 = new presenter();
        presenter p12 = new presenter();
        presenter p13 = new presenter();
        presenter p14 = new presenter();
        presenter p15 = new presenter();


        //set all attributes
        p1.setPresenterName("Presenter: Dąbrowska, Marta\n" +
                "Author: Dąbrowska, Marta");
        p1.setTitle(" \"Safe Drive, Save Life.\" English as a Tool in the \nPromotion of Social Change in India’s Public \nSpace");
        p1.setStrands("General \nSociolinguistics");
        p1.setID("76");
        p1.setRoom("R.01");
        p1.setTime("Fri 15:00 – 16:30");

        p2.setPresenterName("Presenter: Dalumpines, Daniela Julia\n" +
                "Author: Dalumpines, Daniela Julia");
        p2.setTitle("Political Remembrance and Montage \nof Filipino Ideological Formation\n" +
                " in Gina Apostol’s Historiographic\n Metafiction Gun Dealers’ Daughter");
        p2.setStrands("Textualization,\n Contextualization, \nEntextualization\n");
        p2.setID("100");
        p2.setRoom(".");
        p2.setTime(".");

        p3.setPresenterName("Presenter: Dalumpines, Daniela Julia\n" +
                "Author: Dalumpines, Daniela Julia");
        p3.setTitle("Filipino Language Ideology:\n Exploring Gendered Voices \n" +
                "in five Filipino novels");
        p3.setStrands("Language \nIdeologies");
        p3.setID("105");
        p3.setRoom(".");
        p3.setTime(".");

        p4.setPresenterName("Presenter: Dang, \nThi Dieu Trang\n" +
                "Author: Dang, \nThi Dieu Trang");
        p4.setTitle("Linguistics in the \nCommunication Culture of \nVietnamese Teenagers Today");
        p4.setStrands("Language \nSocialization");
        p4.setID("41");
        p4.setRoom(".");
        p4.setTime(".");


        p5.setPresenterName("Presenter: Darragon,\n Martine Frederique\n" +
                "Author: Darragon, \nMartine Frederique");
        p5.setTitle("On Chinese Characters, \nThe Alphabet Effect,\n" +
                "Fuzzy Logic,\n and Beyond Language Relativism");
        p5.setStrands("Cognitive \nAnthropology and \nLanguage");
        p5.setID("223");
        p5.setRoom(".");
        p5.setTime(".");

        p6.setPresenterName("Presenter: Daschaudhuri,\n Mohar\n" +
                "Author: Daschaudhuri,\n Mohar");
        p6.setTitle("Re-writing the story of Draupadi: \n" +
                "An Analysis of The Veil of Draupadi \n" +
                "(Le voile de Draupadi by Ananda Devi\n" +
                " (Mauritius) and Yajnaseni\n" +
                " by Pratibha Ray (India)");
        p6.setStrands("Language, \n" +
                "Gender,\n Sexuality");
        p6.setID("86");
        p6.setRoom(".");
        p6.setTime(".");

        p7.setPresenterName("Presenter: de \nSilva, Nedha\n" +
                "Author: de \nSilva, Nedha");
        p7.setTitle("The Role of Emotion on Identity\n" +
                " Negotiation of Undergraduates in \nEnglish Language Usage \nin Academia");
        p7.setStrands("Applied \nSociolinguistics");
        p7.setID("172");
        p7.setRoom(".");
        p7.setTime(".");

        p8.setPresenterName("Presenters: De Sousa,\n Silvio Moreira;\n Raan-Hann, Tan\n" +
                "Authors: Raan-Hann, Tan;\n De Sousa, \nSilvio Moreira");
        p8.setTitle("The Shades of Being Eurasian\n" +
                " and Its Reverberations among \nthe Community in Malaysia");
        p8.setStrands("Anthropological \nLinguistics");
        p8.setID("83");
        p8.setRoom("R.05");
        p8.setTime("Wed \n15:00 – 16:30");

        p9.setPresenterName("Presenters: De Sousa, \nSilvio Moreira; Raan-Hann, Tan\n" +
                "Authors: Raan-Hann, Tan; De Sousa, \nSilvio Moreira");
        p9.setTitle("Language shift and terms of \naddress of two Malayo-Portuguese\n" +
                " Creole communities in\n Malaysia and Indonesia");
        p9.setStrands("Language Contact \nand Change");
        p9.setID("158");
        p9.setRoom("R.04");
        p9.setTime("Fri 13:00 – 14:30");

        p10.setPresenterName("Presenter: Do,\n Thu Hien\n" +
                "Author: Do,\n Thu Hien");
        p10.setTitle("Women in Truyền Kỳ Mạn Lục\n and Truyền Kỳ Tân Phả: A \n" +
                "Feminist Perspective");
        p10.setStrands("Language, \nGender,\n Sexuality");
        p10.setID("123");
        p10.setRoom("R.05");
        p10.setTime("Fri\n 08:30 – 10:00");

        p11.setPresenterName("Presenter: Dubuisson,\n Eva-Marie\n" +
                "Author: Dubuisson,\n Eva-Marie");
        p11.setTitle("The Pragmatic\n Recontextualization of \nOral Tradition in Central\n Asia");
        p11.setStrands("Textualization,\n Contextualization, \nEntextualization");
        p11.setID("90");
        p11.setRoom("R.01");
        p11.setTime("Fri 13:00 – 14:30");

        p12.setPresenterName("Presenter: Dulnuan, Romana\n" +
                "Author: Dulnuan, Romana");
        p12.setTitle("Honga, Bolwa, Katlu:\n A Cultural Analysis of The \nIfugao Traditions");
        p12.setStrands("Anthropological\n Linguistics");
        p12.setID("186");
        p12.setRoom(".");
        p12.setTime(".");

        p13.setPresenterName("Presenter: Dutta, Shuvam\n" +
                "Author: Dutta, Shuvam");
        p13.setTitle("Attitude and Endangerment: \nUnderstanding from Field Work\n among Lodha Speakers");
        p13.setStrands("Language\n Contact\n and Change");
        p13.setID("26");
        p13.setRoom("R.04");
        p13.setTime("Fri 13:00 – 14:30");

        p14.setPresenterName("Presenter: Dutta, Shuvam\n" +
                "Author: Dutta, Shuvam");
        p14.setTitle("Case Marking of Rava in \nComparison with Bangla");
        p14.setStrands("Language\n Documentation");
        p14.setID("194");
        p14.setRoom("R.01");
        p14.setTime("Fri 08:30 – 10:00");

        p15.setPresenterName("Presenters: Duwadi, Eak Prasad; Rijal, Sindhu\n" +
                "Authors: Duwadi, Eak Prasad; Rijal, Sindhu");
        p15.setTitle("English in Nepal:\n Phoenix or Dinosaur?");
        p15.setStrands("Language\n Minorities\n and Majorities");
        p15.setID("6");
        p15.setRoom(".");
        p15.setTime(".");

        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);
        presenterList.add(p5);
        presenterList.add(p6);
        presenterList.add(p7);
        presenterList.add(p8);
        presenterList.add(p9);
        presenterList.add(p10);
        presenterList.add(p11);
        presenterList.add(p12);
        presenterList.add(p13);
        presenterList.add(p14);
        presenterList.add(p15);

        addRows();
    }
    //Completed
    void CalaButtonF(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();

        //set all attributes
        p1.setPresenterName("Presenter: Fedorova, Kapitolina\n" +
                "Author: Fedorova, Kapitolina");
        p1.setTitle("Between global and local contexts: Seoul \nlinguistic landscape");
        p1.setStrands("Language and \nSpatiotemporal \nFrames");
        p1.setID("43");
        p1.setRoom("R.01");
        p1.setTime("Thurs 08:30 – 10:00");

        p2.setPresenterName("Presenter: Firtha, Ayu Rachmasari\n" +
                "Author: Firtha, Ayu Rachmasari");
        p2.setTitle("Roland Barthes’ Semiotic Theory in “Menikmati\n Akhir Pekan” Poetry by M. Aan Mansyur");
        p2.setStrands("Semiotics and \nSemiology");
        p2.setID("147");
        p2.setRoom(".");
        p2.setTime(".");

        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        addRows();
    }
    void CalaButtonG(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();
        presenter p5 = new presenter();
        presenter p6 = new presenter();

        //set all attributes
        p1.setPresenterName("Presenter: Gallelli, Beatrice\n" +
                "Author: Gallelli, Beatrice");
        p1.setTitle("Whose dream? Renmin (people) as a floating \nsignifier in contemporary China");
        p1.setStrands("Language \nIdeologies");
        p1.setID("189");
        p1.setRoom("R.04");
        p1.setTime("Thurs 15:00 – 16:30");

        //add all presenters in list
        p2.setPresenterName("Presenter: Geka, Aoi\n" +
                "Author: Geka, Aoi");
        p2.setTitle("Topicality of the \n" +
                "Copula Form Shi \nin Santa Mongolian");
        p2.setStrands("Language Contact and Change");
        p2.setID("64");
        p2.setRoom(".");
        p2.setTime(".");

        p3.setPresenterName("Presenters: Genato, Angel L.; \nJandoc, Maria Rousselle G.\n" +
                "Authors: Jandoc, Maria\n Rousselle G.; \nGenato, Angel L.");
        p3.setTitle("Students’ Gender and Sexuality \nin Nueva Vizcaya State \nUniversity-Bayombong Campus");
        p3.setStrands("Language,\n Gender,\n Sexuality");
        p3.setID("254");
        p3.setRoom("R.04");
        p3.setTime("Wed 15:00 – 16:30");

        p4.setPresenterName("Presenter: Genon,\n Lynrose Jane\n" +
                "Author: Genon, \nLynrose Jane");
        p4.setTitle("Performing Leadership: \nIntersectionality of Gender\n and Ethnicity in the Narratives \n" +
                "of Meranao Women Leaders in Mindanao");
        p4.setStrands("Language,\n Gender,\n Sexuality");
        p4.setID("179");
        p4.setRoom("R.05 ");
        p4.setTime("Fri 08:30 – 10:00");

        p5.setPresenterName("Presenter:\n Ghosh, Aditi\n" +
                "Author: Ghosh,\n Aditi");
        p5.setTitle("A study of language attitude\n among non-Bengali speakers \nin Kolkata");
        p5.setStrands("General\n Sociolinguistics");
        p5.setID("37");
        p5.setRoom(".");
        p5.setTime(".");

        p6.setPresenterName("Presenter: Guo, Xiaohui\n" +
                "Authors: Guo, Xiaohui; \nAng, Lay Hoon; Sabariah,\n Hj Md Rashid; Ser,\n Wue Hiong");
        p6.setTitle("A Study on Chinese\n Images in Bian Cheng");
        p6.setStrands("Anthropological\n Linguistics\n");
        p6.setID("102");
        p6.setRoom("");
        p6.setTime("");

        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);
        presenterList.add(p5);
        presenterList.add(p6);
        addRows();
    }
    //completed
    void CalaButtonH(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();
        presenter p5 = new presenter();
        presenter p6 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenter: Ha, Thi Mai Thanh\n" +
                "Author: Ha, Thi Mai Thanh");
        p1.setTitle("Polysemy of words expressing human body\n parts of the middle part area in Thai language \nin Vietnam (with reference to Vietnamese)");
        p1.setStrands("Language \nMinorities and\n Majorities");
        p1.setID("74");
        p1.setRoom(".");
        p1.setTime(".");


        //add all presenters in list
        p2.setPresenterName("Presenter: Hamida, Layli\n" +
                "Author: Hamida, Layli");
        p2.setTitle("The Impact of Children \nYoutube Videos on English\n Language Socialization and \nAcquisition in Indonesia");
        p2.setStrands("Anthropological\n Linguistics");
        p2.setID("230");
        p2.setRoom("R.04");
        p2.setTime("Thurs 08:30 – 10:00");

        p3.setPresenterName("Presenter: Han, Yanmei\n" +
                "Author: Han, Yanmei");
        p3.setTitle("Place Making with Cantonese \n" +
                "in the Linguistic Landscapes of \n" +
                "Guangzhou: Beyond Orders\n and Borders");
        p3.setStrands("General \nSociolinguistics\n");
        p3.setID("52");
        p3.setRoom("R.01");
        p3.setTime("Fri\n 15:00 – 16:30");

        p4.setPresenterName("Presenters: Harjanto, \nIgnatius; Lie, Anita\n" +
                "Authors: Harjanto,\n Ignatius; Lie, Anita;\n Wijaya, Juliana");
        p4.setTitle("English Text, \nGlobal Context,\n Indonesian Identity");
        p4.setStrands("Anthropological \nLinguistics");
        p4.setID("240");
        p4.setRoom("R.02");
        p4.setTime("Fri \n08:30 – 10:00");

        p5.setPresenterName("Presenters: Hoang, \nThi My Hanh; Cao, \nThi Thu Hoai\n" +
                "Authors: Hoang,\n Thi My Hanh; Cao,\n Thi Thu Hoai");
        p5.setTitle("Explain Historical Prize \n" +
                "in The Novel \"Rice on the Temple\"\n by Nguyen Xuan Khanh");
        p5.setStrands("Text,\n Context," +
                "\n Entextualization");
        p5.setID("40");
        p5.setRoom("R.02");
        p5.setTime("Thurs \n08:30 – 10:00");

        p6.setPresenterName("Presenter: Hombrebueno, Mild R.\n" +
                "Authors: Hombrebueno,\n Mild R.; Galima,\n Loreta Vivian R.");
        p6.setTitle("Locating Local Researches: \nProcesses, Challenges, \n" +
                "and Issues as Departure\n Points For Local Policies");
        p6.setStrands("Language, \nCommunity,\n Ethnicity");
        p6.setID("200");
        p6.setRoom("R.03");
        p6.setTime("Thurs \n08:30 – 10:00");

        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);
        presenterList.add(p5);
        presenterList.add(p6);
        addRows();
    }
    //Completed
    void CalaButtonI(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenter: Indart, Karin Noemi Rühle\n" +
                "Author: Indart, Karin Noemi Rühle");
        p1.setTitle("The Different Linguistic Identities and National\n Identity of Timor-Leste");
        p1.setStrands("Anthropological \nLinguistics");
        p1.setID("95");
        p1.setRoom("R.05");
        p1.setTime("Fri 13:00 – 14:30");

        //add all presenters in list
        presenterList.add(p1);
        addRows();
    }
    //completed
    void CalaButtonJ(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();
        presenter p5 = new presenter();
        presenter p6 = new presenter();
        presenter p7 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenter: Jabbari, Mohammad Jaffar\n" +
                "Author: Jabbari, Mohammad Jaffar");
        p1.setTitle("Generic Masculine, \na Linguistic Universal");
        p1.setStrands("Anthropological \nLinguistics");
        p1.setID("196");
        p1.setRoom(".");
        p1.setTime(".");


        //add all presenters in list
        p2.setPresenterName("Presenter: Jabeen, Tallat\n" +
                "Author: Jabeen, Tallat");
        p2.setTitle("A Study of Effect of Students\n Socioeconomic Class on their\n" +
                " Selection of Address Forms:\n A Pakistani Perspective");
        p2.setStrands("Ethnography of\n Communication");
        p2.setID("180");
        p2.setRoom("R.04");
        p2.setTime("Wed\n 17:00 – 18:30");

        p3.setPresenterName("Presenter: Jamal,\n Muhammad Irwan\n" +
                "Author: Jamal,\n Muhammad Irwan");
        p3.setTitle("Malay Language Campaign in \nSingapore: A Critical Discourse \nAnalysis of the Bulan Bahasa");
        p3.setStrands("Language\n Ideologies");
        p3.setID("246");
        p3.setRoom("R.04");
        p3.setTime("Fri \n15:00 – 16:30");

        p4.setPresenterName("Presenter: Jandoc, \nMaria Rousselle G.\n" +
                "Authors: Hombrebueno, \nMild R.; Jandoc,\n Maria Rousselle G.");
        p4.setTitle("Cultural Mapping of \nEthnographic Objects:\n Processes, Complexities \nand Trajectories");
        p4.setStrands("Ethnographical \nLanguage Work");
        p4.setID("185");
        p4.setRoom("R.02");
        p4.setTime("Thurs \n15:00 – 16:30");

        //add all presenters in list
        p5.setPresenterName("Presenters: Jandoc,\n Maria Rousselle G.;\n Genato, Angel L.\n" +
                "Authors: Jandoc,\n Maria Rousselle G.;\n Genato, Angel L.");
        p5.setTitle("Students’ Gender and \nSexuality in Nueva Vizcaya\n State University-Bayombong \nCampus");
        p5.setStrands("Language,\n Gender,\n Sexuality");
        p5.setID("254");
        p5.setRoom("R.04");
        p5.setTime("Wed\n 15:00 – 16:30");

        p6.setPresenterName("Presenter: Jawaut, \nNopthira\n" +
                "Authors: Jawaut,\n Nopthira;\n Dumlao, Remart");
        p6.setTitle("From Upland to Lowland: \n" +
                "Karen Learners Positioning and Identity\n" +
                " Construction through Language \nSocialization in Thai Classroom\n Context");
        p6.setStrands("Language \nSocialization");
        p6.setID("78");
        p6.setRoom("R.04");
        p6.setTime("Thurs\n 08:30 – 10:00");

        p7.setPresenterName("Presenters: Jepri;\nMuhammad Adam\n" +
                "Authors: Jepri;\n Muhammad Adam");
        p7.setTitle("The Metaphorical Use of Carrying\n Something on One’s Shoulders\n" +
                " – A Study of Embodied Experience and \n" +
                "Socio-Cultural Differences between English \nand Indonesian Language");
        p7.setStrands("Anthropological Linguistics");
        p7.setID("170");
        p7.setRoom(".");
        p7.setTime(".");
        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);
        presenterList.add(p5);
        presenterList.add(p6);
        presenterList.add(p7);
        addRows();
    }
    //complated
    void CalaButtonK(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();
        presenter p5 = new presenter();
        presenter p6 = new presenter();
        presenter p7 = new presenter();
        presenter p8 = new presenter();
        presenter p9 = new presenter();
        presenter p10 = new presenter();
        presenter p11 = new presenter();
        presenter p12 = new presenter();

        //set all attributes
        p1.setPresenterName("Presenter: Kafipour, Reza\n" +
                "Author: Kafipour, Reza");
        p1.setTitle("Investigating the Relationship between \nAcademic Achievement, Creativity, Multiple \nIntelligences and Motivation among Iranian \nStudents of Medical Sciences");
        p1.setStrands("Social Psychology \nof Language");
        p1.setID("152");
        p1.setRoom(".");
        p1.setTime(".");

        //add all presenters in list
        p2.setPresenterName("Presenter: Kai Kit,\n Gerald Choa\n" +
                "Author: Kai Kit, \nGerald Choa");
        p2.setTitle("\"Between a Rock and Reclaimed\n" +
                " Land\": A Structural Comparison \n" +
                "between Singaporean and \nMauritian Petrifaction Myths");
        p2.setStrands("Anthropological \nLinguistics");
        p2.setID("188");
        p2.setRoom("R.05");
        p2.setTime("Thurs \n15:00 – 16:30");

        p3.setPresenterName("Presenter: Karmakar,\n Ankita\n" +
                "Author: Karmakar, \nAnkita");
        p3.setTitle("Unexplored Shades \nof Monpa");
        p3.setStrands("Language, \nCommunity,\n Ethnicity");
        p3.setID("19");
        p3.setRoom(".");
        p3.setTime(".");

        p4.setPresenterName("Presenters: Kaushalya;\n Singh,\n Satyapal\n" +
                "Author: Kaushalya");
        p4.setTitle("Sociology of Sanskrit \n" +
                "Language: in the Context\n of Women and Shudras");
        p4.setStrands("Language \nSocialization\n" +
                "\n");
        p4.setID("171");
        p4.setRoom("R.05");
        p4.setTime("Fri\n 08:30 – 10:00");

        p5.setPresenterName("Presenter: Kida,\n Tsuyoshi\n" +
                "Author: Kida, \nTsuyoshi");
        p5.setTitle("A New French-Based Register?\n Analysis of Commercial \nNaming in Public Urban \nSpace in Japan");
        p5.setStrands("Language Contact \nand Change");
        p5.setID("207");
        p5.setRoom("R.02");
        p5.setTime("Wed \n17:00 – 18:30");

        p6.setPresenterName("Presenter: Kieu,\n Trung Son\n" +
                "Author: Kieu, \nTrung Son");
        p6.setTitle("Linguistic in\n Vietnamese Folk Songs");
        p6.setStrands("Poetics");
        p6.setID("44");
        p6.setRoom(".");
        p6.setTime(".");

        p7.setPresenterName("Presenter: Kumar,\nDevanpalli Srikanth\n" +
                "Author: Kumar,\n Devanpalli Srikanth");
        p7.setTitle("Culture and \nEthno-linguistic \nAspects of Koyas");
        p7.setStrands("Ethnographical \nLanguage Work");
        p7.setID("20");
        p7.setRoom(".");
        p7.setTime(".");

        p8.setPresenterName("Presenter: Kumari,\n Laxmi\n" +
                "Author: Kumari, \nLaxmi");
        p8.setTitle("A Sociocultural Study\n of Folktalkesof ‘Ho’");
        p8.setStrands("Text,\n Context and \nEntextualization");
        p8.setID("212");
        p8.setRoom("R.01");
        p8.setTime("Fri \n13:00 – 14:30");

        p9.setPresenterName("Presenter: Kumari,\n Mamta\n" +
                "Author: Kumari,\n Mamta");
        p9.setTitle("Endangerment and \nRevitalization of Indigenous\n Numeral Systems of \nSouth Asia");
        p9.setStrands("Language\n Revitalization\n" +
                "\n");
        p9.setID("21");
        p9.setRoom(".");
        p9.setTime(".");

        p10.setPresenterName("Presenters: Kurnia\n N, Iis; Nia,\n Kurniasih\n" +
                "Authors: Iis, \nKurnia N; Nia,\n Kurniasih");
        p10.setTitle("English Adjectives in \nIndonesian Cosmetic Advertisement:\n" +
                " A Study of Emphatic Personal \nMetadiscourse Marker");
        p10.setStrands("Language,\n Gender,\n Sexuality");
        p10.setID("109");
        p10.setRoom("R.02");
        p10.setTime("Fri \n13:00 – 14:30");

        p11.setPresenterName("Presenters:\n Kurniasih, Nia;\n Iis, Kurnia N\n" +
                "Authors: Kurniasih, \nNia; Iis,\n Kurnia N");
        p11.setTitle("English Adjectives in Indonesian \n" +
                "Cosmetic Advertisement: \nA Study of Emphatic Personal\n Metadiscourse Marker");
        p11.setStrands("Language, \nGender, \nSexuality");
        p11.setID("109");
        p11.setRoom("R.02");
        p11.setTime("Fri \n13:00 – 14:30");

        p12.setPresenterName("Presenters: Kurniasih,\n Nia; Nuriman,\n Harry\n" +
                "Authors: Kurniasih,\n Nia; Nuriman, \nHarry");
        p12.setTitle("From Verbal to Three-Dimensional\n Digital Visual Text: \nA Construction of a Javanese\n Prince");
        p12.setStrands("Text, \nContext and \nEntextualization");
        p12.setID("110");
        p12.setRoom("R.05");
        p12.setTime("Thurs \n08:30 – 10:00");


        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);
        presenterList.add(p5);
        presenterList.add(p6);
        presenterList.add(p7);
        presenterList.add(p8);
        presenterList.add(p9);
        presenterList.add(p10);
        presenterList.add(p11);
        presenterList.add(p12);
        addRows();
    }
    //completed
    void CalaButtonL(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();
        presenter p5 = new presenter();
        presenter p6 = new presenter();
        presenter p7 = new presenter();
        presenter p8 = new presenter();
        presenter p9 = new presenter();
        presenter p10 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenter: Le,\n Quoc Hieu\n" +
                "Author: Le, \nQuoc Hieu");
        p1.setTitle("Revitalization and \n" +
                "Recontextualization – An \nApproach to Rashomon from \n" +
                "the Theory of\n Adaptation");
        p1.setStrands("Text, Context, \nEntextualization");
        p1.setID("39");
        p1.setRoom(".");
        p1.setTime(".");


        //add all presenters in list
        p2.setPresenterName("Presenter: Le, Thi Phuong\n" +
                "Author: Le, Thi Phuong");
        p2.setTitle("Building community identity through\n" +
                " restoring traditional festival \n" +
                "(case studying in Duong Yen hamlet, \nHanoi suburban)");
        p2.setStrands("Multifunctionality\n");
        p2.setID("38");
        p2.setRoom(".");
        p2.setTime(".");

        p3.setPresenterName("Presenters: Lei, Jing;\n Rao, Yufang\n" +
                "Authors: Lei, Jing; \nRao, Yufang");
        p3.setTitle("Language, Identity and Ideology: \n" +
                "Media-induced Linguistic \n" +
                "Innovations in Contemporary\n China");
        p3.setStrands("Anthropological \nLinguistics");
        p3.setID("79");
        p3.setRoom("R.05");
        p3.setTime("Fri\n 15:00 – 16:30");

        p4.setPresenterName("Presenter: Leo, \nAngela Rumina\n" +
                "Author: Leo, " +
                "\nAngela Rumina");
        p4.setTitle("The Survival of Manglish\n in the Spices Archipelago");
        p4.setStrands("Anthropological Linguistics");
        p4.setID("227");
        p4.setRoom("R.04");
        p4.setTime("Wed \n17:00 – 18:30");

        p5.setPresenterName("Presenters: Lestari,\n" +
                " Puji Audina; Setyaningsih,\n" +
                " Retno Wulandari\n" +
                "Authors: Lestari, Puji Audina;\n" +
                " Setyaningsih, Retno Wulandari");
        p5.setTitle("Singlish Idiomatic Expressions in\n" +
                " Crazy Rich Asian (2018) Movie and\n Its Indonesian Subtitles");
        p5.setStrands("Anthropological \nLinguistics");
        p5.setID("229");
        p5.setRoom("R.03");
        p5.setTime("Fri \n08:30 – 10:00");

        p6.setPresenterName("Presenter: Li, Michelle\n" +
                "Author: Li, Michelle");
        p6.setTitle("Intercultural contact as seen in a \n" +
                "19th century Chinese text");
        p6.setStrands("Language Contact \nand Change");
        p6.setID("80");
        p6.setRoom(".");
        p6.setTime(".");

        p7.setPresenterName("Presenters: Lie, \nAnita; Harjanto,\n Ignatius\n" +
                "Authors: Harjanto, Ignatius;\n Lie, Anita;\n Wijaya, Juliana");
        p7.setTitle("English Text,\n Global Context,\n Indonesian Identity");
        p7.setStrands("Anthropological \nLinguistics");
        p7.setID("240");
        p7.setRoom("R.02");
        p7.setTime("Fri \n08:30 – 10:00");

        p8.setPresenterName("Presenter: Lin, Wen Yue\n" +
                "Authors: Lin, Wen Yue; \nAng, Lay Hoon; Chan,\n" +
                " Mei Yuit; Shamala, \nAP Paramasivam");
        p8.setTitle("Gender Representation in \nMalaysian Mandarin Textbooks");
        p8.setStrands("Language\n Gender\n Sexuality");
        p8.setID("138");
        p8.setRoom("R.02");
        p8.setTime("Fri \n13:00 – 14:30");

        p9.setPresenterName("Presenters: Lisbeth, \nSinan Lendik\n" +
                "Authors: Chan, Mei \nYuit; Lisbeth,\n Sinan Lendik");
        p9.setTitle("A Preliminary Study on the " +
                "\nUse of Epithets in\n Kenyah Long Wat");
        p9.setStrands("Language \nDocumentation\n");
        p9.setID("49");
        p9.setRoom("R.02");
        p9.setTime("Thurs \n 15:00-16:30");

        p10.setPresenterName("Presenter: Lupano, Emma\n" +
                "Author: Lupano, Emma");
        p10.setTitle("\"The Belt and Road is How\"" +
                ". Audiovisual \nnarratives and \n" +
                "political discourse\n on the " +
                "\"New Silk Road\"");
        p10.setStrands("Applied \nSociolinguistics\n" +
                "\n");
        p10.setID("224");
        p10.setRoom(".");
        p10.setTime(".");

        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);
        presenterList.add(p5);
        presenterList.add(p6);
        presenterList.add(p7);
        presenterList.add(p8);
        presenterList.add(p9);
        presenterList.add(p10);

        addRows();
    }
    //completed
    void CalaButtonM(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();
        presenter p5 = new presenter();
        presenter p6 = new presenter();
        presenter p7 = new presenter();
        presenter p8 = new presenter();
        presenter p9 = new presenter();
        presenter p10 = new presenter();
        presenter p11 = new presenter();
        presenter p12 = new presenter();
        presenter p13 = new presenter();
        presenter p14 = new presenter();
        presenter p15 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenter: Mallareddy, Kotthireddy\n" +
                "Author: Mallareddy, Kotthireddy");
        p1.setTitle("Socio Linguistic Reflections in Kaloji Writings");
        p1.setStrands("Language Dialect \nSociolect Genre");
        p1.setID("163");
        p1.setRoom(".");
        p1.setTime(".");

        //add all presenters in list
        p2.setPresenterName("Presenter: MS,\n Shebin\n" +
                "Authors: Menon, Indu; \nMS, Shebin");
        p2.setTitle("Shamanic Rituals and the\n Survival of Endangered Tribal\n Languages – An Anthropological \nStudy On The Basis Of \'Gaddika\'");
        p2.setStrands("Anthropological\n Linguistics");
        p2.setID("157");
        p2.setRoom("R.05");
        p2.setTime("Fri \n13:00 – 14:30");

        p3.setPresenterName("Presenter: Mla, \nSambay P.\n" +
                "Author: Mla,\n Sambay P.");
        p3.setTitle("Anthology of Maguindanaon\n Folk Literature: \nIts Educational Possibilities");
        p3.setStrands("Ethnographical\n Language Work");
        p3.setID("159");
        p3.setRoom(".");
        p3.setTime(".");

        p4.setPresenterName("Presenter: Militello,\n Jacqueline\n" +
                "Author: Militello, \nJacqueline");
        p4.setTitle("Western Name,\n Asian Text: \nUptake of Sinicized Western \nPersonal Names in Hong Kong");
        p4.setStrands("Anthropological\n Linguistics");
        p4.setID("101");
        p4.setRoom("R.05");
        p4.setTime("Fri\n 15:00 – 16:30");

        //add all presenters in list
        p5.setPresenterName("Presenter: \nMittapalli, Rajeshwar\n" +
                "Author: Mittapalli, \nRajeshwar");
        p5.setTitle("English in India: \nFrom a Class Marker to \nLeveller of Classes");
        p5.setStrands("General \nSociolinguistics\n" +
                "\n");
        p5.setID("65");
        p5.setRoom(".");
        p5.setTime(".");

        p6.setPresenterName("Presenters: Mohamad Ibrani,\n Shahrimin Adam Assim;\n Mohamad Maulana, Magiman\n" +
                "Authors: Mohamad Maulana, \nMagiman; Mohamad Ibrani, \nShahrimin Adam Assim");
        p6.setTitle("Sociocultural Imperatives \n" +
                "of Collaborative Social Interactions \n" +
                "among Indigenous and Non-Indigenous \n" +
                "Children in a Computer Environment");
        p6.setStrands("Language in Real\n and Virtual Spaces");
        p6.setID("258");
        p6.setRoom("R.01");
        p6.setTime("Thurs \n15:00 – 16:30");

        p7.setPresenterName("Presenters: Mohamad Ibrani,\n Shahrimin Adam Assim;\n Mohamad Maulana, Magiman\n" +
                "Authors: Mohamad Maulana,\n Magiman; Mohamad Ibrani, \nShahrimin Adam Assim");
        p7.setTitle("Orang Asli & Sociocultural\nPlanes of Analyses:\n A Review on Plausible\n Lenses");
        p7.setStrands("Language Minorities \nand Majorities");
        p7.setID("259");
        p7.setRoom("R.03");
        p7.setTime("Wed \n15:00 – 16:30");

        //add all presenters in list
        p8.setPresenterName("Presenters: Mohamad,\n Maulana Magiman; \nNorhuda, Salleh\n" +
                "Authors: Mohamad Maulana,\n Magiman; Norhuda, \nSalleh; Sabah Alexander, \nAk Chelum");
        p8.setTitle("Makan Tahun: Ritual \nCommunication of Kadayan \nCommunity in Sarawak");
        p8.setStrands("Language,\n Community,\n Ethnicity");
        p8.setID("257");
        p8.setRoom("R.01");
        p8.setTime("Wed \n15:00 – 16:30");

        p9.setPresenterName("Presenters: Mohamad Maulana, \nMagiman; Mohamad Ibrani, \nShahrimin Adam Assim\n" +
                "Authors: Mohamad Maulana, \nMagiman; Mohamad Ibrani,\n Shahrimin Adam Assim");
        p9.setTitle("Sociocultural Imperatives\n of Collaborative Social Interactions\n among Indigenous and\n" +
                "Non-Indigenous Children in a \nComputer Environment");
        p9.setStrands("Language in Real \nand Virtual Spaces");
        p9.setID("258");
        p9.setRoom("R.01");
        p9.setTime("Thurs \n15:00 – 16:30");

        p10.setPresenterName("Presenters: Mohamad Maulana,\n Magiman; Mohamad Ibrani,\n Shahrimin Adam Assim\n" +
                "Authors: Mohamad Maulana, Magiman;\n Mohamad Ibrani,\n Shahrimin Adam Assim");
        p10.setTitle("Orang Asli & Sociocultural \nPlanes of Analyses: \nA Review on Plausible Lenses");
        p10.setStrands("Language Minorities \nand Majorities");
        p10.setID("259");
        p10.setRoom("R.03");
        p10.setTime("Wed \n15:00 – 16:30");

        //add all presenters in list
        p11.setPresenterName("Presenter: \nMorbiato, Anna\n" +
                "Author: Morbiato, Anna");
        p11.setTitle("Spatio-temporal Frames and\n" +
                " Chinese Word Order – \nA Cognitive-functional Analysis" +
                "\n based on the Containment Schema");
        p11.setStrands("Language and \nSpatiotemporal \nFrames");
        p11.setID("195");
        p11.setRoom("R.01");
        p11.setTime("Thurs \n08:30 – 10:00");

        p12.setPresenterName("Presenter: Mouli,\n T.Sai Chandra\n" +
                "Author: Mouli, \nT.Sai Chandra");
        p12.setTitle("Rise and Decline of \nLanguages: Struggle for \nExistence");
        p12.setStrands("Language Contact\n and Change");
        p12.setID("111");
        p12.setRoom("R.02");
        p12.setTime("Wed \n17:00 – 18:30");

        p13.setPresenterName("Presenter: Muhammad Adam\n" +
                "Author: Muhammad Adam");
        p13.setTitle("Metonymy in Indonesian\n Political Discourse");
        p13.setStrands("Metonymy in Indonesian \nPolitical Discourse");
        p13.setID("162");
        p13.setRoom(".");
        p13.setTime(".");

        //add all presenters in list
        p14.setPresenterName("Presenters: Muhammad Adam; Jepri\n" +
                "Authors: Muhammad Adam; \nJepri");
        p14.setTitle("The Metaphorical Use of Carrying \n" +
                "Something on One’s Shoulders – \n" +
                "A Study of Embodied Experience and\n" +
                "Socio-Cultural Differences between English\n" +
                " and Indonesian Language");
        p14.setStrands("Anthropological \n" +
                "Linguistics");
        p14.setID("170");
        p14.setRoom(".");
        p14.setTime(".");

        p15.setPresenterName("Presenter: Murshida,\n" +
                " Afrida Aainun\n" +
                "Author: Murshida, \n" +
                "Afrida Aainun");
        p15.setTitle("Creating a \'Global Self\'\n" +
                ": Meena Alexander’s Dilemma of \n" +
                "Identity and its Deconstruction");
        p15.setStrands("Language\n Ideologies");
        p15.setID("178");
        p15.setRoom("R.04");
        p15.setTime("Fri \n08:30 – 10:00\n" +
                "\n");

        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);
        presenterList.add(p5);
        presenterList.add(p6);
        presenterList.add(p7);
        presenterList.add(p8);
        presenterList.add(p9);
        presenterList.add(p10);
        presenterList.add(p11);
        presenterList.add(p12);
        presenterList.add(p13);
        presenterList.add(p14);
        presenterList.add(p15);

        addRows();
    }
    //complete
    void CalaButtonN(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();
        presenter p5 = new presenter();
        presenter p6 = new presenter();
        presenter p7 = new presenter();
        presenter p8 = new presenter();
        presenter p9 = new presenter();
        presenter p10 = new presenter();
        presenter p11 = new presenter();
        presenter p12 = new presenter();
        presenter p13 = new presenter();
        presenter p14 = new presenter();
        presenter p15 = new presenter();
        presenter p16 = new presenter();
        presenter p17 = new presenter();
        presenter p18 = new presenter();
        presenter p19 = new presenter();
        presenter p20 = new presenter();
        presenter p21 = new presenter();
        presenter p22 = new presenter();
        presenter p23 = new presenter();
        presenter p24 = new presenter();
        presenter p25 = new presenter();
        presenter p26 = new presenter();
        presenter p27 = new presenter();
        presenter p28 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenter: Nawapan, Thiti\n" +
                "Authors: Dumlao, Remart; Nawapan, Thiti");
        p1.setTitle(" \"How do ASEAN region localizes international\n brads?\": A multidimensional analysis of TV \ncommercials in Southeast Asian region");
        p1.setStrands("Semiotics and \nSemiology");
        p1.setID("77");
        p1.setRoom("R.01");
        p1.setTime("Wednesday 17:00 – 18:30");

        //add all presenters in list
        p2.setPresenterName("Presenter: Ng, Chwee Fang\n" +
                "Authors: Ser, Wue Hiong; Ng, Chwee Fang");
        p2.setTitle("Malay Narratives in Nanyang Travel");
        p2.setStrands("Narrative and Metanarrative");
        p2.setID("247");
        p2.setRoom(".");
        p2.setTime(".");

        p3.setPresenterName("Presenters: Norhuda, Salleh;\n Mohamad Maulana, Magiman\n" +
                "Authors: Mohamad Maulana, Magiman; \nNorhuda, Salleh;\n Sabah Alexander, Ak Chelum");
        p3.setTitle("Makan Tahun: Ritual Communication \nof Kadayan Community in Sarawak");
        p3.setStrands("Language, \nCommunity,\n Ethnicity");
        p3.setID("257");
        p3.setRoom("R.01");
        p3.setTime("Wed \n15:00 – 16:30");

        p4.setPresenterName("Presenters: Nur, Nabilah,\n Abdullah; Rafidah, Sahar\n" +
                "Authors: Rafidah, Sahar;\n Nur, Nabilah, Abdullah");
        p4.setTitle("Conceptualising Doctoral \n" +
                "Supervision in Malaysia as Small \n" +
                "Cultures: PhD Graduates’ Perspectives");
        p4.setStrands("General\n Sociolinguistics\n" +
                "\n");
        p4.setID("181");
        p4.setRoom("R.03");
        p4.setTime("Wed \n15:00 – 16:30");

        p5.setPresenterName("Presenters: Nur, Nabilah,\n Abdullah; Rafidah, Sahar\n" +
                "Authors: Nur, Nabilah, \nAbdullah; Rafidah, Sahar");
        p5.setTitle("Exploring Intercultural Interaction:\n The Use of Semiotic Resources \nin Meaning-Making Process");
        p5.setStrands("Nonverbal\n Semiotics");
        p5.setID("182");
        p5.setRoom("R.04");
        p5.setTime("Thurs \n15:00 – 16:30");

        p6.setPresenterName("Presenters: Nandy, \nParomita; \nSarkar, Anirban\n" +
                "Authors: Nandy, \nParomita; \nSarkar, Anirban");
        p6.setTitle("Cross-Linguistic Homonymy: \n" +
                "Impact on Comprehension among the\n" +
                " Children Belonging to Bengali \nDiaspora Community in Kerala");
        p6.setStrands("Applied\n Sociolinguistics\n");
        p6.setID("119");
        p6.setRoom("R.04");
        p6.setTime("Wed \n17:00 – 18:30");

        p7.setPresenterName("Presenter: Nanjundan,\n Ramesh\n" +
                "Author: Nanjundan,\n Ramesh");
        p7.setTitle("purice:tpimi : Marriage Ritual \nPractices among Toda: An Indigenous\n Community of The Nilgiris");
        p7.setStrands("Language, \nCommunity,\n Ethnicity");
        p7.setID("28");
        p7.setRoom(".");
        p7.setTime(".");

        p8.setPresenterName("Presenter: Nawkamdee, \nPiyanan\n" +
                "Author: Nawkamdee, \nPiyanan");
        p8.setTitle("Lam Phaya Performing Arts in\n Savannakhet Province, \nLaos PDR.");
        p8.setStrands("Language \nSocialization\n" +
                "\n");
        p8.setID("124");
        p8.setRoom(".");
        p8.setTime(".");

        p9.setPresenterName("Presenter: Ngo, \nThi Thu Huong\n" +
                "Author: Ngo, \nThi Thu Huong");
        p9.setTitle("Learning the Use of\n" +
                " Mother Language in Ethnic Minorities\n" +
                " in Nam Tra My District,\n Quang Nam Province, Vietnam");
        p9.setStrands("Language,\n Community,\n Ethnicity");
        p9.setID("213");
        p9.setRoom(".");
        p9.setTime(".");

        p10.setPresenterName("Presenter: Nguyen,\n Minh Tri\n" +
                "Author: Nguyen,\n Minh Tri");
        p10.setTitle("Strengthening the Intercultural \n" +
                "Communication Competence: Language Users’\n" +
                " Perspectives to Cooperative Principles \nand Politeness Strategies");
        p10.setStrands("Language Socialization");
        p10.setID("91");
        p10.setRoom(".");
        p10.setTime(".");

        p11.setPresenterName("Presenter: Nguyen,\n Hong Oanh\n" +
                "Author: Nguyen\n Hong Oanh");
        p11.setTitle("Educator Perspectives on the \nImpact of New Englishes on Practical\n Education");
        p11.setStrands("Anthropological \nLinguistics");
        p11.setID("129");
        p11.setRoom(".");
        p11.setTime(".");

        p12.setPresenterName("Presenter: Nguyen\n Minh Tri\n" +
                "Author: Nguyen,\nMinh Tri");
        p12.setTitle("The Impact of Vietnamese Speech Community\n" +
                " on English Communicative Competence: \nA Case Study at an English Center");
        p12.setStrands("Language,\n Community,\n Ethnicity");
        p12.setID("113");
        p12.setRoom(".");
        p12.setTime(".");

        p13.setPresenterName("Presenter: Nguyen,\n Phuong Lien\n" +
                "Author: Nguyen, \nPhuong Lien");
        p13.setTitle("Concepts of Religions (Confucianism\n" +
                " and Buddhism) from Poetic-Stories \nto Reality in Indochina");
        p13.setStrands("Poetics");
        p13.setID("128");
        p13.setRoom("R.04");
        p13.setTime("Fri 08:30 – 10:00");

        p14.setPresenterName("Presenter: Nguyen\n Thi Dung\n" +
                "Author: Nguyen, \nThi Dung");
        p14.setTitle("Proposing of Solutions to Improve \nthe Efficiency of Teaching Folk \n" +
                "ales in Ethnic Minorities for Teachers \nof Primary Schools on Lao Cai Province");
        p14.setStrands("Language Minorities\n and Majorities");
        p14.setID("145");
        p14.setRoom(".");
        p14.setTime(".");

        p15.setPresenterName("Presenter: Nguyen,\n Thi Hong Thu\n" +
                "Author: Nguyen, \nThi Hong Thu");
        p15.setTitle("Metaphors of Love in \nLate 20th Century\n English Songs");
        p15.setStrands("Cognitive Anthropology\n and Language");
        p15.setID("53");
        p15.setRoom(".");
        p15.setTime(".");

        p16.setPresenterName("Presenters: Nguyen,\n Thi Minh Thu;\n Nguyen, Thi Nhung\n" +
                "Authors: Nguyen, \nThi Minh Thu;\n Nguyen, Thi Nhung");
        p16.setTitle("Radio and Television Broadcasting\n" +
                " in Ethnic Minority Languages in\n" +
                " Northern Vietnam, some Characteristics \nand Experiences");
        p16.setStrands("Language Minorities \nand Majorities");
        p16.setID("127");
        p16.setRoom(".");
        p16.setTime(".");

        p17.setPresenterName("Presenters: Nguyen,\n Thi Nhung; Nguyen,\n Thi Minh Thu\n" +
                "Authors: Nguyen, \nThi Minh Thu;\n Nguyen, Thi Nhung");
        p17.setTitle("Radio and Television Broadcasting\n" +
                " in Ethnic Minority Languages in Northern \nVietnam, some Characteristics and \nExperiences");
        p17.setStrands("Language Minorities \nand Majorities");
        p17.setID("127");
        p17.setRoom(".");
        p17.setTime(".");

        p18.setPresenterName("Presenter: Nguyen,\n Thi Yen\n" +
                "Author: Nguyen,\n Thi Yen");
        p18.setTitle("Performance Art \"Len Dong\"\n of Vietnamese People: \nfrom Ritual to Stage");
        p18.setStrands("Language \nDocumentation\n" +
                "\n");
        p18.setID("35");
        p18.setRoom(".");
        p18.setTime(".");

        p19.setPresenterName("Presenter: Nikoo,\n Vahid Hojati\n" +
                "Author: Nikoo, \nVahid Hojati");
        p19.setTitle("Sociological Study of Social\n Relationships in Migration \nCities and its Social Factors");
        p19.setStrands("Ethnography of\n Communicatio");
        p19.setID("25");
        p19.setRoom(".");
        p19.setTime(".");

        p20.setPresenterName("Presenter: Nilep, Chad\n" +
                "Author: Nilep, Chad");
        p20.setTitle("Narrative, Identity, and \nTransnational Family: Hippo\n" +
                " Family Club and the Construction of\n Cosmopolitan Citizenship");
        p20.setStrands("Language Ideologies");
        p20.setID("23");
        p20.setRoom(".");
        p20.setTime(".");

        p21.setPresenterName("Presenter: Nirwan\n" +
                "Author: Nirwan");
        p21.setTitle("Single Talk: Pointing via \nLanguage in Pakkadoq");
        p21.setStrands("Anthropological \nLinguistics");
        p21.setID("62");
        p21.setRoom(".");
        p21.setTime(".");

        p22.setPresenterName("Presenter: Noguchi\n, Mary Goebel\n" +
                "Author: Noguchi, \nMary Goebel");
        p22.setTitle("The Shifting Sub-Text of\n Japanese Gendered Language");
        p22.setStrands("Language,\n Gender,\n Sexuality");
        p22.setID("206");
        p22.setRoom("R.04");
        p22.setTime("Wed \n15:00 – 16:30");

        p23.setPresenterName("Presenter: Norazmah,\n Suhailah bt Abdul Malek\n" +
                "Authors: Norazmah, \nSuhailah bt Abdul Malek; \nAireen, bt Ibrahim");
        p23.setTitle("What Are You?");
        p23.setStrands("Language in Real \nand Virtual Spaces");
        p23.setID("237");
        p23.setRoom(".");
        p23.setTime(".");

        p24.setPresenterName("Presenter: Norazuna, \nNorahim\n" +
                "Author: Norazuna, \nNorahim");
        p24.setTitle("Berawan-Lower Baram Language \nGroup: A Study on Linguistic\n Affiliation and Language Survival");
        p24.setStrands("Language Contact\n and Change");
        p24.setID("214");
        p24.setRoom("R.02");
        p24.setTime("Fri \n15:00 – 16:30");

        p25.setPresenterName("Presenter: Normaliza,\n Abd Rahim\n" +
                "Authors: Normaliza, Abd Rahim; \nAliaa, Roslan, Siti Nur; \nMaisarah, Roslan,\n Nur; Widad, Roslan, Nur");
        p25.setTitle("Context in Utterances \ntowards the \"Social Values in\n Children’s Story Model\"");
        p25.setStrands("Anthropological Linguistics");
        p25.setID("51");
        p25.setRoom(".");
        p25.setTime(".");

        p26.setPresenterName("Presenter: Normaliza, Abd Rahim\n" +
                "Authors: Normaliza, Abd Rahim; \nMaisarah, Roslan, Nur;\n Aliaa, Roslan, Siti Nur; \nWidad Roslan, Nur");
        p26.setTitle("The Forgotten Malay Folklore:\n Dissemination of Virtues in Animal Stories");
        p26.setStrands("Ethnography of \nCommunication");
        p26.setID("130");
        p26.setRoom(".");
        p26.setTime(".");

        p27.setPresenterName("Presenter: Nose, Masahiko\n" +
                "Author: Nose, Masahiko");
        p27.setTitle("Morphological analysis of \nnegation in Amele, \nPapua New Guinea");
        p27.setStrands("Anthropological Linguistics");
        p27.setID("45");
        p27.setRoom("R.05");
        p27.setTime("Wed \n15:00 – 16:30");

        p28.setPresenterName("Presenters: Nuriman, Harry;\n Kurniasih, Nia\n" +
                "Authors: Nuriman,\n Harry; Kurniasih,\n Nia");
        p28.setTitle("From Verbal to Three-Dimensional \nDigital Visual Text: A Construction\n of a Javanese Prince");
        p28.setStrands("Text, Context and \nEntextualization");
        p28.setID("110");
        p28.setRoom("R.05");
        p28.setTime("Thurs \n08:30 – 10:00");
        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);
        presenterList.add(p5);
        presenterList.add(p6);
        presenterList.add(p7);
        presenterList.add(p8);
        presenterList.add(p9);
        presenterList.add(p10);
        presenterList.add(p11);
        presenterList.add(p12);
        presenterList.add(p13);
        presenterList.add(p14);
        presenterList.add(p15);
        presenterList.add(p16);
        presenterList.add(p17);
        presenterList.add(p18);
        presenterList.add(p19);
        presenterList.add(p20);
        presenterList.add(p21);
        presenterList.add(p22);
        presenterList.add(p23);
        presenterList.add(p24);
        presenterList.add(p25);
        presenterList.add(p26);
        presenterList.add(p27);
        presenterList.add(p28);
        addRows();
    }
    void CalaButtonP(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();
        presenter p5 = new presenter();
        presenter p6 = new presenter();
        presenter p7 = new presenter();
        presenter p8 = new presenter();
        presenter p9 = new presenter();
        presenter p10 = new presenter();

        //set all attributes
        p1.setPresenterName("Presenter: Paee, Rokiah\n" +
                "Author: Paee, Rokiah; Roswati,\n Abdul Rashid; Roslina, Mamat");
        p1.setTitle("Japanese Animation: Its Effect on Malaysian \nUndergraduates’ Students");
        p1.setStrands("Language \nCommunity \nEthnicity");
        p1.setID("161");
        p1.setRoom(".");
        p1.setTime(".");


        //add all presenters in list
        p2.setPresenterName("Presenter: Parambath, Rasna\n" +
                "Author: Parambath, Rasna");
        p2.setTitle("Study of Unwritten Histories of \n" +
                "Tribal Freedom Fighters of Kerala And Evaluate\n" +
                "the Oral Histories and Historic Documents\nof Kurichya Tribal Freedom Fighters");
        p2.setStrands("Anthropological \nLinguistics\n" +
                "\n");
        p2.setID("97");
        p2.setRoom(".");
        p2.setTime(".");

        p3.setPresenterName("Presenter: Parmisana,\n Venus R.\n" +
                "Author: Parmisana, \nVenus R.");
        p3.setTitle("CSR Communications of Manufacturing\n" +
                " Industries in Northern Mindanao:\nAn Environmental Discourse Analysis");
        p3.setStrands("Ethnography of \n" +
                "Communication\n");
        p3.setID("252");
        p3.setRoom(".");
        p3.setTime(".");

        p4.setPresenterName("Presenter: Pasion,\n Daryl Q.\n" +
                "Author: Pasion, \nDaryl Q.");
        p4.setTitle("Pabaruon taku de Papatoyan:\n An Ethnolinguistic Study of \nIlubu Tribe’s Sense of Justice");
        p4.setStrands("Anthropological Linguistics");
        p4.setID("8");
        p4.setRoom(".");
        p4.setTime(".");


        //add all presenters in list
        p5.setPresenterName("Presenter: Paulson, Dave\n" +
                "Author: Paulson, Dave");
        p5.setTitle("Writing in the Margins: Materiality \nand the Linguistic Landscapes\n of Endangered Austronesian Languages \nin Vietnam");
        p5.setStrands("Language,\n Community,\n Ethnicity");
        p5.setID("13");
        p5.setRoom(".");
        p5.setTime(".");

        p6.setPresenterName("Presenter: Peng, Junhua\n" +
                "Authors: Ang, Lay Hoon; Peng Junhua");
        p6.setTitle("The Multilingual Landscape of \nTourist City in Southwest China—\nA Case Study of Anshun,\n Guizhou");
        p6.setStrands("Anthropological Linguistics");
        p6.setID("256");
        p6.setRoom("R.01");
        p6.setTime("Fri \n15:00 – 16:30");

        p7.setPresenterName("Presenter: Petrů, Tomáš\n" +
                "Author: Petrů, Tomáš");
        p7.setTitle("\"Lands below the Winds\" as Part of the \nPersian Cosmopolis?\n Linguistic Borrowings from the\n Persianate World in Nusantara");
        p7.setStrands("Language Contact \nand Change");
        p7.setID("177");
        p7.setRoom(".");
        p7.setTime(".");

        //add all presenters in list
        p8.setPresenterName("Presenter: Pham,\n Dang Xuan Huong\n" +
                "Author: Pham,\n Dang Xuan Huong");
        p8.setTitle("Poetics characteristics of languages \nin folk \"poetry stories\"\n" +
                " of Tai people in Vietnam");
        p8.setStrands("Poetics");
        p8.setID("126");
        p8.setRoom(".");
        p8.setTime(".");

        p9.setPresenterName("Presenter: Pospechova,\n Zuzana\n" +
                "Author: Pospechova, \nZuzana");
        p9.setTitle("Gender Influence on\n Prosody of Standard Chinese");
        p9.setStrands("Language,\n Gender, \nSexuality");
        p9.setID("85");
        p9.setRoom(".");
        p9.setTime(".");

        p10.setPresenterName("Presenter: Priyadarshi, \nPrashant\n" +
                "Authors: Roy, Samapika; Priyadarshi, \nPrashant; Sukhada");
        p10.setTitle("Grammatical Polarity\n in Maithili");
        p10.setStrands("Multifunctionality\n" +
                "\n");
        p10.setID("220");
        p10.setRoom("R.01");
        p10.setTime("Wed \n15:00 -16:30");

        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);
        presenterList.add(p5);
        presenterList.add(p6);
        presenterList.add(p7);
        presenterList.add(p8);
        presenterList.add(p9);
        presenterList.add(p10);

        addRows();
    }
    //Complete
    void CalaButtonQ(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenter: Quinto, Edward Jay M.\n" +
                "Author: Quinto, Edward Jay M.");
        p1.setTitle("Co- and re-contextualizing ideologies in\n political discourse: Intersemiotic mechanisms\n in Former President Benigno Aquino III’s \nOctober 30th Speech");
        p1.setStrands("Language \nIdeologies");
        p1.setID("153");
        p1.setRoom("R.04");
        p1.setTime("Fri 08:30 – 10:00");


        //add all presenters in list
        presenterList.add(p1);
        addRows();
    }
    void CalaButtonR(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();
        presenter p5 = new presenter();
        presenter p6 = new presenter();
        presenter p7 = new presenter();
        presenter p8 = new presenter();
        presenter p9 = new presenter();
        presenter p10 = new presenter();
        presenter p11 = new presenter();
        presenter p12 = new presenter();
        presenter p13 = new presenter();
        presenter p14 = new presenter();
        presenter p15 = new presenter();
        presenter p16 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenters: Raan-Hann, Tan; De \nSousa, Silvio Moreira\n" +
                "Authors: Raan-Hann, Tan; De \nSousa, Silvio Moreira");
        p1.setTitle("The Shades of Being Eurasian and Its\n Reverberations among the Community in\n Malaysia");
        p1.setStrands("Anthropological \nLinguistics");
        p1.setID("83");
        p1.setRoom("R.05");
        p1.setTime("Wed 15:00 – 16:30");


        //add all presenters in list
        p2.setPresenterName("Presenters: Raan-Hann, \nTan; De Sousa, \nSilvio Moreira\n" +
                "Authors: Raan-Hann, \nTan; De Sousa,\n Silvio Moreira");
        p2.setTitle("Language shift and terms of \naddress of two Malayo-Portuguese \nCreole communities in Malaysia\n and Indonesia");
        p2.setStrands("Language Contact \nand Change");
        p2.setID("158");
        p2.setRoom("R.04");
        p2.setTime("Fri \n13:00 – 14:30");

        p3.setPresenterName("Presenters: Rafidah,\n Sahar; Nur,\n Nabilah, Abdullah\n" +
                "Authors: Rafidah, \nSahar; Nur, \nNabilah, Abdullah");
        p3.setTitle("Conceptualising Doctoral Supervision\n in Malaysia as Small Cultures:\n PhD Graduates’ Perspectives");
        p3.setStrands("General Sociolinguistics");
        p3.setID("181");
        p3.setRoom("R.01");
        p3.setTime("Fri \n15:00 – 16:30");

        p4.setPresenterName("Presenters: Rafidah,\n Sahar; Nur,\n Nabilah, Abdullah\n" +
                "Authors: Nur, Nabilah,\n Abdullah; Rafidah, Sahar");
        p4.setTitle("Exploring Intercultural Interaction:\n The Use of Semiotic Resources\n in Meaning-Making Process");
        p4.setStrands("Nonverbal Semiotics");
        p4.setID("182");
        p4.setRoom("R.04");
        p4.setTime("Thurs \n15:00 – 16:30");

        //add all presenters in list
        p5.setPresenterName("Presenter: Raheja, Roshni\n" +
                "Author: Raheja, Roshni");
        p5.setTitle("Effects of Speech Accents on Perceptions \n" +
                "of Intelligence in Multicultural \nUniversity Environments Today");
        p5.setStrands("Social Psychology\n of Language");
        p5.setID("4");
        p5.setRoom("R.03");
        p5.setTime("Thurs \n15:00 – 16:30");

        p6.setPresenterName("Presenter: Rahman,\n A R M Mostafizar\n" +
                "Author: Rahman,\n A R M Mostafizar");
        p6.setTitle("Ideological Evaluation of \nPracticing Hybrid Bangla");
        p6.setStrands("Language Ideologies");
        p6.setID("148");
        p6.setRoom(".");
        p6.setTime(".");

        p7.setPresenterName("Presenters: Rao, \nYufang; Lei, Jing\n" +
                "Authors: Lei, \nJing; Rao, Yufang");
        p7.setTitle("Language, Identity and Ideology: \nMedia-induced Linguistic Innovations\n in Contemporary China");
        p7.setStrands("Anthropological Linguistics");
        p7.setID("79");
        p7.setRoom("R.05");
        p7.setTime("Fri \n15:00 – 16:30");

        //add all presenters in list
        p8.setPresenterName("Presenter: Razaul, \nKarim Faquire\n" +
                "Author: Razaul,\n Karim Faquire");
        p8.setTitle("Depicting the Visual Linguistic \nLandscape of Dhaka University Campus");
        p8.setStrands("Language in Real\n and Virtual Spaces");
        p8.setID("75");
        p8.setRoom(".");
        p8.setTime(".");

        p9.setPresenterName("Presenter: Rhee, Seongha\n" +
                "Author: Rhee, Seongha");
        p9.setTitle("On an Unsystematic System:\n Fluctuating Pronominal\n Reference in Korean");
        p9.setStrands("Cognitive Anthropology \nand Language");
        p9.setID("30");
        p9.setRoom("R.03");
        p9.setTime("Thurs \n15:00 – 16:30");

        p10.setPresenterName("Presenters: Rijal, Sindhu;\n Duwadi, Eak Prasad\n" +
                "Authors: Duwadi, Eak \nPrasad; Rijal, Sindhu");
        p10.setTitle("English in Nepal:\n Phoenix or Dinosaur?");
        p10.setStrands("Language Minorities\n and Majorities");
        p10.setID("6");
        p10.setRoom(".");
        p10.setTime(".");

        //add all presenters in list
        p11.setPresenterName("Presenter: Riva, Natalia\n" +
                "Author: Riva, Natalia");
        p11.setTitle("A Preliminary Assessment of \nResearch on Chinese Legal Discourse\n and Its Pedagogical Translation");
        p11.setStrands("General Sociolinguistics\n" +
                "\n");
        p11.setID("221");
        p11.setRoom("R.03");
        p11.setTime("Wed\n 15:00 – 16:30");

        p12.setPresenterName("Presenter: Riva,\n Natalia\n" +
                "Authors: Riva, Natalia;\n Bertulessi, Chiara; \nLupano, Emma");
        p12.setTitle("A Glass of Two Languages:\n The Specialized Terminology of\n Italian Wine in Chinese");
        p12.setStrands("General Sociolinguistics");
        p12.setID("222");
        p12.setRoom("R.03");
        p12.setTime("Thurs\n 08:30 – 10:00");

        p13.setPresenterName("Presenter: Robert, \nSam\n" +
                "Author: Robert, Sam");
        p13.setTitle("Mother Tongue Bible Translation\n Among Indigenous Groups in \nIndia: The TWFTW Approach");
        p13.setStrands("Language Revitalization\n" +
                "\n");
        p13.setID("137");
        p13.setRoom(".");
        p13.setTime(".");

        //add all presenters in list
        p14.setPresenterName("Presenter: Roslina, Mamat\n" +
                "Authors: Rozlina, Mamat; \nRoswati, Abdul Rashid;\n Rokia, Paiee");
        p14.setTitle("A Comparative Communication Study of \n" +
                "the Japanese Language in Intercultural\n" +
                " Communication among Japanese \nAnd Malay Tourist Guides");
        p14.setStrands("Language \nCommunity \nEthnicity");
        p14.setID("131");
        p14.setRoom(".");
        p14.setTime(".");

        p15.setPresenterName("Presenter: Roswati,\n Abdul Rashid\n" +
                "Authors: Roswati, Abdul Rashid; \nRozlina, Mamat; Rokia, Paiee");
        p15.setTitle("Compliment Strategies Used by \nJapanese and Malaysian Tour \nGuides during Tour Trip Sessions: \nComparative Studies");
        p15.setStrands("Ethnography of \nCommunication");
        p15.setID("56");
        p15.setRoom(".");
        p15.setTime(".");

        p16.setPresenterName("Presenter: Roy, Samapika\n" +
                "Authors: Roy, Samapika; \nSukhada; Singh,\n Anil Kr");
        p16.setTitle("Linguistic Analysis of Indian \nEnglish News Headlines");
        p16.setStrands("Text, \nContext, \nEntextualization");
        p16.setID("219");
        p16.setRoom("R.02");
        p16.setTime("Fri 08:30 – 10:00");

        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);
        presenterList.add(p5);
        presenterList.add(p6);
        presenterList.add(p7);
        presenterList.add(p8);
        presenterList.add(p9);
        presenterList.add(p10);
        presenterList.add(p11);
        presenterList.add(p12);
        presenterList.add(p13);
        presenterList.add(p14);
        presenterList.add(p15);
        presenterList.add(p16);

        addRows();
    }
    //completed
    void CalaButtonS(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();
        presenter p5 = new presenter();
        presenter p6 = new presenter();
        presenter p7 = new presenter();
        presenter p8 = new presenter();
        presenter p9 = new presenter();
        presenter p10 = new presenter();
        presenter p11 = new presenter();
        presenter p12 = new presenter();
        presenter p13 = new presenter();
        presenter p14 = new presenter();
        presenter p15 = new presenter();
        presenter p16 = new presenter();
        presenter p17 = new presenter();
        presenter p18 = new presenter();
        presenter p19 = new presenter();
        presenter p20 = new presenter();
        presenter p21 = new presenter();
        presenter p22 = new presenter();
        presenter p23 = new presenter();

        //set all attributes
        p1.setPresenterName("Presenter: Sahoo, Piyusa Ranjan\n" +
                "Author: Sahoo, Piyusa Ranjan");
        p1.setTitle("A Sociolinguistic study of a preliterate tribe\n Ang (Jarawa) of Andaman and Nicobar Islands \nof India");
        p1.setStrands("Anthropological \nLinguistics");
        p1.setID("121");
        p1.setRoom("R.01");
        p1.setTime("Thurs 15:00 – 16:30");


        //add all presenters in list
        p2.setPresenterName("Presenter: Sak-Humphry,\n Chhany\n" +
                "Author: Sak-Humphry, \nChhany");
        p2.setTitle("(Re)claiming Identity: Traditional \n" +
                "Folktales of the Hare រ ឿងររេងសុភាទន្សាយ /\n" +
                " Language Materials for Heritage Learners");
        p2.setStrands("Language\n Revitalization");
        p2.setID("60");
        p2.setRoom("R.05");
        p2.setTime("Thurs\n 15:00 – 16:30");

        p3.setPresenterName("Presenter: Samarita, \nRommel Chrisden Rollan\n" +
                "Author: Samarita, \nRommel Chrisden Rollan");
        p3.setTitle("Translating cultures in the \n" +
                "English translation of Cirilo\n" +
                " F. Bautista’s Tagalog poetry \nSugat ng Salita");
        p3.setStrands("Poetics");
        p3.setID("97");
        p3.setRoom(".");
        p3.setTime(".");

        p4.setPresenterName("Presenter: Samarita, \nRommel Chrisden Rollan\n" +
                "Author: Samarita,\n Rommel Chrisden Rollan");
        p4.setTitle("Distant Reading the Philippine \n" +
                "Literary Archive: Preliminary\n Texts and Contexts");
        p4.setStrands("Text,\n Context,\n Entextualization");
        p4.setID("98");
        p4.setRoom(".");
        p4.setTime(".");

        p5.setPresenterName("Presenter: Sanchez,\n Korina\n" +
                "Author: Sanchez,\n Korina");
        p5.setTitle("President Rodrigo Duterte’s \n" +
                "Remarks on Women and its\n" +
                " Impact on Young Men in\n the Philippines");
        p5.setStrands("Anthropological \nLinguistics");
        p5.setID("208");
        p5.setRoom(".");
        p5.setTime(".");

        p6.setPresenterName("Presenter: Sanjmyatav,\n Gantsetseg\n" +
                "Author: Sanjmyatav, \nGantsetseg");
        p6.setTitle("Cultural Mentality:\n English and Mongolian\n Idioms");
        p6.setStrands("Semiotics and \nSemiology");
        p6.setID("96");
        p6.setRoom(".");
        p6.setTime(".");

        p7.setPresenterName("Presenter: Sarkar,\n Anirban; Nandy, Paromita\n" +
                "Author: Nandy, Paromita; \nSarkar, Anirban");
        p7.setTitle("Cross-Linguistic Homonymy:\n" +
                " Impact on Comprehension among \n" +
                "the Children Belonging to Bengali \n" +
                "Diaspora Community in Kerala");
        p7.setStrands("Applied \nSociolinguistics\n" +
                "\n");
        p7.setID("119");
        p7.setRoom("R.04");
        p7.setTime("Wed \n17:00 – 18:30");

        p8.setPresenterName("Presenter: Sarkisov,\n Ivan\n" +
                "Author: Sarkisov,\n Ivan");
        p8.setTitle("Comparative Analysis of Metrics \n" +
                "in Thai and Burmese Poetry");
        p8.setStrands("Poetics");
        p8.setID("29");
        p8.setRoom(".");
        p8.setTime(".");

        p9.setPresenterName("Presenter: Sasikumar, V.K.\n" +
                "Author: Sasikumar, V.K.");
        p9.setTitle("A Story to be Told: Seeing Ethnic\n" +
                " Malayalee on the World Stage");
        p9.setStrands("Language, \nCommunity,\n Ethnicity");
        p9.setID("211");
        p9.setRoom(".");
        p9.setTime(".");

        p10.setPresenterName("Presenter: Shar, \nManik Mustafa\n" +
                "Author: Shar, \nManik Mustafa");
        p10.setTitle("Indus Language Script its \n" +
                "Interpretations and\n Complexities");
        p10.setStrands("Language \nDocumentation\n" +
                "\n");
        p10.setID("36");
        p10.setRoom(".");
        p10.setTime(".");

        p11.setPresenterName("Presenter: Sharma,\n Sandeep Kumar\n" +
                "Authors: Sharma, Sandeep Kumar; \nSinha, Sweta");
        p11.setTitle("Metaphors in Culture: A Cognitive-\n" +
                "Linguistic Investigation of \nHindi");
        p11.setStrands("Cognitive Anthropology \nand Language");
        p11.setID("115");
        p11.setRoom(".");
        p11.setTime(".");

        p12.setPresenterName("Presenters: Setyaningsih, \n" +
                "Retno Wulandari;\n Lestari, Puji Audina\n" +
                "Authors: Lestari, \n" +
                "Puji Audina; Setyaningsih," +
                "\n Retno Wulandari");
        p12.setTitle("Singlish Idiomatic Expressions\n" +
                " in Crazy Rich Asian (2018) Movie and \n" +
                "Its Indonesian Subtitles");
        p12.setStrands("Anthropological\n Linguistics");
        p12.setID("229");
        p12.setRoom("R.03");
        p12.setTime("Fri \n08:30 – 10:00");

        p13.setPresenterName("Presenter: Shebin M.S\n" +
                "Author: Shebin M.S");
        p13.setTitle("Myths and Festivals of Tribal\n" +
                " Goddess Kuttathamma: Anthropological \n" +
                "Study of The Nilgiri Biosphere\n Tribes");
        p13.setStrands("Anthropological \nLinguistics");
        p13.setID("190");
        p13.setRoom(".");
        p13.setTime(".");

        p14.setPresenterName("Presenter: Singh, Harjit\n" +
                "Author: Singh, Harjit");
        p14.setTitle("Coherence in Punjabi:\n" +
                "Discourse oriented " +
                "\ngeneralizations");
        p14.setStrands("Anthropological \nLinguistics");
        p14.setID("225");
        p14.setRoom(".");
        p14.setTime(".");

        p15.setPresenterName("Presenters: Singh, Satyapal;\n Kaushalya\n" +
                "Author: Kaushalya");
        p15.setTitle("Sociology of Sanskrit \n" +
                "Language: in the Context \nof Women and Shudras");
        p15.setStrands("Language \nSocialization");
        p15.setID("171");
        p15.setRoom("R.05");
        p15.setTime("Fri \n08:30 – 10:00");

        p16.setPresenterName("Presenter: Slaměníková, \nTereza\n" +
                "Author: Slaměníková, \nTereza");
        p16.setTitle("Dog Naming Strategies\n in Beijing, China");
        p16.setStrands("Anthropological\n Linguistics");
        p16.setID("192");
        p16.setRoom(".");
        p16.setTime(".");

        p17.setPresenterName("Presenter: Soelistyarini, \nTitien Diah\n" +
                "Author: Soelistyarini, \nTitien Diah");
        p17.setTitle("Negotiating a Hybrid Identity:\n" +
                " Exploring Verbal and Visual Representations\n" +
                " of Asian American Immigrants\n in a Graphic Memoir");
        p17.setStrands("Anthropological \nLinguistics");
        p17.setID("228");
        p17.setRoom("R.04");
        p17.setTime("Thurs \n15:00 – 16:30");

        p18.setPresenterName("Presenter: Sperrazza, \nLelania\n" +
                "Authors: Sperrazza,\n Lelania");
        p18.setTitle("Implications of \'Success Narratives\'" +
                "\n on Writer Identity");
        p18.setStrands("Narrative and \nMetanarrative\n" +
                "\n");
        p18.setID("120");
        p18.setRoom(".");
        p18.setTime(".");

        p19.setPresenterName("Presenters: Sriarun,\n" +
                " Autaiwan; Teerapaowpong,\n Peerapong\n" +
                "Authors: Sriarun, Autaiwan; \nTeerapaowpong,\n Peerapong");
        p19.setTitle("Early childhood music teaching\n" +
                " at the Faculty of Education, \nNakhon Phanom University");
        p19.setStrands("Text, \nContext,\n Entextualization");
        p19.setID("114");
        p19.setRoom(".");
        p19.setTime(".");

        p20.setPresenterName("Presenter: Sumanasara Thero,\n Ethkandure\n" +
                "Author: Sumanasara Thero, \nEthkandure");
        p20.setTitle("Origin Perspective\n and Development of\n Dhyana");
        p20.setStrands("Language\n Documentation\n" +
                "\n");
        p20.setID("");
        p20.setRoom("");
        p20.setTime("");

        p21.setPresenterName("Presenter: Sumartono, \nFirqin\n" +
                "Author: Sumartono\n Firqin");
        p21.setTitle("Beyond a Sinicized Singlish:\n" +
                " An Exploration of Subvariants of \n" +
                "Singlish through Discourse Particles");
        p21.setStrands("Language, \nCommunity,\n Ethnicity");
        p21.setID(".");
        p21.setRoom("R.01");
        p21.setTime("Wed \n15:00 – 16:30");

        p22.setPresenterName("Presenter: Suyao, \nFaye Coleen Rosales\n" +
                "Author: Suyao,\n Faye Coleen Rosales");
        p22.setTitle("Cybora The Online Dating Application\n" +
                " Engagement Among Gays\n and Meeting Afam Offline");
        p22.setStrands("Anthropological \nLinguistics");
        p22.setID("201");
        p22.setRoom(".");
        p22.setTime(".");

        p23.setPresenterName("Presenter: Szabó,\n Péter\n" +
                "Author: Szabó, \nPéter");
        p23.setTitle("Local, regional and global\n" +
                " context scales of discursive\n" +
                " identification by Lingua Franca \n" +
                "English practice: The case of\n the European Parliament");
        p23.setStrands("Language Ideologies");
        p23.setID("69");
        p23.setRoom(".");
        p23.setTime(".");


        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);
        presenterList.add(p5);
        presenterList.add(p6);
        presenterList.add(p7);
        presenterList.add(p8);
        presenterList.add(p9);
        presenterList.add(p10);
        presenterList.add(p11);
        presenterList.add(p12);
        presenterList.add(p13);
        presenterList.add(p14);
        presenterList.add(p15);
        presenterList.add(p16);
        presenterList.add(p17);
        presenterList.add(p18);
        presenterList.add(p19);
        presenterList.add(p20);
        presenterList.add(p21);
        presenterList.add(p22);
        presenterList.add(p23);
        addRows();
    }
    //Completed
    void CalaButtonT(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();
        presenter p5 = new presenter();
        presenter p6 = new presenter();
        presenter p7 = new presenter();
        presenter p8 = new presenter();
        presenter p9 = new presenter();
        presenter p10 = new presenter();

        //set all attributes
        p1.setPresenterName("Presenter: Talam, Anna Marie P.\n" +
                "Author: Talam, Anna Marie P.");
        p1.setTitle("Perceived Concept and Meaning of\n Development among Locals in Dakit,\n Guadalupe, Cebu City");
        p1.setStrands("Language,\n Community,\n Ethnicity");
        p1.setID("176");
        p1.setRoom(".");
        p1.setTime(".");

        //add all presenters in list
        p2.setPresenterName("Presenter: Tan, Ying Ying\nAuthor: Tang, Ying Ying");
        p2.setTitle("New Borrowings and Disused \n" +
                "Loans in a Changing Linguistic \nEcology");
        p2.setStrands("Language Contact \nand Change");
        p2.setID("132");
        p2.setRoom("R.02");
        p2.setTime("Wed \n17:00 – 18:30");

        p3.setPresenterName("Presenter: Tayeh, Brohanah\nAuthors: Tayeh, Brohanah; Kaping, \nKamila; Samae, Nadeehah; Yossiri, \nVaravejbhisis");
        p3.setTitle("The Language Maintenance and\n" +
                "Identities of the Thai-Melayu \n" +
                "Ethnic Group in Jaleh Village, \n" +
                "Yarang District, Pattani \nProvince, Thailand");
        p3.setStrands("Language, \nCommunity, \nEthnicity");
        p3.setID("27");
        p3.setRoom("R.03");
        p3.setTime("Wed 17:00 – 18:30");

        p4.setPresenterName("Presenters: Teerapaowpong,\n" +
                " Peerapong; Sriarun," +
                "\n Autaiwan\nAuthors: Teerapaowpong, Peerapong;\n" +
                " Sriarun, Autaiwan");
        p4.setTitle("Early childhood music teaching \n" +
                "at the Faculty of Education,\n Nakhon Phanom University");
        p4.setStrands("Text,\n Context,\n Entextualization");
        p4.setID("114");
        p4.setRoom(".");
        p4.setTime(".");

        p5.setPresenterName("Presenter: Thayyil, Jamsheer\nAuthor: Thayyil, Jamsheer");
        p5.setTitle("Kinship Terminology and \n" +
                "Term of Reference among Muthuvan \n" +
                "Tribal Community in South India");
        p5.setStrands("Anthropological \nLinguistics");
        p5.setID("173");
        p5.setRoom(".");
        p5.setTime(".");

        p6.setPresenterName("Presenter: Toring, Sherwin\nAuthor: Toring, Sherwin");
        p6.setTitle("Language Ideologies: The \n" +
                "Chavacano Speakers in a \nCebuano Setting");
        p6.setStrands("Language Ideologies");
        p6.setID("34");
        p6.setRoom(".");
        p6.setTime(".");

        p7.setPresenterName("Hien \nAuthor: Tran, Hien");
        p7.setTitle("The Vietnamese ruột \'intestine\'\n" +
                " – the seat of life: The Conceptualizations \n" +
                "of ruột in Vietnamese");
        p7.setStrands("Anthropological \nLinguistics");
        p7.setID("193");
        p7.setRoom(".");
        p7.setTime(".");

        p8.setPresenterName("Presenter: Trinh, Cam Lan\nAuthor: Trinh, Cam Lan");
        p8.setTitle("Dialect Contact in Vietnam \n" +
                "from Accommodation Theory Perspective:\n" +
                "Evidence from Some Dialect Communities");
        p8.setStrands("Language Contact \nand Change");
        p8.setID("58");
        p8.setRoom(".");
        p8.setTime(".");

        p9.setPresenterName("Presenters: Trivedi, Bageshree;\n" +
                " Trivedi, Devashree\nAuthors: Trivedi, " +
                "Bageshree;\n Trivedi, Devashree");
        p9.setTitle("Global Challenges, Local Solutions:\n Linguistic Anthropology as Narrative \nDevice in Dhruv Bhatt’s Gujarati Novels");
        p9.setStrands("Anthropological Linguistic");
        p9.setID("216");
        p9.setRoom(".");
        p9.setTime(".");

        p10.setPresenterName("Presenters: Trivedi, Devashree;\n" +
                " Trivedi, Bageshree\nAuthors: Trivedi, Bageshree;\n" +
                " Trivedi, Devashree");
        p10.setTitle("Global Challenges, Local Solutions: \n" +
                "Linguistic Anthropology as Narrative Device \n" +
                "in Dhruv Bhatt’s Gujarati Novels");
        p10.setStrands("Anthropological \nLinguistics");
        p10.setID("216");
        p10.setRoom(".");
        p10.setTime(".");

        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);
        presenterList.add(p5);
        presenterList.add(p6);
        presenterList.add(p7);
        presenterList.add(p8);
        presenterList.add(p9);
        presenterList.add(p10);
        addRows();
    }
    //Complete
    void CalaButtonU(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenter: Usman\n" +
                "Author: Usman");
        p1.setTitle("Reclaiming Pashtuns Identity: The role of \ninformal spaces in developing an alternative \nNarrative");
        p1.setStrands("Language\n Minorities and \nMajorities");
        p1.setID("187");
        p1.setRoom(".");
        p1.setTime(".");

        //add all presenters in list
        presenterList.add(p1);
        addRows();
    }
    //Complete
    void CalaButtonV(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenter: Vollmann, Ralf\n" +
                "Authors: Vollmann, Ralf; Tek, Wooi Soon");
        p1.setTitle("The Indian Hakkas of Vienna");
        p1.setStrands("Language, \nCommunity, \nEthnicity");
        p1.setID("47");
        p1.setRoom("R.03");
        p1.setTime("Wed 17:00 – 18:30");


        presenter p2 = new presenter();
        //set all attributes
        p2.setPresenterName("Presenter: Vollmann, Ralf\n" +
                "Authors: Vollmann, Ralf; Tek, Wooi Soon");
        p2.setTitle("The Sociolinguistic Status of Malaysian English: \nSpoken Varieties and Standard Language");
        p2.setStrands("Language, Dialect, \nSociolect, Genre");
        p2.setID("48");
        p2.setRoom("R.03");
        p2.setTime("Fri 15:00 – 16:30");

        presenter p3 = new presenter();
        //set all attributes
        p3.setPresenterName("Presenter: Vu, Trang Hong\n" +
                "Author: Vu, Trang Hong");
        p3.setTitle("Simplification and Standardization of Chinese\n Characters in the 1950s – A Social Inclusion\n Policy in Mainland China?");
        p3.setStrands("Text, Context,\n Entextualization");
        p3.setID("245");
        p3.setRoom(".");
        p3.setTime(".");
        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        addRows();
    }
    //cpmleted
    void CalaButtonW(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();
        presenter p5 = new presenter();
        presenter p6 = new presenter();

        //set all attributes
        p1.setPresenterName("Presenter: Wan Farah, Wani binti\n Wan Fakhruddin\n" +
                "Author: Wan Farah, Wani binti \nWan Fakhruddin");
        p1.setTitle("A Systemic Linguistic Analysis of Malay Process\n Types in Professional Forestry Report Genre");
        p1.setStrands("Language, Dialect, \nSociolect, Genre");
        p1.setID("232");
        p1.setRoom(".");
        p1.setTime(".");


        //add all presenters in list
        p2.setPresenterName("Presenter: Wang, Li \n" +
                "Authors: Wang, Li; Ang, Lay Hoon; \n" +
                "Sabariah, Md Rashid;\n Hazlina, Abdul Halim");
        p2.setTitle("Narrative Constitution of Corporate \n" +
                "Identities in Corporate Profiles of \n" +
                "Multinational Corporations of \n" +
                "China — A Case Study");
        p2.setStrands("Narrative and\n Metanarrative");
        p2.setID("140");
        p2.setRoom("R.02");
        p2.setTime("Wed \n15:00 – 16:30");

        p3.setPresenterName("Presenter: Wang, Wei\nAuthor: Wang, Wei");
        p3.setTitle("Kam people’s ethnic \n" +
                "identity in narratives: \nA linguistic ethnographic study");
        p3.setStrands("Narrative and\n Metanarrative");
        p3.setID("5");
        p3.setRoom("");
        p3.setTime("");

        p4.setPresenterName("Presenter: Weixing, Huang\n" +
                "Authors: Weixing, Huang; \n" +
                "Ang, Lay Hoon; \n" +
                "Ser, Wue Hiong;\n Hardev, Kaur");
        p4.setTitle("Text-remote Thick Translation of\n" +
                " Lin Yutang’s English\n Version of Laozi");
        p4.setStrands("Text, \nContext, \nEntextualization");
        p4.setID("143");
        p4.setRoom("R.02");
        p4.setTime("Thurs\n 08:30 – 10:00");

        p5.setPresenterName("Presenter: Wijana,\n I Dewa Putu\n" +
                "Author: Wijana,\n I Dewa Putu");
        p5.setTitle("Metaphors of turtle dove physical \n" +
                "characteristics in Javanese community:\n A preliminary study");
        p5.setStrands("Applied \nSociolinguistics");
        p5.setID("7");
        p5.setRoom("R.05");
        p5.setTime("Wed \n17:00 – 18:30");

        p6.setPresenterName("Presenter: Wu, Jiaye\nAuthor: Wu, Jiaye");
        p6.setTitle("Teaching Mandarin pronunciation \n" +
                "to Mongolian learners in early Republican \n" +
                "period: the case of Mongolian-Han \n" +
                "bilingual Original Sounds of \nFive Regions");
        p6.setStrands("Anthropological\nLinguistics");
        p6.setID("233");
        p6.setRoom("");
        p6.setTime("");

        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);
        presenterList.add(p5);
        presenterList.add(p6);
        addRows();
    }
    //Complete
    void CalaButtonX(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenter: Xia, Yihui\n" +
                "Author: Xia, Yihui");
        p1.setTitle("A Contrastive Analysis of \"Laugher\" \n Onomatopoeia and Mimetic Words between \nJapanese and Chinese");
        p1.setStrands("Language \n Socialization");
        p1.setID("88");
        p1.setRoom("R.05");
        p1.setTime("Thurs 15:00 – 16:30");

        //add all presenters in list
        presenterList.add(p1);
        addRows();
    }
    //Complete
    void CalaButtonY(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenter: Yadav, Shyam Sundar\n Prasad\n" +
                "Author: Yadav, Shyam Sundar\n Prasad");
        p1.setTitle("Vanishing Landlordism In Madhes, Nepal:\n Missing an Opportunity for Development of\n Capitalism");
        p1.setStrands("Text, Context and \n Entextualization");
        p1.setID("238");
        p1.setRoom("R.02");
        p1.setTime("Fri 15:00 – 16:30");

        presenter p2 = new presenter();
        //set all attributes
        p2.setPresenterName("Presenter: Yann, Wong Ling\n" +
                "Author: Yann, Wong Ling");
        p2.setTitle("Local Chinese Dialects and Toponym of The \nChinese Streets in Sibu, Sarawak");
        p2.setStrands("Language, Dialect,\n Sociolect, Genre");
        p2.setID("169");
        p2.setRoom("R.03");
        p2.setTime("Fri 15:00 – 16:30");

        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        addRows();
    }
    //Competed
    void CalaButtonZ(){
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        presenter p3 = new presenter();
        presenter p4 = new presenter();
        presenter p5 = new presenter();
        presenter p6 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenter: Zappa, Marco\n" +
                "Author: Zappa, Marco");
        p1.setTitle("Pleasing the “Bubble”: \nAbe Shinzō’s Strategical \nSelf-exhibition on Facebook");
        p1.setStrands("Language in Real\n and Virtual\n Spaces");
        p1.setID("202");
        p1.setRoom("R.03");
        p1.setTime("Fri 08:30 – 10:00");

        p2.setPresenterName("Presenter: Zhang, Qi \n" +
                "Authors: Zhang, Qi; \nAng, Lay Hoon");
        p2.setTitle("Subtitle Translation Strategies\n" +
                " and Influencing Factors of Chinese \n" +
                "Documentary – A Bite of China 1");
        p2.setStrands("Anthropological\n Linguistics");
        p2.setID("241");
        p2.setRoom("R.03");
        p2.setTime("Fri\n 13:00 – 14:30");

        p3.setPresenterName("Presenter: Zhao, Meijuan \nAuthors: Zhao, Meijuan; Ang, Lay Hoon; \nSabariah, Bt Md Rashid; Florence, Toh Haw Ching");
        p3.setTitle("Translating Narrative\n" +
                " Space in Cao Wenxuan’s \nBronze and Sunflower");
        p3.setStrands("Language and \nSpatiotemporal Frames");
        p3.setID("73");
        p3.setRoom("R.01");
        p3.setTime("Thurs \n08:30 – 10:00");

        p4.setPresenterName("Presenter: Zhu, Hongxiang \n" +
                "Authors: Zhu, Hongxiang;\n Ang, Lay Hoon");
        p4.setTitle("Sound of Weapons in \nThe Legends of the\n Condor Heroes");
        p4.setStrands("Language in Real \n and Virtual Spaces");
        p4.setID("242");
        p4.setRoom("R.03");
        p4.setTime("R.03");


        p5.setPresenterName("Presenter: Zhu, Li \nAuthors: Zhu, Li; Ang, Lay Hoon");
        p5.setTitle("Emotional Appeals in " +
                "\nChildren’s Powdered Milk\n" +
                " Advertising Language Across \nCultures");
        p5.setStrands("Language in Real \nand Virtual Spaces");
        p5.setID("243");
        p5.setRoom("R.02");
        p5.setTime("Thurs \n08:30 – 10:00");

        p6.setPresenterName("Presenter: Muhd Zulkifli, bin Ismail \nAuthors: Muhd Zulkifli, bin Ismail;\n Mohd Zariat, Abdul Rani");
        p6.setTitle("Hadrah: The Folk Art Heritage \n" +
                "in Contemporary Malaysia");
        p6.setStrands("General \nLinguistic Anthropology");
        p6.setID("261");
        p6.setRoom("R.05");
        p6.setTime("Wed 17:00 – 18:30");

        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        presenterList.add(p3);
        presenterList.add(p4);
        presenterList.add(p5);
        presenterList.add(p6);
        addRows();
    }
}
