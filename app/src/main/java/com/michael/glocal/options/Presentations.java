package com.michael.glocal.options;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;
import android.widget.Toast;

import com.michael.glocal.R;

public class Presentations extends Activity {
    private SharedPreferences mprefrences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presentations);
        setTitle("Presentation");



        mprefrences = PreferenceManager.getDefaultSharedPreferences(this);
        String event_name = mprefrences.getString("Event", "default");
        if (event_name.equals("CALA")) {
            TextView data = findViewById(R.id.data_textview);
            TextView subtitle = findViewById(R.id.subtitle);
        }
        if(event_name.equals("Comela"))
        {
            TextView data = findViewById(R.id.data_textview);
            TextView subtitle = findViewById(R.id.subtitle);
            subtitle.setText("Colloquia");
            data.setText("1.5 hours with 3-5 contributors \n" +
                    "Colloquia provide an opportunity for several presenters within the boundaries of a theme or topic to present together in a supportive environment. Colloquium organizers can tailor time lengths to each presentation, but must allow time to include opening and closing remarks, presentations, and audience interaction."
            +"\nThe colloquium chair is responsible for organizing the group and for submitting the colloquium in the Call for Papers platform on behalf of all their contributors.\n"
            +"All colloquium submissions must be uploaded to the submission site using the Colloquium Submission Form Template provided.\n"+
            "Panel organizers here will be responsible for panel chairs. Time keepers will be provided by the COMELA."+
                    "General and non-colloquia session papers"+
                    "Paper presentations will be allocated 25 minutes including 5 minutes for questions and answers.\n\n" +
                    "Time keepers will be provided by the COMELA.\n\n"+
                    "Posters\n\n"+
                    "    Posterswill be displayed at designated times throughout the COMELA.\n"+
                    "    Posters are for one-on-one discussion of a symposium-related theme or topic.\n"+
                    "    A block of time will be allocated for presenters to discuss their posters.\n"+
                    "    Posters will be displayed in the designated areas (main halls).\n"+
                    "    Presenters will be assigned a space to display their poster.\n"+

                    "    Posters should be sized A0: 841 x 1189 mm width x height/ 33.1 x 46.8 inches width x height (landscape or portrait).\n"+
                    "    Submissions should contain a summary of key elements for the presentation.\n"+
                    "    Posters can include any relevant visuals or academically descriptive objects, where color is visually optimal.\n"+
                    "    Posters must be clear, concise, and simple, with large fonts, with the title of the project, and author information (name and affiliation).");

        }
    }
}
