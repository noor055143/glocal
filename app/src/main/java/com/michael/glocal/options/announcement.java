package com.michael.glocal.options;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import com.michael.glocal.R;

public class announcement extends Activity {
    private SharedPreferences mprefrences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_announcement);
        setTitle("Announcement");
        mprefrences = PreferenceManager.getDefaultSharedPreferences(this);
        String event_name = mprefrences.getString("Event", "default");
        if (event_name.equals("CALA")) {
            TextView data = findViewById(R.id.data_textview);
            TextView subtitle = findViewById(R.id.subtitle);
            subtitle.setText("");
            subtitle.setVisibility(View.GONE);
            data.setMovementMethod(new ScrollingMovementMethod());

            data.setText("Check this page for updates and announcements.\n\n");
        }
        if(event_name.equals("Comela"))
        {
            TextView data = findViewById(R.id.data_textview);
            TextView subtitle = findViewById(R.id.subtitle);
            subtitle.setText("");
            subtitle.setVisibility(View.GONE);
            data.setMovementMethod(new ScrollingMovementMethod());

            data.setText("Check this page for updates and announcements.\n\n");
        }
    }
}
