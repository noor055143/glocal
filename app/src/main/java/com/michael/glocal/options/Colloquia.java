package com.michael.glocal.options;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.michael.glocal.R;
import com.michael.glocal.models.ColloquiaList;
import com.michael.glocal.models.ColloquiaModel;
import com.michael.glocal.models.ColloquiaPresenterModel;

import java.util.ArrayList;

public class Colloquia extends Activity implements View.OnClickListener {
    private SharedPreferences mprefrences;
    String event_name;
    TableLayout tableLayout;
    ArrayList<ColloquiaModel> OrganizersList;
    ArrayList<ColloquiaPresenterModel> presentersList;
    Button btnOrganizers, btnPresenters;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colloquia);
        setTitle("Colloquia");

        initViews();
        if(event_name.equals("CALA")) {
            CalabtnOrganizers();
        }
        if(event_name.equals("Comela")) {
            ComeleOrganizers();
        }
    }
    void initViews(){
        tableLayout = (TableLayout) findViewById(R.id.tableOrganizers);
        btnOrganizers = (Button) findViewById(R.id.btnOrganizers);
        btnPresenters = (Button) findViewById(R.id.btnPresenters);

        //Set Onclick Listeners;
        btnOrganizers.setOnClickListener(this);
        btnPresenters.setOnClickListener(this);

        mprefrences = PreferenceManager.getDefaultSharedPreferences(this);
        event_name = mprefrences.getString("Event", "default");
    }
    public void addOrganizersHeaders(){
        TableLayout t1 = (TableLayout) findViewById(R.id.tableOrganizers);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());
        tr.addView(getTextView(0, "Organizers", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "Title", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "Chair/Discussant", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "Strand", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "ID    ", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        t1.addView(tr, getTblLayoutParams());
    }
    public void addPresentersHeaders(){
        TableLayout t1 = (TableLayout) findViewById(R.id.tableOrganizers);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());

        tr.addView(getTextView(0, "Presenter", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "Title", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "Organizer", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "Chair/Discussant", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "Strand", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "ID", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));

        t1.addView(tr, getTblLayoutParams());
    }
    private TextView getTextView(int id, String Title, int color, int TypeFace, int bg){
        TextView tv = new TextView(this);
        tv.setId(id);
        tv.setText(Title.toUpperCase());
        tv.setTextColor(color);
        tv.setPadding(40,40,40,40);
        tv.setTypeface(Typeface.DEFAULT, TypeFace);
        tv.setBackgroundColor(bg);
        tv.setBackgroundResource(bg);
        tv.setLayoutParams(getLayoutParams());
        return tv;
    }
    private TextView getRowsTextView(int id, String Title, int color, int bg){
        TextView tv = new TextView(this);
        tv.setId(id);
        tv.setText(Title);
        tv.setTextColor(color);
        tv.setPadding(40,40,40,40);
        tv.setTypeface(Typeface.DEFAULT);
        tv.setBackgroundColor(bg);
        tv.setBackgroundResource(bg);
        tv.setLayoutParams(getLayoutParams());
        return tv;
    }
    private TableRow.LayoutParams getLayoutParams(){
        TableRow.LayoutParams params = new TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(1,1,1,1);
        params.weight = 1;
        return  params;
    }
    private TableLayout.LayoutParams getTblLayoutParams(){
        return new TableLayout.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT
        );

    }
    void addRows(){
        // Collections.reverse(presenterList);
        for(int i=0; i < OrganizersList.size() ; i ++){
            TableRow tr= new TableRow(Colloquia.this);
            tr.setLayoutParams(getLayoutParams());

            tr.addView(getRowsTextView(0, OrganizersList.get(i).getOrganizer(), Color.BLACK, R.drawable.cell_shape));
            tr.addView(getRowsTextView(0, OrganizersList.get(i).getTitle(), Color.BLACK , R.drawable.cell_shape));
            tr.addView(getRowsTextView(0, OrganizersList.get(i).getChair(), Color.BLACK, R.drawable.cell_shape));
            tr.addView(getRowsTextView(0, OrganizersList.get(i).getStrand(), Color.BLACK,R.drawable.cell_shape));
            tr.addView(getRowsTextView(0, OrganizersList.get(i).getId(), Color.BLACK,  R.drawable.cell_shape));

            tableLayout.addView(tr, getTblLayoutParams());
        }
    }
    void removeRows(){
        while (tableLayout.getChildCount() > 1) {
            TableRow row =  (TableRow)tableLayout.getChildAt(1);
            tableLayout.removeView(row);
        }
    }
    void CalabtnOrganizers(){
        addOrganizersHeaders();
        removeRows();
        OrganizersList = new ArrayList<ColloquiaModel>();
        //make one object
        ColloquiaModel p1 = new ColloquiaModel();
        ColloquiaModel p2 = new ColloquiaModel();
        ColloquiaModel p3 = new ColloquiaModel();
        ColloquiaModel p4 = new ColloquiaModel();
        ColloquiaModel p5 = new ColloquiaModel();
        ColloquiaModel p6 = new ColloquiaModel();
        ColloquiaModel p7 = new ColloquiaModel();
        ColloquiaModel p8 = new ColloquiaModel();
        ColloquiaModel p9 = new ColloquiaModel();
        ColloquiaModel p10 = new ColloquiaModel();

        //set all attributes
        p1.setOrganizer("Channa, Abdul Razaque");
        p1.setTitle("Preserving Heritage Languages: Problems and Prospects");
        p1.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p1.setStrand("Language Revitalization");
        p1.setId("55");

        //p2
        p2.setOrganizer("Galeste, Carmen Angela V.");
        p2.setTitle("Measurements of Grammatical Morphemes of Typically \nDeveloping Filipino-Dominant 4:0-4:11 Children in Metro \nManila: A Protocol Development");
        p2.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p2.setStrand("Language Socialization");
        p2.setId("135");
        //p3
        p3.setOrganizer("Jackson, Jane");
        p3.setTitle("Multilingualism, transnationalism and Identity \nreconstruction");
        p3.setChair("Chair\n" +
                "Jackson, Jane\n" +
                "\n" +
                "Discussant\n" +
                "None");
        p3.setStrand("Narrative and Metanarrative");
        p3.setId("160");
        //p4
        p4.setOrganizer("Li, David Chor Shing");
        p4.setTitle("Written Chinese as a scripta franca for cross-border \ncommunication in early modern East Asia: How \n" +
                "brushtalk (漢文筆談) allowed literati of Sinitic to conduct\n \'silent conversation\' face-to-face");
        p4.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p4.setStrand("Anthropological Linguistics");
        p4.setId("167");
        //p5
        p5.setOrganizer("Muhamad, Fadzllah Hj Zaini");
        p5.setTitle("Perception And Metaphorical Smell: A Malay Manuscript\n Studies (Petua Membina Rumah) As One Of The Asian\n Texts");
        p5.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p5.setStrand("Cognitive Anthropology\n and Language");
        p5.setId("89");
        //p6
        p6.setOrganizer("Nguyen, Thi Hong Minh");
        p6.setTitle("Vietnamese Proverbs: Values Preserved in The Modern\n Society");
        p6.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p6.setStrand("Language Community\n Ethnicity");
        p6.setId("136");
        //p7
        p7.setOrganizer("Prachumwan, Komphorn");
        p7.setTitle("Identity of Thai Saek Ethnic group, Nakhon Phanom\n Province: Ten Sak Ceremony");
        p7.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p7.setStrand("Language, Community,\n Ethnicity");
        p7.setId("116");
        //p8
        p8.setOrganizer("Santos, Mari-An");
        p8.setTitle("From Longing to Belonging: Emotions and Experiences \nof Filipina Migrant Workers in Valencia, Spain");
        p8.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p8.setStrand("Language, Gender, Sexuality");
        p8.setId("67");
        //p9
        p9.setOrganizer("Riva, Natalia");
        p9.setTitle("A Glass of Two Languages: The Specialized Terminology \nof Italian Wine in Chinese");
        p9.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p9.setStrand("General Sociolinguistics");
        p9.setId("222");




        //add all presenters in list
        OrganizersList.add(p1);
        OrganizersList.add(p2);
        OrganizersList.add(p3);
        OrganizersList.add(p4);
        OrganizersList.add(p5);
        OrganizersList.add(p6);
        OrganizersList.add(p7);
        OrganizersList.add(p8);
        OrganizersList.add(p9);

        addRows();
    }
    void CalaBtnPresenters(){
        addPresentersHeaders();
        removeRows();
        presentersList = new ArrayList<ColloquiaPresenterModel>();
        ColloquiaPresenterModel p1 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p2 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p3 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p4 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p5 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p6 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p7 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p8 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p9 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p10 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p11 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p12 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p13 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p31 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p15 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p16 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p17 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p18 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p19 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p20 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p21 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p22 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p23 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p24 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p25 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p26 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p27 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p28 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p29 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p30 = new ColloquiaPresenterModel();
        //p1
        p1.setPresenter("Anida, Sarudin");
        p1.setOrganizer("Muhamad, Fadzllah Hj\n Zaini");
        p1.setTitle("Perception And Metaphorical Smell: A\n Malay Manuscript Studies (Petua \nMembina Rumah) As One Of The Asian\n Texts");
        p1.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p1.setStrand("Cognitive Anthropology\n and Language");
        p1.setId("89");
        //p2
        p2.setPresenter("Ataso, Chalermpol");
        p2.setOrganizer("Prachumwan, Komphorn");
        p2.setTitle("Identity of Thai Saek Ethnic group, \nNakhon Phanom Province: Ten Sak\n Ceremony");
        p2.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p2.setStrand("Language, Community,\n Ethnicity");
        p2.setId("89");
        //p3
        p3.setPresenter("Bertulessi, Chiara");
        p3.setOrganizer("Riva, Natalia");
        p3.setTitle("A Glass of Two Languages: The \nSpecialized Terminology of Italian Wine\n in Chinese");
        p3.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p3.setStrand("General Sociolinguistics");
        p3.setId("222");
        //p4
        p4.setPresenter("Butigan, Krizia Wenamarie \nR.");
        p4.setOrganizer("Galeste, Carmen Angela V.");
        p4.setTitle("Measurements of Grammatical\n Morphemes of Typically Developing \nFilipino-Dominant 4:0-4:11 Children in\n Metro Manila: A Protocol Development");
        p4.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p4.setStrand("Language Documentation");
        p4.setId("135");
        //p5
        p5.setPresenter("Bondoc-Bundoc, Mara Jo");
        p5.setOrganizer("Galeste, Carmen Angela V.");
        p5.setTitle("Measurements of Grammatical\n Morphemes of Typically Developing \nFilipino-Dominant 4:0-4:11 Children in\n Metro Manila: A Protocol Development");
        p5.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p5.setStrand("Language Socialization");
        p5.setId("135");
        //p6
        p6.setPresenter("Camangian, Franchesca Mae S.");
        p6.setOrganizer("Galeste, Carmen Angela V.");
        p6.setTitle("Measurements of Grammatical\n Morphemes of Typically Developing \nFilipino-Dominant 4:0-4:11 Children in\n Metro Manila: A Protocol Development");
        p6.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p6.setStrand("Language Documentation");
        p6.setId("135");
        //p7
        p7.setPresenter("Channa, Abdul Razaque");
        p7.setOrganizer("Channa, Abdul Razaque");
        p7.setTitle("Preserving Heritage Languages:\n Problems and Prospects");
        p7.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p7.setStrand("Language Revitalization");
        p7.setId("55");
        //p8
        p8.setPresenter("Channa, Liaquat Ali");
        p8.setOrganizer("Channa, Abdul Razaque");
        p8.setTitle("Preserving Heritage Languages: \nProblems and Prospects");
        p8.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p8.setStrand("Language Revitalization");
        p8.setId("55");
        //p9
        p9.setPresenter("Escario, Madelyn Cecilia C.");
        p9.setOrganizer("Galeste, Carmen Angela V.");
        p9.setTitle("Measurements of Grammatical\n Morphemes of Typically Developing \nFilipino-Dominant 4:0-4:11 Children in\n Metro Manila: A Protocol Development");
        p9.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p9.setStrand("Language Socialization");
        p9.setId("135");
        //p10
        p10.setPresenter("Galeste, Carmen Angela V.");
        p10.setOrganizer("Galeste, Carmen Angela V.");
        p10.setTitle("Measurements of Grammatical\n Morphemes of Typically Developing \nFilipino-Dominant 4:0-4:11 Children in\n Metro Manila: A Protocol Development");
        p10.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p10.setStrand("Language Socialization");
        p10.setId("135");
        //p11
        p11.setPresenter("Gerona, Jonathan M.");
        p11.setOrganizer("Galeste, Carmen Angela V.");
        p11.setTitle("Measurements of Grammatical\n Morphemes of Typically Developing \nFilipino-Dominant 4:0-4:11 Children in\n Metro Manila: A Protocol Development");
        p11.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p11.setStrand("Language Socialization");
        p11.setId("135");
        //p12
        p12.setPresenter("Jackson, Jane");
        p12.setOrganizer("Jackson, Jane");
        p12.setTitle("Making sense of Evolving L2 Identities\n and International Educational Experience");
        p12.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p12.setStrand("Narrative and \nMetanarrative");
        p12.setId("160");
        //p13
        p13.setPresenter("Lai, Janelle Mae");
        p13.setOrganizer("Galeste, Carmen Angela V.");
        p13.setTitle("Measurements of Grammatical \n Morphemes of Typically Developing \nFilipino-Dominant 4:0-4:11 Children in \nMetro Manila: A Protocol Development");
        p13.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p13.setStrand("Language Socialization");
        p13.setId("135");

        p15.setPresenter("Li, David Chor Shing");
        p15.setOrganizer("Li, David Chor Shing");
        p15.setTitle("Written Chinese as a scripta franca for \ncross-border communication in early \nmodern East Asia: How brushtalk (漢文筆談)\n allowed literati of Sinitic to conduct\n \'silent conversation\' face-to-face");
        p15.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p15.setStrand("Anthropological \nLinguistics");
        p15.setId("167");
        //p16
        p16.setPresenter("Lupano, Emma");
        p16.setOrganizer("Riva, Natalia");
        p16.setTitle("A Glass of Two Languages: The\n Specialized Terminology of Italian Wine\n in Chinese");
        p16.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p16.setStrand("General Sociolinguistics");
        p16.setId("222");
        //p17
        p17.setPresenter("Manan, Syed Abdul");
        p17.setOrganizer("Channa, Abdul Razaque");
        p17.setTitle("Preserving Heritage Languages: \nProblems and Prospects");
        p17.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p17.setStrand("Language Revitalization");
        p17.setId("55");
        //p18
        p18.setPresenter("Moonmueangsaen, Teerawut");
        p18.setOrganizer("Prachumwan, Komphorn");
        p18.setTitle("Identity of Thai Saek Ethnic group, \nNakhon Phanom Province: Ten Sak \nCeremony");
        p18.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p18.setStrand("Language, Community,\n Ethnicity");
        p18.setId("89");
        //p19
        p19.setPresenter("Ngo, Thi Thanh Quy");
        p19.setOrganizer("Nguyen, Thi Hong Minh");
        p19.setTitle("Vietnamese Proverbs: Values Preserved \nin The Modern Society");
        p19.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p19.setStrand("Language, Community,\n Ethnicity");
        p19.setId("136");
        //p20
        p20.setPresenter("Nguyen, Thi Hong Minh");
        p20.setOrganizer("Nguyen, Thi Hong Minh");
        p20.setTitle("Vietnamese Proverbs: Values Preserved \nin The Modern Society");
        p20.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p20.setStrand("Language, Community,\n Ethnicity");
        p20.setId("136");
        //p21
        p21.setPresenter("Mazura, Mastura Muhammad");
        p21.setOrganizer("Muhamad, Fadzllah Hj\n Zaini");
        p21.setTitle("Perception And Metaphorical Smell: A\n Malay Manuscript Studies (Petua\n Membina Rumah) As One Of The Asian\n Texts");
        p21.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p21.setStrand("Cognitive Anthropology and \nLanguage");
        p21.setId("89");

        //p22
        p22.setPresenter("Muhamad, Fadzllah Hj\n Zaini");
        p22.setOrganizer("Muhamad, Fadzllah Hj\n Zaini");
        p22.setTitle("Perception And Metaphorical Smell: A\n Malay Manuscript Studies (Petua\n Membina Rumah) As One Of The Asian\n Texts");
        p22.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p22.setStrand("Cognitive Anthropology and \nLanguage");
        p22.setId("89");
        //p23
        p23.setPresenter("Prachumwan, Komphorn");
        p23.setOrganizer("Prachumwan, Komphorn");
        p23.setTitle("Identity of Thai Saek Ethnic group,\n Nakhon Phanom Province: Ten Sak \nCeremony");
        p23.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p23.setStrand("Language, Community, \nEthnicity");
        p23.setId("89");
        //p24
        p24.setPresenter("Reijiro, Aoyama");
        p24.setOrganizer("Li, David Chor Shing");
        p24.setTitle("Written Chinese as a scripta franca for \ncross-border communication in early\n modern East Asia: How brushtalk (漢文筆談)\n allowed literati of Sinitic to conduct \n \'silent conversation\' face-to-face");
        p24.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p24.setStrand("Anthropological \nLinguistics");
        p24.setId("67");
        //p25
        p25.setPresenter("Riva, Natalia");
        p25.setOrganizer("Riva, Natalia");
        p25.setTitle("A Glass of Two Languages: The\n Specialized Terminology of Italian Wine \nin Chinese");
        p25.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p25.setStrand("General Sociolinguistics");
        p25.setId("222");
        //p26
        p26.setPresenter("Saniah, Abu Bakar, Siti");
        p26.setOrganizer("Muhamad, Fadzllah Hj\n Zaini");
        p26.setTitle("Perception And Metaphorical Smell: A \nMalay Manuscript Studies (Petua\n Membina Rumah) As One Of The Asian\n Texts");
        p26.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p26.setStrand("Cognitive Anthropology \nand Language");
        p26.setId("89");
        //p27
        p27.setPresenter("Santos, Mari-An");
        p27.setOrganizer("Santos, Mari-An");
        p27.setTitle("From Longing to Belonging: Emotions\n and Experiences of Filipina Migrant\n Workers in Valencia, Spain");
        p27.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p27.setStrand("Language, Gender, \nSexuality");
        p27.setId("67");
        //p28
        p28.setPresenter("Sim, Nicole Arden Q.");
        p28.setOrganizer("Galeste, Carmen Angela V.");
        p28.setTitle("Measurements of Grammatical\n Morphemes of Typically Developing \nFilipino-Dominant 4:0-4:11 Children in\n Metro Manila: A Protocol Development");
        p28.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p28.setStrand("Language Socialization");
        p28.setId("135");
        //p29
        p29.setPresenter("Sun, Tongle");
        p29.setOrganizer("Jackson, Jane");
        p29.setTitle("The construction and negotiation of \nmultilingual identities: Transnational\n experiences of Chinese exchange\n students");
        p29.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p29.setStrand("Narrative and\n Metanarrative");
        p29.setId("160");
        //p30
        p30.setPresenter("Wirza, Yanty");
        p30.setOrganizer("Jackson, Jane");
        p30.setTitle("Complexity and the Identity\n Reconstruction of Indonesian Doctoral \nCandidates during Study Abroad");
        p30.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p30.setStrand("Narrative and\n Metanarrative");
        p30.setId("160");
        //p31
        p31.setPresenter("Wong, Tak-Sum");
        p31.setOrganizer("Li, David Chor Shing");
        p31.setTitle("Written Chinese as a scripta franca for \ncross-border communication in early\n modern East Asia: How brushtalk (漢文筆談)\n allowed literati of Sinitic to conduct\n \'silent conversation\' face-to-face");
        p31.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p31.setStrand("Anthropological \nLinguistics");
        p31.setId("167");

        //add all presenters in list
        presentersList.add(p1);
        presentersList.add(p2);
        presentersList.add(p3);
        presentersList.add(p4);
        presentersList.add(p5);
        presentersList.add(p6);
        presentersList.add(p7);
        presentersList.add(p8);
        presentersList.add(p9);
        presentersList.add(p10);
        presentersList.add(p11);
        presentersList.add(p12);
        presentersList.add(p13);
        presentersList.add(p15);
        presentersList.add(p16);
        presentersList.add(p17);
        presentersList.add(p18);
        presentersList.add(p19);
        presentersList.add(p20);
        presentersList.add(p21);
        presentersList.add(p22);
        presentersList.add(p23);
        presentersList.add(p24);
        presentersList.add(p25);
        presentersList.add(p26);
        presentersList.add(p27);
        presentersList.add(p28);
        presentersList.add(p29);
        presentersList.add(p30);
        presentersList.add(p31);

        addRows();
    }
    void ComeleOrganizers(){
        addOrganizersHeaders();
        removeRows();
        OrganizersList = new ArrayList<ColloquiaModel>();
        //make one object
        ColloquiaModel p1 = new ColloquiaModel();
        ColloquiaModel p2 = new ColloquiaModel();
        ColloquiaModel p3 = new ColloquiaModel();
        ColloquiaModel p4 = new ColloquiaModel();
        ColloquiaModel p5 = new ColloquiaModel();
        p1.setOrganizer("Bogdan Oprea, Helga-\nIuliana");
        p1.setTitle("Cinematography « sans frontières ». International Cultural\n Metaphors and Commonplaces in Romanian Cinema\n Terminology");
        p1.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p1.setStrand("Language in real and Virtual \nSpaces");
        p1.setId("52");

        //p2
        p2.setOrganizer("Candelario, Benji");
        p2.setTitle("Emergent Linguistic Anthropologies of the Mediterranean.\n New boundaries");
        p2.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p2.setStrand("Anthropological Linguistics");
        p2.setId("94");
        //p3
        p3.setOrganizer("Candelario, Benji");
        p3.setTitle("The Mediterranean LGBT: Transgressing Queer boundaries");
        p3.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p3.setStrand("Language, Gender, Sexuality\n");
        p3.setId("95");
        //p4
        p4.setOrganizer("Rubakova, Inna");
        p4.setTitle("Chastushka as a Text of Russian Folklore Ethnolinguistics:\n Key Issues for a Non-Russian Reader");
        p4.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p4.setStrand("Language, Dialect,\n Sociolect, Genre");
        p4.setId("18");
        //p5
        p5.setOrganizer("Viacheslav, Rudnev");
        p5.setTitle("Using Folk Constructions/Phrases in Mass Media Language");
        p5.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p5.setStrand("Language Contact and\n Change");
        p5.setId("23");
        OrganizersList.add(p1);
        OrganizersList.add(p2);
        OrganizersList.add(p3);
        OrganizersList.add(p4);
        OrganizersList.add(p5);

        addRows();
    }
    void ComelaPresenters(){
        addPresentersHeaders();
        removeRows();
        presentersList = new ArrayList<ColloquiaPresenterModel>();
        ColloquiaPresenterModel p1 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p2 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p3 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p4 = new ColloquiaPresenterModel();
        ColloquiaPresenterModel p5 = new ColloquiaPresenterModel();

        //p1
        p1.setPresenter("Bogdan Oprea, Helga-\nIuliana; Grigore, Andreea-\nVictoria");
        p1.setOrganizer("Bogdan Oprea, Helga-\nIuliana");
        p1.setTitle("Emergent Linguistic Anthropologies of the\n Mediterranean. New boundaries");
        p1.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p1.setStrand("Language in Real and Virtue \nSpaces");
        p1.setId("52");
        //p2
        p2.setPresenter("Candelario, Benji");
        p2.setOrganizer("Candelario, Benji");
        p2.setTitle("Emergent Linguistic Anthropologies of the\n Mediterranean. New boundaries");
        p2.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p2.setStrand("Anthropological Linguistics");
        p2.setId("95");
        //p3
        p3.setPresenter("Candelario, Benji");
        p3.setOrganizer("Candelario, Benji");
        p3.setTitle("The Mediterranean LGBT: Transgressing \nQueer boundaries");
        p3.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p3.setStrand("Anthropological Linguistics");
        p3.setId("96");
        //p4
        p4.setPresenter("Rubakova, Inna");
        p4.setOrganizer("Rubakova, Inna");
        p4.setTitle("Chastushka as a Text of Russian Folklore\n Ethnolinguistics: Key Issues for a Non-\nRussian Reader");
        p4.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p4.setStrand("Language, Dialect,\n Sociolect, Genre");
        p4.setId("18");
        //p5
        p5.setPresenter("Viacheslav, Rudnev");
        p5.setOrganizer("Viacheslav, Rudnev");
        p5.setTitle("Using Folk Constructions/Phrases in Mass\n Media Language");
        p5.setChair("Chair\n" +
                "To be updated\n" +
                "\n" +
                "Discussant\n" +
                "To be updated");
        p5.setStrand("Language Contact and \nChange");
        p5.setId("23");

        //add all presenters in list
        presentersList.add(p1);
        presentersList.add(p2);
        presentersList.add(p3);
        presentersList.add(p4);
        presentersList.add(p5);
        addRows();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnOrganizers:
                if (event_name.equals("CALA")) {
                    CalabtnOrganizers();
                }
                if (event_name.equals("Comela")) {
                    ComeleOrganizers();
                }
                break;
            case R.id.btnPresenters:
                if (event_name.equals("CALA")) {
                    CalaBtnPresenters();
                }
                if (event_name.equals("Comela")) {
                    ComelaPresenters();
                }
                break;
        }
    }
}
