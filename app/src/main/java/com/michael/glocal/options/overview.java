package com.michael.glocal.options;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.michael.glocal.R;

public class overview extends Activity {

    private SharedPreferences mprefrences;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.overview);
        setTitle("Overview");

        mprefrences = PreferenceManager.getDefaultSharedPreferences(this);
        String event_name = mprefrences.getString("Event", "default");
        if(event_name.equals("CALA")) {
            TextView data = findViewById(R.id.data_textview);
            TextView subtitle = findViewById(R.id.subtitle);
            subtitle.setText("CALAbration");
            data.setMovementMethod(new ScrollingMovementMethod());

            data.setText("    The Conference on Asian Linguistic Anthropology 2020, The second CALA (2), at the Universiti Putra Malaysia, Bintulu, Sarawak, Malaysia, symbolizes a significant leap forward for Linguistic Anthropology, from the previous highly successfully attended CALA 2019, to further problematize current perspectives and praxis in the field of Asian Languages, Linguistics, and Society.\n\n" +
                    "    Conceptualized close to a decade ago, the CALA 2020, as with the CALA 2019, responds to concerns by those within Linguistics, Anthropology, Sociolinguistics, Sociology, Cultural studies, and Linguistic Anthropology, pertinent to Asia. The CALA 2019 significantly reduced the gap between focus on Asian regions and work by Asian academics, largely contributable to issues of funding and expertise.The CALA 2020 will extend on these efforts, as it aims " +
                    "to further extend the global networks of Asian Linguistic Anthropology, connecting Asian with Western Universities and their academics. All papers and their authors will be SCOPUS indexed, and all papers will be subsequently channeled to Top Tier Journal Publication Special Issues and Monographs.\n\n" +
                    "    The CALA 2020 will increasingly opportune academics to exchange knowledge, expertise, and valuable Linguistic and Anthropological " +
                    "Data across the world, through the interpersonal and inter-institutional networks the CALA conferences build. To ground these efforts, the Conference, with Universiti Putra Malaysia at the centre, with major University Partners SOAS, The School of Oriental and African Studies, at The University of London, Nanyang Technological University in Singapore, and with major Publishing partner Taylor and Francis Global Publishers, will network a growing number of institutions globally, to support this much needed Asian yet fully global project.\n\n" +
                    "    The theme for the CALA 2020 is Asian Text, Global Context, a theme pertinent to the current state of many Asian regions and countries vis-a-vis their global analogues.\n" +
                    "    Universiti Putra Malaysia, hosting the CALA in 2020 in Bintulu, Sarawak, the Land of the Hornbills, Malaysia, constitutes one of an interchanging series of annual hosts, and in this way, the CALA global network expands to involve institutions worldwide.\n" +
                    "    We thus welcome you to the CALA 2020, the Second Annual Conference on Asian Linguistic Anthropology, and to the CALA in general.\n\n" +
                    "________________________\n\n"+
                    "Assoc. Prof. Dr. Hazlina ABDUL HALIM\n" +
                    "Chair\n" +
                    "Conference on Asian Linguistic Anthropology, The CALA 2020\n" +
                    "Head, Dept. of Foreign Languages\n" +
                    "Faculty of Modern Languages & Communication, Universiti Putra Malaysia\n");
        }
        if(event_name.equals("Comela"))
        {
            TextView data = findViewById(R.id.data_textview);
            TextView subtitle = findViewById(R.id.subtitle);
            subtitle.setText("Calibration");
            data.setMovementMethod(new ScrollingMovementMethod());

            data.setText("Hello and welcome to the COMELA 2020\n\n" +
                    "    The COMELA, The Annual Conference on Mediterranean and European Linguistic Anthropology, akin to its global siblings (The CALA, The MEALA, The SCAALA, The AFALA, The COOLA), has emerged to reinvigorate the fundamental roots and principles of Linguistic Anthropology, and to re-localize the field, thus marrying the focus on Mediterranean and European regions and work by Global academics.\n\n" +
                    "    The COMELA 2020, at The American College in Greece, September 2-5, 2020, arrives as a highly symbolic juncture in Mediterranean and European cum Global Linguistic Anthropology. The COMELA 2020, themed Bounded Languages … Unbounded, exposes the current Linguistic and Anthropological climate in Europe and the Mediterranean. Borders are constantly contested, and ethnicity and nation alike shift yet re legitimize their communities and heritages, thus reigniting a dedicated scholarship and praxis in the field of Language and Society.\n\n" +
                    "    The theme for the COMELA 2020 constitutes a theme pertinent to the current state of all Mediterranean and European regions vis-a-vis their global analogues." +
                    "Impervious boundaries throughout Europe and the Mediterranean are now as pervasive as at any time throughout history, not least through a migration effected by political and economic instability. Here, the Mediterranean and Europe have historically motivated the renegotiation of languages, where junctures form between shifting language, society, notions of heritage, and ethnolinguistic subjectivities. The term ‘bounded’ describes the spatiotemporal articulations yet alignments of populations throughout global time and space, that is, the ways in which language and ethnicity legitimize themselves so to reaffirm communities of ethnolinguistic practice as they migrate and return to their heritage locations. The term ‘unbounded’ signifies efforts by these ethnicities to renegotiate transnational privileges, through developing both heritage and global capital, an effort central to neoliberal intentions.\n\n" +
                    "    The COMELA 2020 will build and extend global networks of Mediterranean and European Linguistic Anthropology, collating work within Linguistics, Anthropology, Sociolinguistics, Sociology, Cultural studies, and Linguistic Anthropology, all pertinent to the Mediterranean and Europe, but throughout worldwide arenas. Through the Conference, global academics will juxtapose knowledge, expertise, and valuable Linguistic and Anthropological data, through the interpersonal and inter-institutional networks constructed by the COMELA. To ground these efforts, the COMELA, with The American College in Greece at the centre, networks an ever-growing number of institutions globally.\n" +
                    "With over 120 Academics prominent in the field on its Scientific Committee, over 120 institutions globally as affiliates, with major official partner Taylor and Francis Global Publishers, and a global network of partners in Linguistic Anthropology, The COMELA 2020, The Annual conference on Mediterranean and European Linguistic Anthropology, September 2-5, 2020, at The American College in Greece, in Athens, as a fully non-profit Conference on Linguistic Anthropology, brings to life the original yet progressive intentions of Linguistic Anthropology, in and for the Mediterranean and Europe.\n" +
                    "We thus welcome you to the COMELA 2020, the Annual Conference on Mediterranean and European Linguistic Anthropology in Athens, Greece, September 2-5, 2020, and to the COMELA in whole.\n\n" +
                    "________________________\n\n"+
                    "Professor Helena Maragou\n" +
                    "Chair\n" +
                    "The Conference on Mediterranean and European Linguistic Anthropology 2020, The COMELA 2020\n" +
                    "School of Liberal Arts and Sciences\n" +
                    "College of Arts & Sciences, The American College in Greece\n");

        }
    }
}
