package com.michael.glocal.options;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.michael.glocal.*;

import androidx.annotation.Nullable;

public class speakerr extends Activity {

    private SharedPreferences mprefrences;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.speakers);
        setTitle("Speakers");

        mprefrences = PreferenceManager.getDefaultSharedPreferences(this);
        String event_name = mprefrences.getString("Event", "default");
        //Toast.makeText(speakerr.this, "Event: " + event_name, Toast.LENGTH_SHORT).show();
        if(event_name.equals("CALA"))
        {
            TextView data = findViewById(R.id.speaker1);
            data.setMovementMethod(new ScrollingMovementMethod());
            data.setText("1- Li Wei\n" +
                    "(University College London)\n\n" +
                    "Li Wei became Professor of Linguistics at Newcastle University in 1998 and was Director of the Centre for Research in Linguistics between 1999 and 2002 and head of the School of Education, Communication and Language Sciences between 2002 and the end of 2006. He joined Birkbeck, University of London in January 2007 as Professor of linguistics. In January 2015, he took up the Chair of Linguistics at University College London’s Institute of Education."+
                    "\n\n2- Asmah Haji Omar\n" +
                    "(University of Malaya)\n\n" +
                    "Asmah Haji Omar is Professor Emeritus of the University of Malaya, where she has been Professor (Chair) of Malay Linguistics, Founder Dean of the Faculty of Languages and Linguistics, and Deputy Vice-Chancellor. Instrumental in the implememtation of the National Language Policy at the UM, Asmah was responsible for the changeover to Malay as official and main language of the university. She has worked on Malay communities, and on Malaysian aborigines and indigenous peoples of the Sabah and Sarawak. Asmah has also worked on the linguistics of the indigenous peoples of Malaysia, as well as the spread of Malay. Apart from books and articles, Asmah has produced two encyclopedias.\n" +
                    "\n" +
                    "Keynote Description\n" +
                    "\n" +
                    "Language and Speaker Movement in the Malay World: as told in folk narratives\n" +
                    "\n" +
                    "Research into various aspects of speech systems in indigenous communities in the Malay world provides a picture of a close relationship between them, one that shows that they originate from a single parent. The determination of this relationship belongs to historical-comparative linguistics with theories and methodologies useful in deriving information of the historical development of a family of languages, which includes the geographical direction of language development, hence movement, to a new place of distribution.\n" +
                    "\n" +
                    "Language diffusion in the old days could not take place without the movement of the speakers who at the end of their migration opened up new settlements, i.e. new geolinguistic regions, giving identity to their group. Although language can store what has taken place in history, the single words and phrases used in isolation of context that have become data of historical linguistics can only relate part of the story of a particular language, i.e. its phylogenetic history. But the history of a language is much more than that; there are social, cultural, political, and geographical aspects of their being. Information of this nature of communities of the past are only available in folk narratives. This paper will examine those narratives in terms of the settlement and migration of the people, to see whether information therein agrees with linguistic data analyses.\n\n"+

                    "3- Susan Needham\n" +
                    "(California State University)\n\n" +

                    " Susan Needham is Professor of Anthropology at California State University Dominguez Hills in Carson, California. She has conducted sociolinguistic and ethnographic research in the Cambodian community of Long Beach, California since 1988 with a particular focus on language ideologies related to knowledge systems and knowledge display, as well as the development, maintenance, and reinterpretation of situated social identities (age, gender, and ethnicity) associated with language and learning in Cambodian arts and ritual practices. Dr. Needham is co-founder and director of the Cambodian Community History and Archive Project (camchap.org) located at the Historical Society of Long Beach.\n" +
                    "\n" +
                    "Keynote Description\n" +
                    "\n" +
                    "Khmer Brahmanist Ritual Practices: Social and Linguistic Mediation of (Super)natural Encounters\n" +
                    "\n" +
                    "As an exercise in semiotic connections, this session will feature the enactment of a Khmer Brahmanist ritual called, sompeah kruu (honoring the teacher/guardian spirits). Through the sompeah kruu ceremony, individuals honor and show gratitude for the knowledge and protection imparted to them from their teachers, their guardian spirit, the Buddha, and Hindu and Khmer deities. At the center of the ritual is a multi-stepped altar, called asanah (seat of power) upon which are placed familiar ritual offerings such as incense, candles, water, flowers, special foods, and an assortment of objects considered to be spiritually powerful. However, the defining sacred objects for the Khmer asanah are the multi-level cylindrical objects known as baaysei, which provide a place for the spirits to inhabit.\n" +
                    "\n" +
                    "Until the 1970s asanah were a central part of a wide range of Cambodian occupations and trades, including carpenters, wrestlers, midwives, spiritual healers, and artists, such as dancers and musicians. The little research that has been done on these rituals has focused on their role in healing practices in modern Cambodia, making it appear the rituals are limited to this one domain. However, the practice is quite widespread and a variety of individuals in Cambodia and throughout the diaspora have altars in their homes where they regularly conduct less formal variations of the sompeah kruu ceremony for local spirits as well as ancestor and guardian spirits.\n" +
                    "\n" +
                    "The presentation will begin with a brief discussion of the performative and semiotic aspects of the ritual that connect several areas of experience including the past and present, the living and the non-living, the “natural” and the “supernatural.” The ritual will be officiated by Mr. Sakphan Keam, a Khmer-American ritual specialist from Long Beach, California."+
                    "\n\n4- Hans Henrich Hock\n" +
                    "(University of Illinois Urbana Champaign)\n\n" +

                    "Hans Henrich Hock is Professor of Linguistics and Sanskrit at the University of Illinois at Urbana-Champaign. He holds a Ph.D. in linguistics from Yale University. His research interests include general historical and comparative linguistics, as well as the linguistics of Sanskrit. He currently teaches general historical linguistics, Indo-European linguistics, Sanskrit, diachronic sociolinguistics, pidgins and creoles, and the history of linguistics.\n" +
                    "\n" +
                    "Keynote Description\n" +
                    "\n" +
                    "The Steppes, Anatolia, India? Migration, archaeology, genomes, and Indo-European\n" +
                    "\n" +
                    "Around 1000 BC, Indo-European languages were distributed over a wide area, ranging from Xinjiang and India to Ireland and Anatolia. This widespread distribution has generally been considered to result from migrations. Early hypotheses, which placed the original home in South or Central Asia, were largely based on preconceived notions, such as the idea that Sanskrit was the ancestor of the other Indo-European languages. From the middle of the 19th century racial considerations led to a shift farther west, which culminated in the “Nordic” homeland proposed by people like Penka, Kossinna, and Childe.\n" +
                    "\n" +
                    "The association of the “Nordic” homeland hypothesis with Nazi ideology was a factor in anthropologists’ questioning migration accounts in general, and some archaeologists have proposed that languages can spread through stimulus difficusion, just like various artifacts. However, migrations are well attested in history (e.g. the Roman Empire) and must be accepted for all stages of human experience. Moreover, I argue, languages are not just words but are complex structural systems that cannot “move” without people also moving.\n" +
                    "\n" +
                    "Against this background I show that linguistic palaeontology clearly argues for a 4th-millennium BC Indo-European homeland in the Eurasian Steppes, at or near the area where horses were domesticated and where two-wheeled horse-drawn chariots and horse burials originated. This requires the assumption of centrifugal migrations from the Steppes to the areas in which the languages are first found. I demonstrate that the “Steppe Hypothesis” is superior to its most recent rival, the “Anatolian Hypothesis, according to which the Indo-European homeland was in 7th-millennium BC Anatolia. Moreover, I argue that the evidence of linguistic palaeontology raises serious questions about Indian Nationalist attempts to locate the homeland in South Asia.\n" +
                    "\n" +
                    "In the final part of the presentation I exmine the most recent genomic findings, especially research based on Ancient DNA. The findings are compatible with the “Steppe Hypothesis”, as regards both Europe and India/South Asia, but questions remain. Perhaps the most important among these is whether the postulated migrations out of the Eurasian Steppes were, as assumed by genomic researchers, limited to speakers of Indo-European languages or whether they may have involved speakers of other languages, including the ancestor of Basque.\n" +
                    "\n" +
                    "I conclude that the evidence for the “Steppe Hypothesis” provides cold comfort to “Nordic supremacists” as well as to Indian Nationalists – “A theory that does not satisfy various nationalist or ideological movements can’t be all bad …”\n\n"+

                    "5- Nathan Hill\n" +
                    "(SOAS University of London)\n\n" +
                    "Nathan Hill was educated at the Catlin Gabel School and Harvard University. He has also studied for shorter periods in France, Nepal, Tibet and Japan. He came to SOAS in 2008 after teaching at Harvard University and Universität Tübingen. At SOAS he teaches courses in historical linguistics and well as Tibetan language and history. He convenes Tibetan Studies at SOAS. Hill’s work involves work on Tibetan literature and history in the Department of China & Inner Asia, as well as on historical, descriptive and corpus linguistics, in particular with reference to Tibetan or other Tibeto-Burman/Sino-Tibetan languages, in the Department of Linguistics.\n" +
                    "\n" +
                    "Keynote Description\n" +
                    "\n" +
                    "Technology and Language Documentation in the Global Era\n" +
                    "\n" +
                    "Many of the world’s languages are unstudied and on the verge of disappearance. A few linguists strive to address this problem, but compared to Indo-European languages, there is much more work to do, with only a fraction of the manpower and under immense time pressure. Over the last two decades the field of ‘documentary linguistics’ has attempted to address this situation, but with successes that can be said to be partial at best.\n" +
                    "\n" +
                    "After considering some of the obstacles to language documentation, this talk takes a fresh look at the technical side of linguistic fieldwork. It does this from the perspective of recent technological innovations, and examines the workflows and approaches to data management in particular. I conduct two case studies, the methods through which, may hold great promise in helping communities that speak understudied languages, namely autocompletion software for mobile phones and the automatic phonetic transcription of spoken languages using neural networks." );
        }
        if(event_name.equals("Comela"))
        {
            TextView SpeakerOneData = findViewById(R.id.speaker1);
            TextView SpeakerTwoData = findViewById(R.id.speaker2);
            TextView SpeakerThreeData = findViewById(R.id.speaker3);
            ImageView speakerOneImage = (ImageView) findViewById(R.id.Speaker1Image);
            ImageView speakerTwoImage = (ImageView) findViewById(R.id.Speaker2Image);
            ImageView speakerThreeImage = (ImageView) findViewById(R.id.Speaker3Image);
            SpeakerOneData.setText("Jan Blommaert\n\n" + "Department of Culture Studies\n"+"Tilburg University"+"The Netherlands\n"+
                    "_________________________\n" +
                    "Jan Blommaert is Professor of Language, Culture and Globalization, and Director of the Babylon Center at Tilburg University, the Netherlands. He is of the world’s most prominent Sociolinguists and Linguistic Anthropologists, and has contributed substantially to sociolinguistic globalization theory, focusing on historical as well as contemporary patterns of language and literacy, and on lasting and new forms of inequality emerging from globalization processes."+
                    "\n_________________________");
            speakerOneImage.setImageResource(R.drawable.jan);

            SpeakerTwoData.setText("Alexandra Georgakopoulou\n\n" + "King’s College London" + "London, England\n"+
                    "_________________________\n" +
                    "Alexandra Georgakopoulou is a narrative and discourse analyst with a longstanding interest in how people present themselves and relate to others in significant socialization contexts (family, friendship groups, school, leisure sites, social media platforms). Drawing on sociocultural linguistic and interactional perspectives, she has researched the role of everyday life stories (both face-to-face and on new/social media) in the (re)formation of social relations. She has specifically researched, with ethnographic methods, female adolescent peer-groups, in Greece and in London. "+
                    "\n_________________________\n" );
            speakerTwoImage.setImageResource(R.drawable.alexandra);

            SpeakerThreeData.setText("Dimitris Dalakoglou\n\n" + "Social Anthropology Department" + "Vrije University"+ "Amsterdam, The Netherlands"+
                    "\n_________________________\n" +
                    "Dimitris Dalakoglou is Professor at Vrije University Amsterdam, where he holds the Chair of Social Anthropology. Since 2004 he is developing an anthropology of material infrastructures which most recently culminated to his current project infra-demos that studies democracy and infrastructures in Greece. He is director of the Lab on Mobility, Infrastructures, Sustainability and Commons (MISC Lab)."+
                    "\n_________________________\n" );
            speakerThreeImage.setImageResource(R.drawable.dimitris);
        }
    }
}
