package com.michael.glocal.options;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.michael.glocal.R;
import com.michael.glocal.models.presenter;

import java.util.ArrayList;

public class Posters extends AppCompatActivity implements View.OnClickListener {
    private SharedPreferences mprefrences;
    String event_name;
    TableLayout tableLayout;
    ArrayList<presenter> presenterList;
    Button btnD,btnL,btnN,btnO,btnS,btnZ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posters);
        getSupportActionBar().hide();
        initViews();
        if(event_name.equals("CALA")) {

            CalaButtonD();
        }
        if(event_name.equals("Comela")) {
        }
    }

    void initViews() {
        tableLayout = (TableLayout) findViewById(R.id.table);
        btnD = (Button) findViewById(R.id.btnD);
        btnL = (Button) findViewById(R.id.btnL);
        btnN = (Button) findViewById(R.id.btnN);
        btnO = (Button) findViewById(R.id.btnO);
        btnS = (Button) findViewById(R.id.btnS);
        btnZ = (Button) findViewById(R.id.btnZ);
        btnD.setOnClickListener(this);
        btnL.setOnClickListener(this);
        btnN.setOnClickListener(this);
        btnO.setOnClickListener(this);
        btnS.setOnClickListener(this);
        btnZ.setOnClickListener(this);
        addHeaders();

        mprefrences = PreferenceManager.getDefaultSharedPreferences(this);
        event_name = mprefrences.getString("Event", "default");
    }
    public void addHeaders(){
        TableLayout t1 = (TableLayout) findViewById(R.id.table);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());

        tr.addView(getTextView(0, "Presenter", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "Title", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "Strand", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "ID    ", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "Room   ", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));
        tr.addView(getTextView(0, "Time   ", Color.BLACK, Typeface.BOLD, R.drawable.cell_shape));

        t1.addView(tr, getTblLayoutParams());
    }
    private TextView getTextView(int id, String Title, int color, int TypeFace, int bg){
        TextView tv = new TextView(this);
        tv.setId(id);
        tv.setText(Title.toUpperCase());
        tv.setTextColor(color);
        tv.setPadding(40,40,20,40);
        tv.setTypeface(Typeface.DEFAULT, TypeFace);
        tv.setBackgroundColor(bg);
        tv.setBackgroundResource(bg);
        tv.setLayoutParams(getLayoutParams());
        return tv;
    }
    private TextView getRowsTextView(int id, String Title, int color, int bg){
        TextView tv = new TextView(this);
        tv.setId(id);
        tv.setTextSize(12);
        tv.setText(Title);
        tv.setTextColor(color);
        tv.setPadding(40,40,20,40);
        tv.setTypeface(Typeface.DEFAULT);
        tv.setBackgroundColor(bg);
        tv.setBackgroundResource(bg);
        tv.setLayoutParams(getLayoutParams());
        return tv;
    }
    private TableRow.LayoutParams getLayoutParams(){
        TableRow.LayoutParams params = new TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(2,1,2,1);
        params.weight = 1;
        return  params;
    }
    private TableLayout.LayoutParams getTblLayoutParams() {
        return new TableLayout.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT
        );

    }
    void addRows(){
        // Collections.reverse(presenterList);
        for(int i=0; i < presenterList.size() ; i ++){
            TableRow tr= new TableRow(Posters.this);
            tr.setLayoutParams(getLayoutParams());

            tr.addView(getRowsTextView(0, presenterList.get(i).getPresenterName(), Color.BLACK, R.drawable.cell_shape));
            tr.addView(getRowsTextView(0, presenterList.get(i).getTitle(), Color.BLACK , R.drawable.cell_shape));
            tr.addView(getRowsTextView(0, presenterList.get(i).getStrands(), Color.BLACK, R.drawable.cell_shape));
            tr.addView(getRowsTextView(0, presenterList.get(i).getID(), Color.BLACK,R.drawable.cell_shape));
            tr.addView(getRowsTextView(0, presenterList.get(i).getRoom(), Color.BLACK,  R.drawable.cell_shape));
            tr.addView(getRowsTextView(0, presenterList.get(i).getTime(), Color.BLACK, R.drawable.cell_shape));

            tableLayout.addView(tr, getTblLayoutParams());
        }
    }
    void removeRows(){
        if(tableLayout.getChildCount() != 0) {
            while (tableLayout.getChildCount() > 1) {
                TableRow row = (TableRow) tableLayout.getChildAt(1);
                tableLayout.removeView(row);
            }
        }

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnD:
                if (event_name.equals("CALA")) {
                    CalaButtonD();
                }
                if (event_name.equals("Comela")) {
                }
                break;

            case R.id.btnL:
                if (event_name.equals("CALA")) {
                    CalaButtonL();
                }
                if (event_name.equals("Comela")) {
                }
                break;
            case R.id.btnN:
                if (event_name.equals("CALA")) {
                    CalaButtonN();
                }
                if (event_name.equals("Comela")) {
                }
                break;
            case R.id.btnO:
                if (event_name.equals("CALA")) {
                    CalaButtonO();
                }
                if (event_name.equals("Comela")) {
                }
                break;
            case R.id.btnS:
                if (event_name.equals("CALA")) {
                    CalaButtonS();
                }
                if (event_name.equals("Comela")) {
                }
                break;
            case R.id.btnZ:
                if (event_name.equals("CALA")) {
                    CalaButtonZ();
                }
                if (event_name.equals("Comela")) {
                }
                break;
        }
    }

    private void CalaButtonD() {
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenters: Do, Hien;\n" +
                " Nguyen, Thi Thanh Thuy\n" +
                "Authors: Do, Hien; Nguyen,\n" +
                " Thi Thanh Thuy");
        p1.setTitle("Socialization through Sign \nLanguage for Deaf Children in\n Early School Years under the\n Light of Communication Theory");
        p1.setStrands("Language\n Dialect\n Sociolect \nGenre");
        p1.setID("134");
        p1.setRoom(".");
        p1.setTime(".");

        //add all presenters in list
        presenterList.add(p1);
        addRows();
    }

    private void CalaButtonL() {
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenters: Loh, Chun Han;\n Seng, Hui Zanne\n" +
                "Authors: Loh, Chun Han;\n Seng, Hui Zanne; \nChan, Mei Yuit");
        p1.setTitle("Understanding The Penang \nChinese Naming Practices In \nMulticultural Malaysia");
        p1.setStrands("Anthropological\n Linguistics");
        p1.setID("92");
        p1.setRoom(".");
        p1.setTime(".");

        //add all presenters in list
        presenterList.add(p1);
        addRows();
    }
    private void CalaButtonN() {
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        presenter p2 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenter: Nguyen, Hoa\n" +
                "Author: Nguyen, Hoa");
        p1.setTitle("Alcohol in Tay ethnic culture\n in the Northeast of Vietnam");
        p1.setStrands("Language\n Community \nEthnicity");
        p1.setID("142");
        p1.setRoom(".");
        p1.setTime(".");

        p2.setPresenterName("Presenters: Nguyen, \nThi Thanh Thuy;\n Do, Hien\n" +
                "Authors: Do, Hien; \nNguyen, Thi Thanh Thuy");
        p2.setTitle("Socialization through Sign \nLanguage for Deaf Children in\n Early School Years under the\n Light of Communication Theory");
        p2.setStrands("Language \nDialect \nSociolect\n Genre");
        p2.setID("134");
        p2.setRoom(".");
        p2.setTime(".");
        //add all presenters in list
        presenterList.add(p1);
        presenterList.add(p2);
        addRows();
    }
    private void CalaButtonO() {
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenter: On, Thi My Linh\n" +
                "Author: On, Thi My Linh");
        p1.setTitle("The Influence of The Brothers\n Grimm on Nguyen Dong Chi’s \nViewpoint and Method of \nCollecting Folk Narratives");
        p1.setStrands("Language, \nDialect,\n Sociolect, \nGenre");
        p1.setID("174");
        p1.setRoom(".");
        p1.setTime(".");

        //add all presenters in list
        presenterList.add(p1);
        addRows();
    }
    private void CalaButtonS() {
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenters: Seng,\n Hui Zanne; Loh, \nChun Han\n" +
                "Authors: Loh, Chun Han;\n Seng, Hui Zanne; \nChan, Mei Yuit");
        p1.setTitle("Understanding The Penang \nChinese Naming Practices In\n Multicultural Malaysia");
        p1.setStrands("Anthropological\n Linguistics\n");
        p1.setID("92");
        p1.setRoom(".");
        p1.setTime(".");

        //add all presenters in list
        presenterList.add(p1);
        addRows();
    }
    private void CalaButtonZ() {
        removeRows();
        presenterList = new ArrayList<presenter>();
        //make one object
        presenter p1 = new presenter();
        //set all attributes
        p1.setPresenterName("Presenter: Zahria, \nFarlydia Farikhin\n" +
                "Author: Zahria, \nFarlydia Farikhin");
        p1.setTitle("The Study of Intertextuality \non Novel Entitled a very Yuppy\n Wedding and Divortiare by \nIka Natassa");
        p1.setStrands("Textualization \nContextualization\n Entextualization");
        p1.setID("164");
        p1.setRoom(".");
        p1.setTime(".");

        //add all presenters in list
        presenterList.add(p1);
        addRows();
    }
}
