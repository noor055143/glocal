package com.michael.glocal.options;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.michael.glocal.R;

public class chronology extends Activity {
    private SharedPreferences mprefrences;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cronology);
        setTitle("Chronology");

        mprefrences = PreferenceManager.getDefaultSharedPreferences(this);
        String event_name = mprefrences.getString("Event", "default");
        if (event_name.equals("CALA")) {
            TextView data = findViewById(R.id.data_textview);
            TextView subtitle = findViewById(R.id.subtitle);
            subtitle.setText("");
            subtitle.setVisibility(View.GONE);
            data.setMovementMethod(new ScrollingMovementMethod());

            data.setText("Abstract and poster proposal submission – Second Call\n" +
                    "Opens: May 23, 2019\n" +
                    "Closes: August 23, 2019\n" +
                    "\n" +
                    "Notification of acceptance\n" +
                    "No later than June 10, 2019 (for first call)\n" +
                    "No later than September 1, 2019 (for second call)\n" +
                    "\n" +
                    "Registration\n" +
                    "Early bird registration\n" +
                    "\n" +
                    "Opens: March 10, 2019\n" +
                    "Closes: June 14, 2019\n" +
                    "\n" +
                    "Normal bird registration\n" +
                    "\n" +
                    "Opens: June 15, 2019\n" +
                    "Closes: September 25, 2019\n" +
                    "\n" +
                    "Presenters will need to have registered for The CALA by no later than September 25 2019, midnight (UTC Time), to guarantee a place in the program. Registration will remain open after this date, but the conference organizers can not guarantee placement in the conference.\n" +
                    "\n" +
                    "Late bird registration\n" +
                    "\n" +
                    "Opens: September 26, 2019\n" +
                    "Closes: February 8, 2020\n" +
                    "\n" +
                    "Dates\n" +
                    "Day 1: Wednesday February 5, 2020\n" +
                    "Day 2: Thursday February 6, 2020\n" +
                    "Day 3: Friday February 7, 2020\n" +
                    "Day 4: Saturday February 8, 2020 – Full day of optional cultural tour (separate cost)\n" +
                    "\n");
        }
        if(event_name.equals("Comela"))
        {
            TextView data = findViewById(R.id.data_textview);
            TextView subtitle = findViewById(R.id.subtitle);
            subtitle.setText("");
            subtitle.setVisibility(View.GONE);
            data.setMovementMethod(new ScrollingMovementMethod());

            data.setText("Second and Final Abstract and poster proposal submission\n" +
                    "Opens: December 1, 2019 at midnight\n" +
                    "Closes: April 1, 2020 at midnight\n" +
                    "\n" +
                    "Notification of acceptance\n" +
                    "No later than April 20, 2020 at midnight\n\n" +
                    "Registration\n" +
                    "Early bird registration\n" +
                    "\n" +
                    "Opens: October 30, 2019 at midnight\n" +
                    "Closes: January 21, 2020 at midnight\n" +
                    "\n" +
                    "Normal bird registration\n" +
                    "\n" +
                    "Opens: January 22, 2020 at midnight\n" +
                    "Closes: April 25, 2020 at midnight\n" +
                    "\n" +
                    "Presenters will need to have registered for The COMELA by no later than April 25, 2020, midnight, to guarantee a place in the program. Registration will remain open after this date, but the conference organizers can not guarantee placement in the conference.\n" +
                    "\n" +
                    "Late bird registration\n" +
                    "\n" +
                    "Opens: April 26, 2020 at midnight\n" +
                    "Closes: September 5, 2020 (Conference end)\n" +
                    "\n" +
                    "Dates\n" +
                    "Day 1: Wednesday September 2, 2020\n" +
                    "Day 2: Thursday September 3, 2020\n" +
                    "Day 3: Friday September 4, 2020\n" +
                    "Day 4: Saturday September 5, 2020 – Full day of optional cultural tour (separate cost)\n" +
                    "\n");
        }
    }
}
