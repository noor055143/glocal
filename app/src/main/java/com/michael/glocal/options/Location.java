package com.michael.glocal.options;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.michael.glocal.ComelaLocation;
import com.michael.glocal.MapsActivity;
import com.michael.glocal.R;

public class Location extends Activity{
    TextView Location;
    TextView LocationInfo;
    private SharedPreferences mprefrences;
    String event_name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        setTitle("Location");
        mprefrences = PreferenceManager.getDefaultSharedPreferences(this);
        event_name = mprefrences.getString("Event", "default");
        Location = (TextView) findViewById(R.id.locationData);
        LocationInfo = (TextView) findViewById(R.id.locationInfo);
        if(event_name.equals("CALA")) {
            Location.setText("Conference Venue\n" + "_____________________________________\n" + "Parkcity Everly Hotel");
            LocationInfo.setText("Bintulu, Sarawak" + "\nMalaysia");
        } else if(event_name.equals("Comela")) {
            Location.setText("American College of Greece\n" + "_____________________________________\n" + "6 Gravias Str.");
            LocationInfo.setText("153-42 Aghia Paraskevi" + "\nAthens – GREECE");
        }

    }
    public void ShowMaps(View v){
        if(event_name.equals("CALA")) {
            Intent location = new Intent(Location.this, MapsActivity.class);
            startActivity(location);
        } else if(event_name.equals("Comela")) {
            Intent location = new Intent(Location.this, ComelaLocation.class);
            startActivity(location);
        }
    }


}
