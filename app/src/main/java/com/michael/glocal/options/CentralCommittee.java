package com.michael.glocal.options;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.widget.TextView;

import com.michael.glocal.R;

public class CentralCommittee extends AppCompatActivity {
    private SharedPreferences mprefrences;
    TextView SubTitle1,SubTitle2,SubTitle3,SubTitle4,SubTitle5,SubTitle6,SubTitle7,SubTitle8;
    TextView SubTitle1Info,SubTitle3Info,SubTitle4Info,SubTitle5Info,SubTitle6Info,SubTitle7Info,SubTitle8Info;
    private String h1,h2,h3,h4,h5,h6,h7,h8;
    private String p1,p2,p3,p4,p5,p6,p7,p8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_central_committee);
        getSupportActionBar().hide();
        mprefrences = PreferenceManager.getDefaultSharedPreferences(this);
        String event_name = mprefrences.getString("Event", "default");

        SubTitle1 = (TextView) findViewById(R.id.subtitle1);
        SubTitle2 = (TextView) findViewById(R.id.subtitle2);
        SubTitle3 = (TextView) findViewById(R.id.subtitle3);
        SubTitle4 = (TextView) findViewById(R.id.subtitle4);
        SubTitle5 = (TextView) findViewById(R.id.subtitle5);
        SubTitle6 = (TextView) findViewById(R.id.subtitle6);
        SubTitle7 = (TextView) findViewById(R.id.subtitle7);
        SubTitle8 = (TextView) findViewById(R.id.subtitle8);
        SubTitle1Info = (TextView) findViewById(R.id.subtitle1Info);
        SubTitle3Info = (TextView) findViewById(R.id.subtitle3Info);
        SubTitle4Info = (TextView) findViewById(R.id.subtitle4Info);
        SubTitle5Info = (TextView) findViewById(R.id.subtitle5Info);
        SubTitle6Info = (TextView) findViewById(R.id.subtitle6Info);
        SubTitle7Info = (TextView) findViewById(R.id.subtitle7Info);
        SubTitle8Info = (TextView) findViewById(R.id.subtitle8Info);
        if(event_name.equals("CALA")) {
            h1= "Central";
            h2= "Chair";
            h3= "Vice Chair";
            h4= "Committee Members";
            h5= "Treasurer";
            h6= "Communications";
            h7= "Legal";

            p1= "The responsibilities of the CALA 2020 – Coordinating committee include to ensure that all procedural, ethical, interpersonal, legal, and academic requirements are executed and met. Within these requirements, the committee observes the central importance of equality and opportunity for all.";
            p2 = "\n\nThe Coordinating committee is also responsible for facilitating selection of, and the organizing of, all committees, and their members.";

            SubTitle1.setText(h1 + "\n");
            SubTitle1Info.setText(p1 + p2);

            SubTitle2.setText(h2 +
                    "Assoc. Prof. Dr. Hazlina ABDUL HALIM\n" +
                    "Head\n" +
                    "Dept. of Foreign Languages\n" +
                    "Faculty of Modern Languages & Communication\n" +
                    "Universiti Putra Malaysia\n" +
                    "hazlina_ah@upm.edu.my");

            SubTitle3.setText(h3 + "\n");
            SubTitle3Info.setText("Maulana Magiman, PhD\n" +
                    "Dept. of Foreign Languages\n" +
                    "Faculty of Modern Languages & Communication\n" +
                    "Universiti Putra Malaysia\n" +
                    "mdmaulana@upm.edu.my");
            SubTitle4.setText(h4 + "\n");
            SubTitle4Info.setText("Muhd Zulkifli bin Ismail, PhD\n" +
                    "Dept. of Foreign Languages\n" +
                    "Faculty of Modern Languages & Communication\n" +
                    "Universiti Putra Malaysia\n" +
                    "zulismail@upm.edu.my\n\n" +
                    "Ang Lay Hoon, PhD\n" +
                    "Dept. of Foreign Languages\n" +
                    "Faculty of Modern Languages & Communication\n" +
                    "Universiti Putra Malaysia\n" +
                    "hlang@upm.edu.my\n\n" +
                    "Elyza Saharudin, PhD\n" +
                    "Dept. of Foreign Languages\n" +
                    "Faculty of Modern Languages & Communication\n" +
                    "Universiti Putra Malaysia\n" +
                    "nses@upm.edu.my\n\n");
            SubTitle5.setText(h5 + "\n");
            SubTitle5Info.setText("Pabiyah Hajimaming, PhD\n" +
                    "Dept. of Foreign Languages\n" +
                    "Faculty of Modern Languages & Communication\n" +
                    "Universiti Putra Malaysia\n" +
                    "pabiyah@upm.edu.my\n");
            SubTitle6.setText(h6 + "\n");
            SubTitle6Info.setText("Johnny Dawson\n" +
                    "Head of Communications\n" +
                    "info@cala.asia");
            SubTitle7.setText(h7 + "\n");
            SubTitle7Info.setText("Colin Cavendish Jones, PhD\n" +
                    "Counsel and Head of Legal\n" +
                    "ccj@cala.asia");
            SubTitle8Info.setText("");

        }
        if(event_name.equals("Comela"))
        {
            h1= "Purpose";
            h2= "Committee";
            h3= "Chair";
            h4= "Vice Chair";
            h5= "Steering Committee";
            h6= "Communications";
            h7= "Counsel and Head of Legal";
            h8= "Funding";

            p1= "The responsibilities of the COMELA Conference 2020 – Coordinating committee include to ensure that all procedural, ethical, interpersonal, legal, and academic requirements for the COMELA Conference 2020 are executed and met. Within these requirements, the committee observes the central importance of equality and opportunity for all.";
            p2= "The Coordinating committee is also responsible for facilitating selection of, and the organizing of, all committees, and their members.";
            p3= "Professor Helena Maragou\n" +
                    "Dean\n" +
                    "School of Liberal Arts and Sciences\n" +
                    "The American College of Greece\n" +
                    "(maragou@acg.edu)";
            p4= "Professor Chryssa Zachou\n" +
                    "Department of Sociology\n" +
                    "School of Liberal Arts and Sciences\n" +
                    "The American College of Greece\n" +
                    "(czachou@acg.edu)";
            p5= "Professor Ilay Romain Ors\n" +
                    "Department of Anthropology\n" +
                    "School of Liberal Arts and Sciences\n" +
                    "The American College of Greece\n" +
                    "\n" +
                    "Professor Spyros Gangas\n" +
                    "Department of Sociology\n" +
                    "School of Liberal Arts and Sciences\n" +
                    "The American College of Greece\n" +
                    "(sgangas@acg.edu)\n" +
                    "\n" +
                    "Professor Marina Kolokonte\n" +
                    "Department of Linguistics\n" +
                    "School of Liberal Arts and Sciences\n" +
                    "The American College of Greece\n" +
                    "(mkolokonte@acg.edu)\n" +
                    "\n" +
                    "Professor Stergios Pardalis\n" +
                    "Department of Anthropology\n" +
                    "School of Liberal Arts and Sciences\n" +
                    "The American College of Greece";
            p6= "Anastasia Tsantes\n" +
                    "Communication coordinator\n" +
                    "(comela@acg.edu)\n" +
                    "\n" +
                    "Nhan Huynh\n" +
                    "Head of Communications\n" +
                    "(comela@acg.edu)";
            p7= "Professor Colin Cavendish Jones\n" +
                    "(ccj@comela.me)";
            p8= "Cheryl Omnes\n" +
                    "Funding Coordinator";


            SpannableString paragraph1 = new SpannableString(p3);

            SpannableString paragraph2 = new SpannableString(p4);
            SpannableString paragraph3 = new SpannableString(p5);
            SpannableString paragraph4 = new SpannableString(p6);
            SpannableString paragraph5 = new SpannableString(p7);
            SpannableString paragraph6 = new SpannableString(p8);

            ForegroundColorSpan m1 = new ForegroundColorSpan(Color.rgb(132,34,34));
            ForegroundColorSpan m2 = new ForegroundColorSpan(Color.rgb(132,34,35));
            ForegroundColorSpan m3 = new ForegroundColorSpan(Color.rgb(132,34,36));
            ForegroundColorSpan m4 = new ForegroundColorSpan(Color.rgb(132,34,37));
            ForegroundColorSpan m5 = new ForegroundColorSpan(Color.rgb(132,34,38));

            paragraph1.setSpan(m1,0, 25, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            paragraph1.setSpan(m2,96, 114, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

            paragraph2.setSpan(m1,0, 24, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            paragraph2.setSpan(m2,115, 133, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

            paragraph3.setSpan(m1,0, 25, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            paragraph3.setSpan(m2,120, 146, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            paragraph3.setSpan(m3,235, 284, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            paragraph3.setSpan(m5,374, 424, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

            paragraph4.setSpan(m1,0, 73, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            paragraph4.setSpan(m2,96, 112, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);


            paragraph5.setSpan(m1,0,47, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

            paragraph6.setSpan(m1,0,12, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

            SubTitle1.setText(h1 + "\n");
            SubTitle2.setText(h2);
            SubTitle3.setText(h3 + "\n");
            SubTitle4.setText(h4 + "\n");
            SubTitle5.setText(h5 + "\n");
            SubTitle6.setText(h6 + "\n");
            SubTitle7.setText(h7 + "\n");
            SubTitle8.setText(h8 + "\n");

            SubTitle1Info.setText(p1 + "\n" + p2 + "\n");
            SubTitle3Info.setText(paragraph1, TextView.BufferType.SPANNABLE);
            SubTitle4Info.setText(paragraph2, TextView.BufferType.SPANNABLE );
            SubTitle5Info.setText(paragraph3, TextView.BufferType.SPANNABLE);
            SubTitle6Info.setText(paragraph4 , TextView.BufferType.SPANNABLE);
            SubTitle7Info.setText(paragraph5, TextView.BufferType.SPANNABLE );
            SubTitle8Info.setText(paragraph6 , TextView.BufferType.SPANNABLE);
        }
    }
}
