package com.michael.glocal.options;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import com.michael.glocal.R;

public class CitationAndPublications extends Activity {

    private SharedPreferences mprefrences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_citation_and_publications);
        setTitle("Citation and Publication");

        mprefrences = PreferenceManager.getDefaultSharedPreferences(this);
        String event_name = mprefrences.getString("Event", "default");
        if(event_name.equals("CALA")) {
            //Remaining
            TextView data = findViewById(R.id.data_textview);
            TextView subtitle = findViewById(R.id.subtitle);
            subtitle.setText("Citations and Publications\n");
            data.setMovementMethod(new ScrollingMovementMethod());

            data.setText("");
        }
        if(event_name.equals("Comela"))
        {
            TextView data = findViewById(R.id.data_textview);
            TextView subtitle = findViewById(R.id.subtitle);
            subtitle.setText("Citations and Publications\n");
            data.setMovementMethod(new ScrollingMovementMethod());

            data.setText("Citations\n" +
                     "_________________________________________\n\n"+
                    "All proceedings will be channeled through SCOPUS citation indexing. Here, papers submitted to the COMELA 2020 proceedings will be channeled for SCOPUS ranking and citation.\n\n" +
                    "Publications\n\n" +
                    "All papers submitted to the COMELA 2020 will be considered for review for publication in Top Tier journals and monographs.\n");

        }

    }
}
