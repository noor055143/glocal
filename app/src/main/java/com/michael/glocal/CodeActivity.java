package com.michael.glocal;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.michael.glocal.Utils.NetworkUtil;

public class CodeActivity extends AppCompatActivity implements View.OnClickListener {
    private Button donebtn;
    private EditText codeedit;
    SharedPreferences shared;

    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_code);
        codeedit = (EditText) findViewById(R.id.code);
        donebtn = (Button) findViewById(R.id.done);
        donebtn.setOnClickListener(this);
        shared = PreferenceManager.getDefaultSharedPreferences(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (NetworkUtil.startActivityIfNetworkIsNotConnected(this)) {
            finish();
            return;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        NetworkUtil.canNetworkWatcherThreadKeepRunning = false;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.done) {
            String codeE = codeedit.getText().toString();
            if (TextUtils.isEmpty(codeE)) {
                Toast.makeText(getApplicationContext(), "Enter code!", Toast.LENGTH_SHORT).show();
            } else {
                progress = new ProgressDialog(CodeActivity.this);
                progress.setMessage("Checking code:)");
                progress.show();
                int c = Integer.parseInt(codeedit.getText().toString());
                Log.e("code", Integer.toString(c));
                int code = shared.getInt("code", 0);
                Log.e("code", Integer.toString(code));
                if (code == c) {

                    Intent i = new Intent(CodeActivity.this, UserProfile.class);
                    progress.dismiss();
                    startActivity(i);
                } else {
                    progress.dismiss();
                    Toast.makeText(getApplication(), "Invalid Code", Toast.LENGTH_LONG).show();
                }

            }
        }

    }
}
