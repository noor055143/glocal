package com.michael.glocal;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GestureDetectorCompat;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.michael.glocal.adapter.PageAdapter_main;
import com.michael.glocal.models.singleoption_model;
import com.michael.glocal.models.pagemodel;
import com.tbuonomo.viewpagerdotsindicator.SpringDotsIndicator;

import java.util.ArrayList;

public class CALA extends AppCompatActivity{

    ViewPager menulist;
    SpringDotsIndicator dot;
    ArrayList<singleoption_model> list  = new ArrayList<>();
    private ArrayList<singleoption_model> list2;
    private ArrayList<singleoption_model> list3;
    ImageView back;
    ArrayList<pagemodel> mainlist = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_cal);
        menulist = (ViewPager) findViewById(R.id.gridviewpager);
      //  dot = findViewById(R.id.spring_dots_indicator);
        back = (ImageView) findViewById(R.id.btnBack);

        singleoption_model m1 = new singleoption_model(R.drawable.ic_info,"OVERVIEW");
        singleoption_model m2 = new singleoption_model(R.drawable.ic_theme,"THEME");
        singleoption_model m3 = new singleoption_model(R.drawable.announcements,"ANNOUNCEMENTS");
        singleoption_model m4 = new singleoption_model(R.drawable.ic_agenda,"AGENDA");
        singleoption_model m5= new singleoption_model(R.drawable.ic_speaker,"SPEAKERS");
        singleoption_model m6 = new singleoption_model(R.drawable.ic_business_and_finance,"EVENT GAME");
        singleoption_model m7 = new singleoption_model(R.drawable.ic_location,"MAPS");
        singleoption_model m8 = new singleoption_model(R.drawable.ic_documents,"DOCUMENTS");
        singleoption_model m9 = new singleoption_model(R.drawable.ic_graph,"CHRONOLOGY");
        singleoption_model m11 = new singleoption_model(R.drawable.ic_central_committee,"CENTRAL COMMITTEE");
        singleoption_model m33 = new singleoption_model(R.drawable.meeting,"COLLOQUIA");
        singleoption_model m44 = new singleoption_model(R.drawable.ic_poster,"POSTERS");



        list.add(m1);
        list.add(m2);
        list.add(m3);
        list.add(m4);
        list.add(m5);
        list.add(m6);
        list.add(m7);
        list.add(m8);
        list.add(m9);
        list.add(m11);
        list.add(m33);
        list.add(m44);


        pagemodel pagemodel = new pagemodel(list);
        mainlist.add(pagemodel);
        menulist.setAdapter(new PageAdapter_main(mainlist,this));
      //  dot.setViewPager(menulist);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(CALA.this, MainActivity.class);
                startActivity(i);
            }});
    }
}
