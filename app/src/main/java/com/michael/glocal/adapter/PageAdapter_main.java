package com.michael.glocal.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.michael.glocal.R;
import com.michael.glocal.models.pagemodel;

import java.util.ArrayList;

public class PageAdapter_main extends PagerAdapter {

   ArrayList<pagemodel> models;
    LayoutInflater layoutInflater;
    Context thiscontext;
    public PageAdapter_main( ArrayList<pagemodel> models, Context context) {
        this.models = models;
        thiscontext = context;

    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
         layoutInflater = LayoutInflater.from(thiscontext);
        View view = layoutInflater.inflate(R.layout.optionlist, container, false);
        GridView gridview  = view.findViewById(R.id.alloptions_gridview);
        gridview.setAdapter(new optionadapter(thiscontext,models.get(position).getList()));

        container.addView(view, 0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}