package com.michael.glocal.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.michael.glocal.options.Posters;
import com.michael.glocal.R;
import com.michael.glocal.models.singleoption_model;
import com.michael.glocal.options.CentralCommittee;
import com.michael.glocal.options.Colloquia;
import com.michael.glocal.options.GeneralPapers;
import com.michael.glocal.options.Location;
import com.michael.glocal.options.Presentations;
import com.michael.glocal.options.Schedule_activity;
import com.michael.glocal.options.speakerr;

import java.util.ArrayList;

public class optionadapter extends BaseAdapter {

    ArrayList<singleoption_model> data;
    Context activity;
    LayoutInflater inflater;

    public optionadapter(Context activity, ArrayList<singleoption_model> data){
        this.activity = activity;
        this.data = data;
        inflater = ( LayoutInflater )activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = inflater.inflate(R.layout.singal, null);
        final ImageView img = rowView.findViewById(R.id.option_img);
        TextView textView = rowView.findViewById(R.id.option_textview);
        img.setImageResource(data.get(position).getId1());
        textView.setText(data.get(position).getName1());



        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (position) {

                    case 0:
                        Intent overview = new Intent(activity, com.michael.glocal.options.overview.class);
                        activity.startActivity(overview);
                        break;
                    case 1:
                        Intent theme = new Intent(activity, com.michael.glocal.options.theme.class);
                        activity.startActivity(theme);
                        break;

                    case 2:
                        Intent announcement = new Intent(activity, com.michael.glocal.options.announcement.class);
                        activity.startActivity(announcement);
                        break;
                    case 3:
                            Intent agenda = new Intent(activity, Schedule_activity.class);
                            activity.startActivity(agenda);
                        break;
                    case 4:
                        Intent speakers = new Intent(activity, speakerr.class);
                        activity.startActivity(speakers);
                        break;

                    case 6:
                        Intent location = new Intent(activity, Location.class);
                        activity.startActivity(location);
                        break;
                    case 7:
                        Intent papers = new Intent(activity, GeneralPapers.class);
                        activity.startActivity(papers);
                        break;
                    case 8:
                        Intent chronology = new Intent(activity, com.michael.glocal.options.chronology.class);
                        activity.startActivity(chronology);
                        break;
                    case 9:
                        Intent Central_Committee = new Intent(activity, CentralCommittee.class);
                        activity.startActivity(Central_Committee);
                        break;

                    case 10:
                        Intent colloquia = new Intent(activity, Colloquia.class);
                        activity.startActivity(colloquia);
                        break;
                    case 11:
                        Intent posters = new Intent(activity, Posters.class);
                        activity.startActivity(posters);
                        break;

                 /*   case 12:
                        Intent Presentation = new Intent(activity, Presentations.class);
                        activity.startActivity(Presentation);
                        break;*/
                }

            }
        });

        return rowView;
    }
}
